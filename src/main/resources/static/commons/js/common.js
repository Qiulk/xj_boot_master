/**
 * 公共js
 */
// 模块名称注册
if (pcs == null)
    var pcs = {};
if (pcs.common == null)
    pcs.common = {};

var url = document.location.pathname;
var itmp = url.indexOf("/", 1);
var webpath = itmp < 0 ? url : url.substr(0, itmp);
if (webpath.indexOf('/') == -1) {
    webpath = '/' + webpath;
}
pcs.common = {
    path : window.location.protocol + '//' + window.location.host, //+ webpath,
    nopicUrl:function() {
        return pcs.common.path + '/commons/images/nopic.gif';
    },
    /**
     * 分页的代码 defaultHtml 如果没有找到数据，则将这个变量原本的显示在panel中
     */
    newchangePage : function(url, pageSize, result, youfun, pagePanel, defaultHtml,totalPanel,currentPageInit) {
        $(pagePanel).text('');
        if(totalPanel!=null && totalPanel!=''){
            $(totalPanel).text('');
        }
        pcs.common.beLoading(result);
        var totalRows = 'data.rs.pager.totalRows';
        var currentpage = 'data.rs.pager.currentPage';
        url = encodeURI(url);
        $.post(url, {
            PAGE_SIZE : pageSize,
            CURRENT_PAGE : 1
        }, function(data) {
            if (eval(totalRows) != null && eval(totalRows) != undefined && eval(currentpage) != null && eval(currentpage) != undefined) {
                if(eval(totalRows)!=0 && totalPanel!=null && totalPanel!=''){
                    $(totalPanel).html('共'+eval(totalRows)+'条记录');
                }
                if (pagePanel != null && pagePanel != "" && eval(totalRows) > pageSize) {
                    $(pagePanel).pagination({
                        pageCount: Math.ceil(eval(totalRows)/eval(pageSize)),//页数
                        current:eval(currentpage),
                        jump: true,
                        coping: true,
                        homePage: '首页',
                        endPage: '末页',
                        prevContent: '上页',
                        nextContent: '下页',
                        callback: function (api) {
                            //删除url中的current参数
                            url = pcs.common.delQueStr(url,'current')+"&current=";
                            $.post(url, {
                                PAGE_SIZE : pageSize,
                                CURRENT_PAGE : api.getCurrent()
                            }, function(data) {
                                pcs.common.showdata(data,youfun,result,defaultHtml,api.getCurrent());
                            }, "json");
                        }
                    });
                }
            }
            pcs.common.showdata(data,youfun,result,defaultHtml);
        }, "json");
    },
    /**
     * 分页的代码 defaultHtml 如果没有找到数据，则将这个变量原本的显示在panel中
     */
    changePage : function(url, pageSize, result, youfun, pagePanel, defaultHtml,totalPanel,currentPageInit) {
        $(pagePanel).text('');
        if(totalPanel!=null && totalPanel!=''){
            $(totalPanel).text('');
        }
        pcs.common.beLoading(result);
        var totalRows = 'data.rs.pager.totalRows';
        var currentpage = 'data.rs.pager.currentPage';
        url = encodeURI(url);
        $.post(url, {
            PAGE_SIZE : pageSize,
            CURRENT_PAGE : 1
        }, function(data) {
            if (eval(totalRows) != null && eval(totalRows) != undefined && eval(currentpage) != null && eval(currentpage) != undefined) {
                if(eval(totalRows)!=0 && totalPanel!=null && totalPanel!=''){
                    $(totalPanel).html('共'+eval(totalRows)+'条记录');
                }
                if (pagePanel != null && pagePanel != "" && eval(totalRows) > pageSize) {
                    $(pagePanel).pagination(eval(totalRows), {
                        items_per_page : pageSize,
                        current_page : eval(currentpage) - 1,
                        num_display_entries : 10,
                        callback : function(page_id, panel) {
                            //删除url中的current参数
                            url = pcs.common.delQueStr(url,'current')+"&current=";
                            $.post(url, {
                                PAGE_SIZE : pageSize,
                                CURRENT_PAGE : page_id + 1
                            }, function(data) {
                                pcs.common.showdata(data,youfun,result,defaultHtml,page_id + 1);
                            }, "json");
                        }
                    });
                }
            }
            pcs.common.showdata(data,youfun,result,defaultHtml);
        }, "json");
    },

    /**
     * 插入等待图片1
     */
    beLoading : function(result) {
        $(result).html('<img src="'+pcs.common.path+'/commons/images/loader.gif"/>');
    },


    /**
     * 插入等待图片2
     */
    beLoading2 : function(result) {
        $(result).html('<img src="'+pcs.common.path+'/commons/images/load.gif"/>');
    },

    /**
     * 插入等待图片3
     */
    forLoading: function(result) {
        $(result).html('<li><em style="width:100%;text-align: center;"><img src="'+pcs.common.path+'/commons/images/loader.gif"/></em></li>');
    },

    /**
     * 插入等待图片4
     */
    forLoading2: function(result) {
        $(result).html('<li><em style="width:100%;text-align: left;"><img src="'+pcs.common.path+'/commons/images/load.gif"/></em></li>');
    },

    /**
     * 显示分页数据
     */
    showdata : function(data, youfun, result, defaultHtml,current_page) {
        $(result).text('');
        var t=true;
        //alert(current_page);
        if(current_page==undefined){
            current_page="";
        }else{
            //$('#schCurrentPage').val(current_page);
        }
        if(eval('data.rs.rs')!=null && eval('data.rs.rs')!=undefined && eval('data.rs.rs.length')>0){
            $.each(eval('data.rs.rs'),function(i,n){
                youfun(this,$(result),i,current_page,data.rs.rs.length);
                t=false;
            });
        }
        if(t){	$(result).append(defaultHtml);	}
        $(".list li").mouseover(function(){
            $(this).addClass("over");}).mouseout(function(){
            $(this).removeClass("over");})
        $(".list li:even").addClass("alt");
    },
    /**
     * 分页数据列表
     */
    listPage:function(url,xmlname,result,youfun,defaultHtml){
        url=encodeURI(url);
        $.post(url, {}, function(data) {
            if(data.success){
                pcs.common.showDictData(data,xmlname,youfun,result,defaultHtml);
            }
            else if(data.errors.errmsg == "您没有该操作的权限"){
                $(result).html('<li><em style="text-align: center;width:100%">您没有查看列表的权限</em></li>');
            }
            else{
                alert(data.errors.errmsg);
            }
        }, "json");
    },

    /**
     * 显示分页字典数据
     */
    showDictData:function(data,xmlname,youfun,result,defaultHtml) {
        var t=true;
        if(eval('data.root.'+xmlname+'.rs')!=null && eval('data.root.'+xmlname+'.rs')!=undefined && eval('data.root.'+xmlname+'.rs.length')>0){
            $.each(eval('data.root.'+xmlname+'.rs'),function(i,n){
                youfun(this,$(result),i,n);
                t=false;
            })
        }
        if(t){
            $(result).append(defaultHtml);
        }
    },

    /**
     * 取得数据字典数据
     */
    getDictByPage : function(DICT_ID,CLASS_CODE,result,youfn){
        var url = pcs.common.path + '/json?action=QUERY_T_SYSTEM_STATUS_ACTION&STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        if(CLASS_CODE.length>0){
            url = pcs.common.path + '/json?action=GET_T_SYSTEM_STATUS_ACTION&STATUS_TYPE='+ DICT_ID+'&STATUS_CODE='+CLASS_CODE;//获取某一类的指定的数据
        }
        url=encodeURI(url);
        var xmlname='T_SYSTEM_STATUS';
        pcs.common.listPage(url,xmlname,result,youfn);
    },

    /**
     * 取得字典下拉框数据
     */
    getDictOption : function(DICT_ID,result,val){
        var url = pcs.common.path + '/getSystemStatusList?STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.success){
                    var str='';
                    $.each(data.rs,function(i,obj){
                        var key = obj.statusCode;
                        var value = obj.statusName;
                        if(key == val){
                            str += '<option value="'+key+'" selected="selected">'+value+'</option>';
                        }else{
                            str += '<option value="'+key+'">'+value+'</option>';
                        }
                    });
                    $(result).append(str);
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    },

    clearErrorSpan:function(name){
        $('#'+name+'Span').html('');
    },
    /**
     * 取得字典单选框数据
     */
    getDictRadio : function(DICT_ID,result,name,val,isDisabled){
        var url = pcs.common.path + '/getSystemStatusList?STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.success){
                    var str='';
                    var strDisabled = '';
                    var redStyle = '';
                    if(isDisabled==1){
                        strDisabled = 'disabled="disabled"';
                        redStyle = 'style="color:red"';
                    }
                    $.each(data.rs,function(i,obj){
                        var key = obj.statusCode;
                        var value = obj.statusName;
                        if(key == val){
                            str += '<input '+strDisabled+' type="radio" name="'+name+'" value="'+key+'" style="cursor: pointer;" checked="checked" onclick="pcs.common.clearErrorSpan(&quot;'+name+'&quot;)"/><span '+redStyle+'>'+value+'</span>&nbsp;';
                        }else{
                            str += '<input '+strDisabled+' type="radio" name="'+name+'" value="'+key+'" style="cursor: pointer;" onclick="pcs.common.clearErrorSpan(&quot;'+name+'&quot;)"/>'+value+'&nbsp;';
                        }
                    });
                    $(result).append(str);
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    },
    /**
     * 取得字典单选框数据，查看回填
     */
    getDictRadioValue : function(DICT_ID,result,name,val){
        var url = pcs.common.path + '/getSystemStatusList?STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.success){
                    var str='';
                    $.each(data.rs,function(i,obj){
                        var key = obj.statusCode;
                        var value = obj.statusName;
                        if(key == val){
                            str += value;
                        }
                    });
                    $(result).append(str);
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    },
    /**
     * 取得某个字典的数据并回填
     */
    getDictName : function(DICT_ID,code,result){
        var dicName;//字典名称
        var url = pcs.common.path + '/getSystemStatusList?STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.success){
                    var str='';
                    $.each(data.rs,function(i,obj){
                        var key = obj.statusCode;
                        var value = obj.statusName;
                        if(key == code){
                            $(result).html(filterUndefined(value));
                        }
                    });
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    },
    /**
     * 取得字典复选框数据
     */
    getDictCheckbox : function(DICT_ID,result,name,val,isDisabled){
        var url = pcs.common.path + '/getSystemStatusList?STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.success){
                    var str='';
                    var strDisabled = '';
                    if(isDisabled==1){
                        strDisabled = 'disabled="disabled"';
                    }
                    $.each(data.rs,function(i,obj){
                        var key = obj.statusCode;
                        var value = obj.statusName;
                        if(val!=null && val!='' && val.indexOf(key)>=0){
                            str += '<input '+strDisabled+' type="checkbox" name="'+name+'" value="'+key+'" style="cursor: pointer;" checked="checked" onclick="pcs.common.clearErrorSpan(&quot;'+name+'&quot;)"/>'+value;
                        }else{
                            str += '<input '+strDisabled+' type="checkbox" name="'+name+'" value="'+key+'" style="cursor: pointer;" onclick="pcs.common.clearErrorSpan(&quot;'+name+'&quot;)"/>'+value;
                        }
                    });
                    $(result).append(str);
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    },
    /**
     * 取得字典复选框选中的数据
     */
    getDictCheckboxValue : function(DICT_ID,result,name,val){
        var url = pcs.common.path + '/getSystemStatusList?STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.success){
                    var str='';
                    //alert(DICT_ID);
                    $.each(data.rs,function(i,obj){
                        var key = obj.statusCode;
                        var value = obj.statusName;
                        if(val!=null && val!='' && val.indexOf(key)>=0){
                            str += '<input type="checkbox" name="'+name+'" value="'+key+'" style="cursor: pointer;" checked="checked" disabled="disabled"/>'+value;
                        }
                    });
                    $(result).append(str);
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    },

    /**
     * 取得字典单选框数据并换行
     */
    getDictRadioBr : function(DICT_ID,result,name,val,isDisabled){
        var url = pcs.common.path + '/getSystemStatusList?STATUS_TYPE='+ DICT_ID;	//获取某一类的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.success){
                    var str='';
                    var strDisabled = '';
                    if(isDisabled==1){
                        strDisabled = 'disabled="disabled"';
                    }
                    $.each(data.rs,function(i,obj){
                        var key = obj.statusCode;
                        var value = obj.statusName;
                        if(i!=0){
                            str += '<br/>';
                        }
                        if(key == val){
                            str += '<input '+strDisabled+' type="radio" name="'+name+'" value="'+key+'" style="cursor: pointer;" checked="checked" onclick="pcs.common.clearErrorSpan(&quot;'+name+'&quot;)"/>'+value;
                        }else{
                            str += '<input '+strDisabled+' type="radio" name="'+name+'" value="'+key+'" style="cursor: pointer;" onclick="pcs.common.clearErrorSpan(&quot;'+name+'&quot;)"/>'+value;
                        }
                    });
                    $(result).append(str);
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    },



    /**
     * 返回URL中的参数值，类似JSP中的request.getParamter('id');
     * 用法：var strGetQuery =document.location.search; var id = getQueryString(strGetQuery,'id');
     */
    getQueryString : function (url,name){
        var reg = new RegExp("(^|\\?|&)"+ name +"=([^&]*)(\\s|&|$)", "i");
        if (reg.test(url)){
            return unescape(RegExp.$2.replace(/\+/g, " "));
        }
        return "";
    },

    //设置url参数值，ref参数名,value新的参数值
    changeURLPar: function (url, ref, value){
        var str = "";
        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url + "?" + ref + "=" + value;
        var returnurl = "";
        var setparam = "";
        var arr;
        var modify = "0";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] == ref) {
                    setparam = value;
                    modify = "1";
                }
                else {
                    setparam = arr[i].split('=')[1];
                }
                returnurl = returnurl + arr[i].split('=')[0] + "=" + setparam + "&";
            }
            returnurl = returnurl.substr(0, returnurl.length - 1);
            if (modify == "0")
                if (returnurl == str)
                    returnurl = returnurl + "&" + ref + "=" + value;
        }
        else {
            if (str.indexOf('=') != -1) {
                arr = str.split('=');
                if (arr[0] == ref) {
                    setparam = value;
                    modify = "1";
                }
                else {
                    setparam = arr[1];
                }
                returnurl = arr[0] + "=" + setparam;
                if (modify == "0")
                    if (returnurl == str)
                        returnurl = returnurl + "&" + ref + "=" + value;
            }
            else
                returnurl = ref + "=" + value;
        }
        return url.substr(0, url.indexOf('?')) + "?" + returnurl;
    },

    //删除参数值
    delQueStr: function (url, ref) {
        var str = "";
        if (url.indexOf('?') != -1) {
            str = url.substr(url.indexOf('?') + 1);
        }
        else {
            return url;
        }
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (var i=0;i<arr.length;i++) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref) {
                return url.substr(0, url.indexOf('?'));
            }
            else {
                return url;
            }
        }
    },

    //获取参数值
    getQueStr: function (url, ref) {
        var str = "";
        //判断连接中是否存在 '?'
        if (url.indexOf('?') != -1) {
            //截取?号之后的部分参数
            str = url.substr(url.indexOf('?') + 1);
        }
        else {//不存在返回 ''
            return '';
        }
        var arr = "";
        var returnurl = "";
        var setparam = "";
        //判断参数是否存在 &
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            //遍历参数是否匹配
            for (var i=0;i<arr.length;i++) {
                if (arr[i].split('=')[0] == ref) {
                    //匹配则输出参数值
                    return arr[i].split('=')[1]
                }
            }
            //不匹配返回空
            return '';
        }
        else {//不存在&
            arr = str.split('=');
            if (arr[0] == ref) {
                return arr[1];
            }
            else {
                return '';
            }
        }
    },

    /**
     * 去除多余的字用指定符号代替
     */
    ellipsis : function(str, maxlength, suffix) {
        if (str.length <= maxlength) {
            return str;
        } else {
            return str.substr(0, maxlength - 0) + suffix
        }
    },

    /**
     * 取得用户session
     */
    getUserSession : function(fn) {
        var url = pcs.common.path + '/admin/getUserSession';
        var userinfo = {
            pkSysUser : '',
            userName : '',
            userType : '',
            userLevel : '',
            mobile: '',
            phone : '',
            area : '',
            email : '',
            remark : '',
            lastLoginTime : '',
            lastLoginIp : '',
            createDate : '',
            updateDate : ''
        };
        $.post(url,{}, function(data) {
            if(data.success){
                userinfo = {
                    pkSysUser : data.rs.pkSysUser,
                    userName : data.rs.userName,
                    userType : data.rs.userType,
                    userLevel : data.rs.userLevel,
                    mobile: data.rs.mobile,
                    phone : data.rs.phone,
                    area : data.rs.area,
                    email : data.rs.email,
                    remark : data.rs.remark,
                    lastLoginTime : data.rs.lastLoginTime,
                    lastLoginIp : data.rs.lastLoginIp,
                    createDate : data.rs.createDate,
                    updateDate : data.rs.updateDate
                }
            }else{
                alert(data.errmsg);
                window.parent.location.href=pcs.common.path + '/login';
            }
            if(fn!=null || fn!=undefined){
                fn(userinfo);
            }
        }, "json");
    },

    /**
     * 验证当前页面的操作按钮是否显示
     * @param {} permissionNames
     * @param {} actionNames
     * @param {} fn
     */
    hasPermission : function(permissionNames, actionNames, fn){
        $.ajax({
            type: 'post',
            url: pcs.common.path + '/admin/validatorPermission',
            data: 'PERMISSION_NAME='+ permissionNames + '&ACTION_NAME='+actionNames,
            dataType: 'json',
            async: false,
            success:function(data){
                if(data.success){
                    fn(data);
                }
                else{
                    alert(data.errmsg);
                }
            }
        })
    },

    /**
     * 自定义弹窗，options为可选属性
     */
    showDialog:function(msg,options){
        var settings={
            modal:true,overlay:{opacity:0.5,background:"black"},
            resizable:false,height: 200,width:300,xtype:'ok',fontsize:'14px',url:'',btnValue:'确定',url2:'',btnValue2:'确定2'
        };
        if(options!=null){
            jQuery.extend(settings, options);
        }
        var ximage=pcs.common.path+'/common/images/';
        switch(settings.xtype){
            case 'ok':
                ximage+='ok_1.gif';
                break;
            case 'error':
                ximage+='error_1.gif';
                break;
            case 'info':
                ximage+='info_1.gif';
                break;
            case 'message':
                ximage+='message_1.gif';
                break;
            default:
                ximage+='ok_1.gif';
                break;
        }
        var onclickfn='';
        var btnStyle='margin-left:15px;BORDER-RIGHT: #7EBF4F 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #7EBF4F 1px solid; PADDING-LEFT: 2px; FONT-SIZE: 14px; FILTER: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr=#ffffff, EndColorStr=#B3D997); BORDER-LEFT: #7EBF4F 1px solid; CURSOR: hand; COLOR: black; PADDING-TOP: 2px; BORDER-BOTTOM: #7EBF4F 1px solid';
        if(settings.url!=''){
            onclickfn="window.location.href='"+settings.url+"'";
        }else{
            onclickfn="$(this).parent().parent().dialog(\'close\');";
        }
        var sBtn1='<input type="button" style="'+btnStyle+'" value="'+settings.btnValue+'" onclick="'+onclickfn+'"/>';
        var sBtn2='';
        if(settings.url2!=''){
            onclickfn="window.location.href='"+settings.url2+"'";
            sBtn2='<input type="button" style="'+btnStyle+'" value="'+settings.btnValue2+'" onclick="'+onclickfn+'"/>';
        }else{
            onclickfn="$(this).parent().parent().dialog(\'close\');";
        }


        var str='<div id="diaglog" class="flora"><img style="padding-right:15px;padding-left:5px;" src="'
            +ximage
            +'" alt=""/><span style="font-size:'+settings.fontsize+';padding-bottom:-10px;">'
            +msg+'</span><div align="center" style="padding-top:25px;">'
            +sBtn1
            +sBtn2
            +'</div></div>';
        $(str).dialog(settings);
    },

    /**
     * 选中所有指定name的组件
     */
    allCheck : function(name) {
        $("input[@name=" + name + "]").each(function() {
            $(this).attr("checked", true);
        });
    },

    /**
     * 去除所有指定name的组件
     */
    desCheck : function(name) {
        $("input[@name=" + name + "]").each(function() {
            $(this).attr("checked", false);
        });
    },

    /**
     * 字符串中指定子字符串按指定样式显示
     */
    setStringHtmlCss : function(t, s, h, c) {
        return t.replace(eval('/' + s + '/g'), '<' + h + ' class="' + c + '">'	+ s + '</' + h + '>');
    },

    /**
     * 	imgId:随机码显示位置的id；xtype;显示类型eg,src,value；def_SessionName：定义一个session名字，空则默认
     */
    getRandcode : function(imgId, xtype, def_SessionName) {

        $('#' + imgId).attr(xtype,"/public/rndcode.jsp?" + Math.random() + '&sessionName=' + def_SessionName);
    },

    /**
     * 取Cookie
     */
    getCookie : function(name) {
        var strCookie = document.cookie;
        var arrCookie = strCookie.split("; ");
        for (var i = 0; i < arrCookie.length; i++) {
            var arr = arrCookie[i].split("=");
            if (arr[0] == name) {
                if (arr[1] == '' || arr[1] == null || arr[1] == undefined) {
                    return "";
                } else {
                    return arr[1];
                }
            }
        }
        return "";
    },

    /**
     * 过滤js脚本和html标签
     */
    noHTML : function(Htmlstring) // 去除HTML标记
    {
        Htmlstring = Htmlstring.replace(/<script.*?>.*?<\/script>/ig, '');
        // 删除html
        Htmlstring = Htmlstring.replace(/<\/?[^>]+>/g, ''); // 去除HTML tag
        //Htmlstring = Htmlstring.replace(/\n/g, '</br>'); // 换行符转换成br
        //Htmlstring = Htmlstring.replace(/\40/g, '&nbsp;&nbsp;'); // 去除行尾空白
        Htmlstring = Htmlstring.replace(/[ | ]*\n/g,'\n'); //去除行尾空白
        Htmlstring = Htmlstring.replace(/\n[\s| | ]*\r/g,'\n'); //去除多余空行
        Htmlstring = Htmlstring.replace(/&nbsp;/ig,'');//去掉&nbsp;
        return $.trim(Htmlstring);
    },

    /**
     * 获取kindeditor编辑器中图片的访问路径
     *hiddenId：页面中隐藏域id，str：编辑器内容，addr：访问路径开头部分
     */
    chkfileImage : function (hiddenId,str,addr){
        var val='';
        var arr;
        patt = new RegExp('<img.*?src="('+addr+'[^\"]*?(\\.gif|\\.png|\\.bmp|\\.jpg))".*?>','g');
        while((arr=patt.exec(str))!=null)val=val+'#'+arr[1];
        $(hiddenId).val(val);
    },
    /**
     * 获取kindeditor编辑器中视频的访问路径
     *hiddenId：页面中隐藏域id，str：编辑器内容，addr：访问路径开头部分
     */
    chkfileMedia : function (hiddenId,str,addr){
        var val='';
        var arr;
        patt = new RegExp('<embed.*?src="('+addr+'[^\"]*?(\\.swf|\\.flv|\\.mp3|\\.wav|\\.wma|\\.wmv|\\.mid|\\.avi|\\.mpg|\\.asf|\\.rm|\\.rmvb))".*?>','g');
        while((arr=patt.exec(str))!=null)val=val+'#'+arr[1];
        //alert(val);
        $(hiddenId).val(val);
    },
    /**
     * 获取kindeditor编辑器中文件的访问路径
     *hiddenId：页面中隐藏域id，str：编辑器内容，addr：访问路径开头部分
     */
    chkfileFile : function (hiddenId,str,addr){
        var val='';
        var arr;
        patt = new RegExp('<a.*?href="('+addr+'[^\"]*?(\\.doc|\\.docx|\\.xls|\\.xlsx|\\.ppt|\\.htm|\\.html|\\.txt|\\.zip|\\.rar|\\.gz|\\.bz2))".*?>','g');
        while((arr=patt.exec(str))!=null)val=val+'#'+arr[1];
        $(hiddenId).val(val);
    },

    /**
     * 取得浏览器类型
     */
    getBrowser : function() {
        var browser;
        if ($.browser.msie) {
            browser = "msie";
        } else if ($.browser.safari) {
            browser = "safari";
        } else if ($.browser.mozilla) {
            browser = "mozilla";
        } else if ($.browser.opera) {
            browser = "opera";
        } else {
            browser = "unknow";
        }
        return browser;
    },

    /******************************************************
     * 功能：转换时间格式
     * 描述: datetime 为输入时间，format 为时间格式
     ******************************************************/
    toChar:function(datetime, format) {
        if(datetime=="" || datetime==null || datetime==undefined){
            return "";
        }else{
            var date;
            if(typeof datetime == 'number'){
                date = new Date(datetime);
            }else{
                date = datetime;
            }
            var yyyy = date.getFullYear();
            var mm = date.getMonth()+1;
            var dd = date.getDate();
            var hh24 = date.getHours();
            var mi = date.getMinutes();
            var ss = date.getSeconds();
            var s1 = format.replace(/yyyy|YYYY/g, yyyy);
            var s2 = s1.replace(/mm|MM/g,mm<10 ? "0" + mm : mm);
            var s3 = s2.replace(/dd|DD/g,dd<10 ? "0" + dd : dd);
            var s4 = s3.replace(/hh24|HH24/g,hh24<10 ? "0" + hh24 : hh24);
            var s5 = s4.replace(/mi|MI/g,mi<10 ? "0" + mi : mi);
            var s6 = s5.replace(/ss|SS/g,ss<10 ? "0" + ss : ss);
            return s6;
        }
    },

    /******************************************************
     * 功能：判断是否为数字
     * 描述：
     * numstr：需要验证的字符串
     * 用法：
     * isNumber('123');//返回:true;
     ******************************************************/
    isNumber:function(numstr) {
        var i,j,strTemp;
        strTemp = "0123456789";
        if (numstr.length== 0)	{ return false; }
        for (i=0;i<numstr.length;i++) {
            j = strTemp.indexOf(numstr.charAt(i));
            if (j == -1)return false;
        }
        return true;
    },

    /**
     * 取得基础数据
     */
    getBaseIno : function(PARENT_ID,result,youfn){
        var url = pcs.common.path + '/json?action=QUERY_T_BASE_INFO_BY_CONDITION_ACTION&PARENT_ID='+PARENT_ID;
        url=encodeURI(url);
        var xmlname='T_BASE_INFO';
        pcs.common.listPage(url,xmlname,result,youfn);
    },

    /**
     * 取得省份Option
     */
    loadFirstOptions : function (firstArea,result){
        var url = pcs.common.path+'/web/areaall/selectBySuperId?superId=0';
        $.post(url,
            {

            }, function(data){
                if (data.success) {
                    var str = '';
                    $.each(data.rs,function(i,obj){
                        if(firstArea==obj.areaCode){
                            str += '<option value="'+obj.areaCode+'" selected="selected">'+obj.areaName+'</option>';
                        }else{
                            str += '<option value="'+obj.areaCode+'">'+obj.areaName+'</option>';
                        }
                    });
                    $(result).append(str);
                } else {
                    alert(data.errmsg);
                }
            }, 'json');
    },

    /**
     * 取得第二级之后区域
     */
    loadOtherOptions : function (firstArea,secondArea,result){
        var url = pcs.common.path+'/web/areaall/selectBySuperId?superId='+firstArea;
        $.post(url,
            {

            }, function(data){
                if (data.success) {
                    var str = '<option value="'+firstArea+'">全部</option>';
                    var y = 0;
                    $.each(data.rs,function(i,obj){
                        if(secondArea==obj.areaCode){
                            str += '<option value="'+obj.areaCode+'" selected="selected">'+obj.areaName+'</option>';
                        }else{
                            str += '<option value="'+obj.areaCode+'">'+obj.areaName+'</option>';
                        }
                        y++;
                    });
                    if(y != 0){
                        $(result).append(str);
                        $(result).parent().show();
                    }else{
                        $(result).html('');
                        $(result).parent().hide();
                    }
                } else {
                    alert(data.errmsg);
                }
            }, 'json');
    },

    hideStatus:function(){
        window.status=' ';
    },

    /**
     * 取得客户经理下拉框数据
     */
    getUserDictOption : function(DICT_ID,result,val){
        var url = pcs.common.path + '/admin/user/findUser?ROLE_ID='+DICT_ID;	//获取角色的全部数据
        $.post(
            url,
            {},
            function(data){
                if(data.code == 1){
                    var str='';
                    $.each(data.rs,function(i,obj){
                        var key = obj.pkSysUser;
                        var value = obj.realName;
                        if(key == val){
                            str += '<option value="'+key+'" selected="selected">'+value+'</option>';
                        }else{
                            str += '<option value="'+key+'">'+value+'</option>';
                        }
                    });
                    $(result).append(str);
                }else{
                    alert(data.errmsg);
                }
            },
            'json'
        );
    }
};
