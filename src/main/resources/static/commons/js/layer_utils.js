function msg_confirm(content,funcYes,funcNo){
	var confirm = false;
	layer.open({
		icon : 3,
		content: content,
		btn: ['确认','取消'],
		yes: function(index){
			confirm = true;
			layer.close(index);
			return false; //阻止系统默认回车事件
		},
		success: function(layero, index){
			this.enterEsc = function(event){
			  if(event.keyCode === 13){
				layer.close(index);
				return false; //阻止系统默认回车事件
			  }
			};
			
			$(document).on('keydown', this.enterEsc);	//监听键盘事件，关闭层
		},
		end: function(){
			$(document).off('keydown', this.enterEsc);	//解除键盘关闭事件
			if(funcYes && confirm == true) funcYes();
			if(funcNo && confirm == false) funcNo();
		}
	});
}

function msg_alert(content,tip,func){
	var icon = 1;
	if(tip=='ok')icon = 1;
	if(tip=='warn')icon = 7;
	if(tip=='error')icon = 2;
	
	layer.open({
		content: content,
		icon: icon,
		btn: ['确认'],
		yes: function(index){
		   layer.close(index);
		   return false; //阻止系统默认回车事件
		},
		success: function(layero, index){
			this.enterEsc = function(event){
				if(event.keyCode === 13){
					layer.close(index);
					return false; //阻止系统默认回车事件
				}
			};
			$(document).on('keydown', this.enterEsc);	//监听键盘事件，关闭层

		},
		end: function(){
			$(document).off('keydown', this.enterEsc);	//解除键盘关闭事件
			if(func) func();
		}
	});
}

function msg_loading(content){
	layer.msg(content, {icon: 16,time: 0,shade: [0.5, '#393D49']});
}

function msg_close(){
	layer.closeAll();
}

//页面层
function win_open(content,title){
	layer.open({
	  type: 2,
	  title: title==null?false:title,
	  shadeClose: false,
	  shade: 0.7,
	  area: ['80%', '90%'],
	  content: content //iframe的url
	});
}
//页面层
function win_open_min(content,title){
	layer.open({
	  type: 2,
	  title: title==null?false:title,
	  shadeClose: false,
	  shade: 0.7,
	  area: ['30%', '68%'],
	  content: content //iframe的url
	});
}
//页面层
//width,height 百分比或像素
function win_open_inupt(content,title,width,height){
	layer.open({
	  type: 2,
	  title: title==null?false:title,
	  shadeClose: false,
	  shade: 0.7,
	  area: [width, height],
	  content: content //iframe的url
	});
}

//页面层
//width,height 百分比或像素
function win_open_colseinupt(content,title,width,height){
	layer.open({
	  type: 2,
	  title: title==null?false:title,
	  shade: 0.7,
	  area: [width, height],
	  content: content ,//iframe的url
	  cancel:function(){
		  location.reload();
	  }
	});
}
