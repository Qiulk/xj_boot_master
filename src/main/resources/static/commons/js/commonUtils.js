if(pcs==null) var pcs={};
if(pcs.common2==null) pcs.common2 = {};

pcs.common2 = {
	
	/**
	 * 框架返回的日期转成字符串
	 * @param {} datetime
	 * @param {} format
	 * @return {String}
	 */
	formatDate:function(datetime, format) {
		if(datetime=="" || datetime==null || datetime==undefined){
			return "";
		}else{
			var date;
			if(typeof datetime == 'number'){
				date = new Date(datetime);
			}else{
				date = datetime;
			}
			var yyyy = date.getFullYear();
			var mm = date.getMonth()+1;
			var dd = date.getDate();
			var hh24 = date.getHours();
			var mi = date.getMinutes();
			var ss = date.getSeconds();
			var s1 = format.replace(/yyyy|YYYY/g, yyyy);
			var s2 = s1.replace(/mm|MM/g,mm<10 ? "0" + mm : mm);
			var s3 = s2.replace(/dd|DD/g,dd<10 ? "0" + dd : dd);
			var s4 = s3.replace(/hh24|HH24/g,hh24<10 ? "0" + hh24 : hh24);
			var s5 = s4.replace(/mi|MI/g,mi<10 ? "0" + mi : mi);
			var s6 = s5.replace(/ss|SS/g,ss<10 ? "0" + ss : ss);
			return s6;//alert(yyyy+"-"+mm+"-"+dd+" "+hh24+":"+mi+":"+ss);
		}		
	},
	
	removeHtml:function(v){
		return $("<div></div>").html(v).text();
	},
	
	
	
	
	//取得年的下拉列表
	getYearOption:function(select,yyyy){
		$(select).empty();
		for(i=2008;i<2020;i++){
			if((i+"")==yyyy){
				$(select).append("<option value="+i+" selected>"+i+"</option>");
			}else{
				$(select).append("<option value="+i+">"+i+"</option>");
			}
		}
	},
	
	//取得月的下拉列表
	getMonthOption:function(select,mm){
		$(select).empty();
		for(i=1;i<13;i++){
			if((i+"")==mm){
				$(select).append("<option value="+i+" selected>"+i+" 月</option>");
			}else{
				$(select).append("<option value="+i+">"+i+" 月</option>");
			}
		}
	},
	
	//取得日的下拉列表
	getDayOption:function(select,dd){
		$(select).empty();
		for(i=1;i<31;i++){
			if((i+"")==dd){
				$(select).append("<option value="+(i<10?("0"+i):i)+" selected>"+i+" 日</option>");
			}else{
				$(select).append("<option value="+(i<10?("0"+i):i)+">"+i+" 日</option>");
			}
		}
	},
	
	//取得小时的下拉列表
	getHourOption:function(select,hour){
		$(select).empty();
		for(i=0;i<24;i++){
			if((i+"")==hour){
				$(select).append("<option value="+(i<10?("0"+i):i)+" selected>"+i+" 点</option>");
			}else{
				$(select).append("<option value="+(i<10?("0"+i):i)+">"+i+" 点</option>");
			}
		}
	},
	
	//取得分钟的下拉列表
	getMinuteOption:function(select,minute){
		$(select).empty();
		for(i=0;i<60;i++){
			if((i+"")==minute){
				$(select).append("<option value="+(i<10?("0"+i):i)+" selected>"+(i<10?("0"+i):i)+" 分</option>");
			}else{
				$(select).append("<option value="+(i<10?("0"+i):i)+">"+(i<10?("0"+i):i)+" 分</option>");
			}
		}
	},
	
	//取得字符串子串，超过长度用省略号代替
	getHertSubString:function(s,length){
		if(s.length<length){
			return s;
		}
		return s.substring(0,length)+'...';
	},
	
	/**
	 * 取得中英混合字符串子串，超过长度用省略号代替
	 * @param {} s
	 * @param {} length
	 * @return {String}
	 */
	getHertSubStringCH:function(s,length){
		if (!s || !length) {
			return '';
		}
		var a = 0;// 预期计数：中文2字节，英文1字节
		var i = 0;// 循环计数
		var temp = '';// 临时字串
		for (i = 0; i < s.length; i++) {
			if (s.charCodeAt(i) > 255) {
				a += 2;// 按照预期计数增加2
			} else {
				a++;
			}
			if (a > length) {// 如果增加计数后长度大于限定长度，就直接返回临时字符串
				return temp+'…';
			}
			temp += s.charAt(i);// 将当前内容加到临时字符串
		}
		return s;// 如果全部是单字节字符，就直接返回源字符串
	},
	
	
	//分页控件
	changePage:function(url,pageSize,xmlname,result,youfun,pagePanel,defaultHtml,a){
		$(pagePanel).text('');
		pcs.common.showMsgForReq('数据加载中，请稍候……');
//		$(result).addClass('loading');	
		var totalRows='data.root.'+xmlname+'.page.totalRows';
		var currentpage='data.root.'+xmlname+'.page.currentPage';
		url=encodeURI(url);
		$.post(url, {
				PAGE_SIZE : pageSize,
				CURRENT_PAGE : 1
			}, function(data) {
			//判断是否有权限 20090209
			var success = data.success;
			if (success){
				if(eval(totalRows)!=null && eval(totalRows)!="undefined" && eval(currentpage)!=null && eval(currentpage)!="undefined"){
					if(pagePanel!=null && pagePanel!=""&&eval(totalRows)>pageSize){
								 $(result).addClass('loading');	
									$(pagePanel).pagination(
										eval(totalRows), {
											items_per_page : pageSize,
											current_page : eval(currentpage) - 1,
											num_display_entries : (a==undefined||a==null)?4:a,//显示几个页码选择卡
											callback : function(page_id, panel) {
												$.post(url, {
													PAGE_SIZE : pageSize,
													CURRENT_PAGE : page_id + 1
												}, function(data) {
													room.common.showdata(data,xmlname,youfun,result,defaultHtml,eval(currentpage));
												}, "json");
											}
										});
							
						}
				}					
				room.common.showdata(data,xmlname,youfun,result,defaultHtml,eval(currentpage));
				$(result).removeClass('loading');
				pcs.common.closeMsgForReq();
			}
			else
			{
				pcs.common.showDialog("系统繁忙！请稍候再试！",{height:200,xtype:'error'});
//				pcs.common.showDialog(data.errors.errmsg,{height:200,xtype:'error'});
			}
			}, "json");
	},
	
	showdata:function(data,xmlname,youfun,panelname,defaultHtml,CURRENT_PAGE) {
		$(panelname).text('');
		var t=true;
		if(eval('data.root.'+xmlname+'.rs')!=null && eval('data.root.'+xmlname+'.rs')!="undefined" && eval('data.root.'+xmlname+'.rs.length')>0){
			$.each(eval('data.root.'+xmlname+'.rs'),function(i,n){
				youfun(this,$(panelname),i,n,CURRENT_PAGE,eval('data.root.'+xmlname+'.rs').length);
				t=false;
			});
			$(".option_DIV").show();
			$(".option_DIV").removeClass("none");
		}
		if(t){
			$(panelname).append(defaultHtml);
			$(".option_DIV").hide();
			$(".option_DIV").addClass("none");
		}
	},
	
	showdata2:function(data,xmlname,youfun,panelname,defaultHtml,CURRENT_PAGE){
		$(panelname).text('');
		var t=true;
		if(eval('data.root.'+xmlname+'.rs')!=null && eval('data.root.'+xmlname+'.rs')!="undefined" && eval('data.root.'+xmlname+'.rs.length')>0){
			$.each(eval('data.root.'+xmlname+'.rs'),function(i,n){
				youfun(this,$(panelname),i,n,CURRENT_PAGE);
				t=false;
			})
		}
		if(t){
			$(panelname).append(defaultHtml);
		}
	},
	/**
	 *  获取福建省区域级联下拉框，html代码：
		<input type="hidden" id="AREA_ID" name="AREA_ID" value="350000"/>
		<span style="float:left">
			<div class="car">
				<span class="carname">
					<select id="firstSelect" style="font-size: 16px">
					<option value="0">请选择</option>
					</select>
				</span>
				<span class="cartype">
					<select id="secondSelect" style="font-size: 16px"></select>
				</span>
				<span class="wheeltype">
					<select id="thirdSelect" style="font-size: 16px"></select>
				</span>
				<span class="towntype">
					<select id="fourthSelect" style="font-size: 16px"></select>
				</span>
				<!--  <span  class="loading">数据装载中......</span>-->
			</div>
		</span>
	 */
	loadFjArea:function(){
		/*************************所在区域start***********************/
		var areaCode2 = '350000';
		if(areaCode2.length==6 && areaCode2.indexOf('0000')>0){//是一级
			pcs.common.loadFirstOptions(areaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(areaCode2,'','#secondSelect');
			$('.cartype').show();
			
		}else if(areaCode2.length==6 && areaCode2.substring(4)=='00'){//是二级
			var firstareaCode2 = areaCode2.substring(0,2)+'0000';
			pcs.common.loadFirstOptions(firstareaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(firstareaCode2,areaCode2,'#secondSelect');
			pcs.common.loadOtherOptions(areaCode2,'','#thirdSelect');
			$('.cartype').show();
			$('.wheeltype').show();
		}else if(areaCode2.length==6 && areaCode2.substring(4)!='00'){//是三级
			var firstareaCode2 = areaCode2.substring(0,2)+'0000';
			var secondareaCode2 = areaCode2.substring(0,4)+'00';
			pcs.common.loadFirstOptions(firstareaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(firstareaCode2,secondareaCode2,'#secondSelect');
			pcs.common.loadOtherOptions(secondareaCode2,areaCode2,'#thirdSelect');
			pcs.common.loadOtherOptions(areaCode2,'','#fourthSelect');
			$('.cartype').show();
			$('.wheeltype').show();
			$('.towntype').show();
		}else{//是四级
			var firstareaCode2 = areaCode2.substring(0,2)+'0000';
			var secondareaCode2 = areaCode2.substring(0,4)+'00';
			var thirdareaCode2 = areaCode2.substring(0,6);
			pcs.common.loadFirstOptions(firstareaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(firstareaCode2,secondareaCode2,'#secondSelect');
			pcs.common.loadOtherOptions(secondareaCode2,thirdareaCode2,'#thirdSelect');
			pcs.common.loadOtherOptions(thirdareaCode2,areaCode2,'#fourthSelect');
			$('.cartype').show();
			$('.wheeltype').show();
			$('.towntype').show();
		}
		/*************************所在区域end*************************/
	},
	loadArea: function (areaId){
		//alert(1);
		/*************************所在区域start***********************/
		var areaCode2 = areaId;
		$('#AREA_ID').val(areaId);
		if(areaCode2.length==6 && areaCode2.indexOf('0000')>0){//是一级
			pcs.common.loadFirstOptions(areaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(areaCode2,'','#secondSelect');
		}else if(areaCode2.length==6 && areaCode2.substring(4)=='00'){//是二级
			var firstareaCode2 = areaCode2.substring(0,2)+'0000';
			pcs.common.loadFirstOptions(firstareaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(firstareaCode2,areaCode2,'#secondSelect');
			pcs.common.loadOtherOptions(areaCode2,'','#thirdSelect');
		}else if(areaCode2.length==6 && areaCode2.substring(4)!='00'){//是三级
			var firstareaCode2 = areaCode2.substring(0,2)+'0000';
			var secondareaCode2 = areaCode2.substring(0,4)+'00';
			pcs.common.loadFirstOptions(firstareaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(firstareaCode2,secondareaCode2,'#secondSelect');
			pcs.common.loadOtherOptions(secondareaCode2,areaCode2,'#thirdSelect');
			pcs.common.loadOtherOptions(areaCode2,'','#fourthSelect');
		}else{//是四级
			var firstareaCode2 = areaCode2.substring(0,2)+'0000';
			var secondareaCode2 = areaCode2.substring(0,4)+'00';
			var thirdareaCode2 = areaCode2.substring(0,6);
			pcs.common.loadFirstOptions(firstareaCode2,'#firstSelect');
			pcs.common.loadOtherOptions(firstareaCode2,secondareaCode2,'#secondSelect');
			pcs.common.loadOtherOptions(secondareaCode2,thirdareaCode2,'#thirdSelect');
			pcs.common.loadOtherOptions(thirdareaCode2,areaCode2,'#fourthSelect');
		}
		/*************************所在区域end*************************/
	},
	/**
	 * 返回URL中的参数值，类似JSP中的request.getParamter('id'); 用法：var strGetQuery =
	 * document.location.search; var id = GetQueryvalue(strGetQuery,'id');
	 * 
	 * @param {String}
	 *            源URL
	 * @param {String}
	 *            参数名称
	 * @return {String}
	 */
	requestGet:function(sorStr,panStr){
		var  vStr="";  
		if  (sorStr==null  ||  sorStr==""  ||  panStr==null  ||  panStr=="")  return  vStr;  
		//sorStr  =  sorStr.toLowerCase();
		panStr  +=  "=";  
		var  itmp=sorStr.indexOf(panStr);  
		if  (itmp<0){return  vStr;}  
		sorStr  =  sorStr.substr(itmp  +  panStr.length);  
		itmp=sorStr.indexOf("&");  
		if  (itmp<0)
		{
		return  sorStr;  
		}  
		else  
		{
		sorStr=sorStr.substr(0,itmp);  
		return  sorStr;
		}  
	}
}
function checkRate(input)    
{    
     var re = /^[0-9]*(\.[0-9]{1,2})?$/;   //判断字符串是否为数字      
     if (!re.test(input))    
     {    
        return false;    
     }
     return true;
}