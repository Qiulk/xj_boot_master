var DictConstant={
	INDUSTRY_DICT_ID:'1',	 	//行业 字典ID
	ACTIVATE_DICT_ID:'2', //活动 字典ID
	AREA_DICT_ID:'3',	 	//地区 字典ID
	LEVEL_DICT_ID:'4',    //球技水平 字典ID
	MEMBER_TYPE_DICT_ID:'5',	 //会员类型 字典ID
	MATCH_TYPE_DICT_ID:'6',	 //比赛类型 字典ID
	COURSE_DICT_ID:'9',	 	//场地 字典ID
	IDENTYTYPE_DICT_ID:'10',	//证件类型 字典ID
	FUJIAN_AREA_DICT_ID:'13',	//福建省ID
	FUJIAN_AREA: '35' //AREA表中福建省ID
};
//查询基础信息
function search_BaseInfo(select,dictId,parentId,isAsyn){
	if(parentId==undefined){
	   parentId="";
	}
	if(isAsyn==undefined){
	   isAsyn=true;
	}
	var url='../json?action=QUERY_PCS_BASE_INFO_BY_DICTID_PARENTID_ACTION';
	$.ajax({    
    	type: "post",   
    	url : url,   
    	async: false,
    	dataType:'json', 
    	data: 'DICT_ID='+dictId+'&PARENT_ID='+parentId,     
    	success: function(data){ 
            var vobj=data.root.PCS_BASE_INFO;
			if(vobj==null||vobj==''||vobj==undefined){}
			else
			{	
				$.each(vobj.rs,function(i,n){
				$("<option value='"+this.CLASS_CODE+"'>"+this.CLASS_NAME+"</option>").appendTo(select);
				})
				
			} 
		} 
     });   
	 
}
//查询数据字典表
function search_SystemStatus(select,STATUS_TYPE,isAsyn){
	if(isAsyn==undefined){
	   isAsyn=true;
	}
	//var url='../json?action=QUERY_PCS_SYSTEM_STATUS_BY_STATUSTYPE_ACTION&STATUS_TYPE='+STATUS_TYPE;
	//var xmlname='PCS_SYSTEM_STATUS';
	//var panel='#familyMember'
	//$.post(
	//	url,
	//	{},
	//	function(data){
	//		var vobj=data.root.PCS_SYSTEM_STATUS;
	//		if(vobj==null||vobj==''||vobj==undefined){}
	///		else
	//		{			
	//			$.each(data.root.PCS_SYSTEM_STATUS.rs,function(i,n){
	//			$("<option value='"+this.STATUS_CODE+"'>"+this.STATUS_NAME+"</option>").appendTo(select);
	//			});
	//		}
	//	},
	//	'json'
	//);
	var url='../json?action=QUERY_PCS_SYSTEM_STATUS_BY_STATUSTYPE_ACTION';
	$.ajax({    
    	type: "post",   
    	url : url,   
    	async: isAsyn,
    	dataType:'json', 
    	data: 'STATUS_TYPE='+STATUS_TYPE,     
    	success: function(data){ 
            var vobj=data.root.PCS_SYSTEM_STATUS;
			if(vobj==null||vobj==''||vobj==undefined){}
			else
			{				
				$.each(vobj.rs,function(i,n){
				$("<option value='"+this.STATUS_CODE+"'>"+this.STATUS_NAME+"</option>").appendTo(select);
				})
				
			} 
		} 
     }); 
}

function bak_search_BaseInfo(select,dictId,parentId){
	if(parentId==undefined){
	   parentId="";
	}
	
	var url='../json?action=QUERY_PCS_BASE_INFO_BY_DICTID_PARENTID_ACTION&DICT_ID='+dictId+'&PARENT_ID='+parentId;
	var xmlname='PCS_BASE_INFO';
	//var panel='#familyMember'
	$.post(
		url,
		{},
		function(data){
			var vobj=data.root.PCS_BASE_INFO;
			if(vobj==null||vobj==''||vobj==undefined){}
			else
			{				
				$.each(data.root.PCS_BASE_INFO.rs,function(i,n){
				$("<option value='"+this.CLASS_CODE+"'>"+this.CLASS_NAME+"</option>").appendTo(select);
				})
				
			}
		},
		'json'
	);
}

function select_Fj_Area(select,isParent,isAsyn,addValue,addName){

	if(isParent==undefined){
	   isParent="";
	}
	if(isAsyn==undefined){
	   isAsyn=true;
	}
	var str ='';
	if(isParent==""||isParent==0)
	{}
	else
		str +='<option value="">福建省</option>';
	str+='<option value="591">福州</option>';
	str+='<option value="592">厦门</option>';
	str+='<option value="593">宁德</option>';
	str+='<option value="594">莆田</option>';
	str+='<option value="595">泉州</option>';
	str+='<option value="596">漳州</option>';
	str+='<option value="597">龙岩</option>';
	str+='<option value="598">三明</option>';
	str+='<option value="599">南平</option>';
	if(addValue!=null&&addValue!=undefined&&addName!=null&&addName!=undefined)
		str +='<option value='+addValue+'>'+addName+'</option>';
	$(str).appendTo(select);
	
}