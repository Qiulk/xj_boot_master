 
		/********** form **********/
		function doSubmit(formId, action, method, target, enctype) {
			var form = document.getElementById(formId);
			form.action = action || "";
			form.method = method || "POST";
			form.target = target || "_self";
			form.enctype = enctype || "application/x-www-form-urlencoded";
			form.submit();
		}
		function doReset(formId) {
			var form = document.getElementById(formId);
			form.reset();
		}
		function addUrlParameter(url, paramName, paramValue) {
			if (url.indexOf("?") < 0) {
				url += "?" + paramName + "=" + paramValue;
			} else {
				url += "&" + paramName + "=" + paramValue;
			}
		}
		
		/********** checkbox **********/
		//<input type="checkbox" onclick="checkAll('chkName', this)"/>
		//<input type="checkbox" name="chkName"/>
		function checkAll(checkboxName, checkAll) {
			if (checkAll.checked) {
				checkAllTrue(checkboxName);
			} else {
				checkAllFalse(checkboxName);
			}
		}
		function checkAllTrue(checkboxName) {
			var checkbox = document.getElementsByName(checkboxName);
			var count = checkbox.length;
			for (var i = 0; i < count; i++) {
				checkbox[i].checked = true;
			}
		}
		function checkAllFalse(checkboxName) {
			var checkbox = document.getElementsByName(checkboxName);
			var count = checkbox.length;
			for (var i = 0; i < count; i++) {
				checkbox[i].checked = false;
			}
		}
		function getCheckedNum(checkboxName) {
			var checkbox = document.getElementsByName(checkboxName);
			var num = 0;
			var count = checkbox.length;
			for (var i = 0; i < count; i++) {
				if (checkbox[i].checked) {
					num++;
				}
			}
			return num;
		}
		function getCheckedVal(checkboxName) {
			var checkbox = document.getElementsByName(checkboxName);
			var value = "";
			var arr = new Array(getCheckedNum(checkboxName));
			var j = 0;
			var count = checkbox.length;
			for (var i = 0; i < count; i++) {
				if (checkbox[i].checked) {
					arr[j++] = checkbox[i].value;
				}
			}
			value = (arr.join(","));
			return value;
		}
		
		function entryLogin(event,triggerId) {
			var trigger = document.getElementById(triggerId);
			event = event ? event : (window.event ? window.event : null);
			if (event.keyCode == 13) {
				trigger.focus();
			}
		}
		//获得中英混合字符串长度
		function getStringLen(s) { 
			var l = 0; 
			var a = s.split(""); 
			for (var i=0;i<a.length;i++) { 
				if (a[i].charCodeAt(0)<299) { 
					l++; 
				}else{ 
					l+=2; 
				} 
			} 
			return l; 
		}
		//把undefined替换成空串
		function filterUndefined(str){
			if(str == undefined || str == 'undefined' 
				|| str == null || str == 'null'){
				str = '';
			}
			return str;
		}
	
