
		
		/**
		* author: lindr
		*/
		/*******************************验证并聚焦，提示错误信息****************************************/
		//jquery trim
		function jqueryTrim(id){
			return $.trim($('#'+id).val());
		}
		//text验证 null
		function textCheckNull(id,msg){
			var tempVal = jqueryTrim(id);
			if(tempVal==null || tempVal==''){
				alert(msg);
				$('#'+id).focus();
				return false;
			}
			return true;
		}
		//text验证 maxlen
		function textCheckMaxlen(id,maxlen,columnName){
			var tempVal = jqueryTrim(id);
			if(getStringLen(tempVal)>maxlen){
				alert(columnName+'不能超过'+maxlen+'个字符');
				$('#'+id).focus();
				return false;
			}
			return true;
		}
		
		//text验证
		function textCheckNullAndMaxlen(id,maxlen,columnName){
			var tempVal = jqueryTrim(id);
			if(tempVal==null || tempVal==''){
				alert('请输入'+columnName);
				$('#'+id).focus();
				return false;
			}
			if(getStringLen(tempVal)>maxlen){
				alert(columnName+'不能超过'+maxlen+'个字符');
				$('#'+id).focus();
				return false;
			}
			return true;
		}
		//下拉框验证
		function textSeletcNull(id,nullval,columnName){
			var tempVal = jqueryTrim(id);
			if(tempVal==nullval){
				alert('请选择'+columnName);
				return false;
			}
			return true;
		}
		//radio验证
		function radioCheck(name,columnName){
			var radioArr = document.getElementsByName(name);
			for(var i=0;i<radioArr.length;i++){
				if(radioArr[i].checked==true){
					return true;
				}
			}
			alert('请选择'+columnName);
			$('#'+name+'Span').html('<img src="'+pcs.common.path+'/resources/admin/images/error.png"  style="margin-right: 3px"/>');
			return false;
		}
		//radio验证
		function checkBoxCheck(name,columnName){
			var checkBoxArr = document.getElementsByName(name);
			for(var i=0;i<checkBoxArr.length;i++){
				if(checkBoxArr[i].checked==true){
					return true;
				}
			}
			alert('请选择'+columnName);
			$('#'+name+'Span').html('<img src="'+pcs.common.path+'/resources/admin/images/error.png"  style="margin-right: 3px"/>');
			return false;
		}
		//select验证
		function selectCheck(id,columnName){
			var tempVal = jqueryTrim(id);
			if(tempVal==null || tempVal==''){
				alert('请选择'+columnName);
				$('#'+id).focus();
				return false;
			}
			return true;
		}
		
		//联系电话验证
		function phoneCheck(id,columnName){
			var tempVal = jqueryTrim(id);
			if(checkPhone(tempVal)==false){
				alert(columnName+'格式有误，请确认');
				$('#'+id).focus();
				return false;
			}
			return true;
		}
	    //邮编验证
		function postCheck(id,columnName){
			var tempVal = jqueryTrim(id);
			if(checkTextDataForNUMBER(tempVal)==false || tempVal.length!=6){
				alert(columnName+'格式有误，请确认');
				$('#'+id).focus();
				return false;
			}
			return true;
		}
		//验证数字
		function numberCheck(id,columnName){
			var tempVal = jqueryTrim(id);
			if(checkTextDataForNUMBER(tempVal)==false){
				alert(columnName+'格式有误，请输入数字');
				$('#'+id).focus();
				return false;
			}
			return true;
		}
		
		//验证身份证号
		function idcardCheck(id){
			var tempVal = jqueryTrim(id);
			var checkFlag = new clsIDCard(tempVal);    
		    if(!checkFlag.IsValid()){ 
			    alert("身份证号码错误"); 
			    $('#'+id).focus();
			    return false; 
		    }
			return true;
		}
		
		//验证所有的手机号码
		function checkAllMobile(str){
			if(!str.match("^(13|15|18)[0-9]{9}$")){
				return false;
			}
			return true;
		}
		
		/********************************验证格式*****************************************/
		/*验证网址*/
		function checkUrl(vurl){
			if (vurl.length>7)
			{
				var pattern = /^http:\/\/([\w-]+\.)+[\w-]+(\/[\w\-\.\/?%&=]*)?$/;
				var returnValue = pattern.test(vurl);
				if (returnValue ==0){
					return false;
				}else{
					return true;
				}
			}else{
				return false;
			}	
		}
		
		//判断name 是否只由数字、26个英文字母或者下划线组成
		function checkWord(name){
			var pattern = /^\w+$/;
			var returnValue = pattern.test(name);
			if (returnValue == 0){			
		 		return false;		
			}else{
				return true;
			}
		}
		//中文
		function checkChinese(name){		
			var pattern=/^[\u4e00-\u9fa5a-z0-9]{0,10}$/i;
			var returnValue = pattern.test(name);
			if (returnValue == 0){			
		 		return false;		
			}else{
				return true;
			}
		
		}	
		//银行帐号
		function  accountcheck(str) 
		{ 
			var regex=/^(\d{4}[\s\-]?){4}\d{3}$/g; 
			return regex.test(str); 
			
		} 
	
		//验证汉字，2-10个字符
		function checkName(str){
			var regex = /^[\u4e00-\u9fa5]{2,10}$/;
			return regex.text(str);
		}
		
		//验证是否为电话号码
		function checkPhone(phoneNum){
			//var pattern = /^(?:0[0-9]{2,3}[-\\s]{1}|\\(0[0-9]{2,4}\\))[0-9]{6,8}$|^[1-9]{1}[0-9]{5,7}$|^[1-9]{1}[0-9]{10}$/;
			var pattern = /^(\d|-)*$/;
			var returnValue = pattern.test(phoneNum);
			if (returnValue == 0){			
		 		return false;		
			}else{
				return true;
			}
		}
		
		//验证是否为邮箱
		function checkEmail(obj)
		{
		  var pattern=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;     
		  flag=pattern.test(obj);   
		  if(!flag){
		  	return false;   
		  } 
		  return true;
		}   
		
		/**
		* 验证浮点数
		*/
		function checkTextDataForFLOAT(strValue)
		{
			var regTextFloat = /^(-)?(\d)*(\.)?(\d)*$/;
			return regTextFloat.test(strValue);
		}
		
		/**
		* 验证数字
		*/
		function checkTextDataForNUMBER(strValue)
		{
		var regTextNumber = /^(\d)*$/;
		return regTextNumber.test(strValue);
		}
		
		/**
		* 验证英文字母，不区分大小写
		*/
		function checkTextDataForENGLISH(strValue)
		{
		var regTextEnglish = /^[a-zA-Z]*$/;
		return regTextEnglish.test(strValue);
		}
		
		/**
		* 验证大写英文字母
		*/
		function checkTextDataForENGLISHUCASE(strValue)
		{
		var regTextEnglishUCase = /^[A-Z]*$/;
		return regTextEnglishUCase.test(strValue);
		}
		
		/**
		* 验证英文字母和数字，不区分大小写
		*/
		function checkTextDataForENGLISHNUMBER(strValue)
		{
		var regTextEnglishNumber = /^[a-zA-Z0-9]*$/;
		return regTextEnglishNumber.test(strValue);
		}
		
		/**
		* 验证大写英文字母和数字，区分大小写
		*/
		function checkTextDataForENGLISHNUMBER(strValue)
		{
		var regTextEnglishNumber = /^[A-Z0-9]*$/;
		return regTextEnglishNumber.test(strValue);
		}
		
		/**
		* 验证手机号码
		*/
		function checkTextDataForMobile(strValue)
		{
			var isMobile = false;
			
			//移动 2G号段（GSM网络）有134x（0-8）、135、136、137、138、139、150、151、152、158、159、182、183、184。
			//3G号段（TD-SCDMA网络）有157、187、188
			//3G上网卡 147
			//4G号段 178
			var yidongMobileFormat = /^(134|135|136|137|138|139|150|151|152|158|159|182|183|184|157|187|188|147|178|170)[0-9]{8}$/;
			
			//联通2G号段（GSM网络）130、131、132、155、156
			//3G上网卡145
			//3G号段（WCDMA网络）185、186
			//4G号段 176
			var liantongMobileFormat = /^(130|131|132|155|156|145|185|186|176)[0-9]{8}$/;
			
			//电信 2G/3G号段（CDMA2000网络）133、153、180、181、189      
			//4G号段 177
			var dianxinMobileFormat = /^(133|153|180|181|189|177)[0-9]{8}$/;
			
			var isYidongMobile = yidongMobileFormat.test(strValue);
			var isLiantongMobile = liantongMobileFormat.test(strValue);
			var isDianxinMobile = dianxinMobileFormat.test(strValue);
			if(isYidongMobile || isLiantongMobile || isDianxinMobile){
				isMobile = true;
			}
			return isMobile;
		}
		function checkPhoneAndMobile(strValue){
			var booleanFlag=strValue.match("^(13|15|18)[0-9]{9}$");
			var tempFlag=strValue.match(/\d{3}-\d{8}|\d{4}-\d{7}/);
			if(booleanFlag||tempFlag){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		* 验证身份证号
		*/
		function checkTextDataForIdcard(strValue)
		{
			strValue=$.trim(strValue);
			if(strValue.length != 18){
				 return false; 
			}
			var checkFlag = new clsIDCard(strValue);    
		    if(!checkFlag.IsValid()){ 
			    return false; 
		    }
			return true;
		}