package com.yong.config.xj;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/1 20:05
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/01	   邱良堃		    		新建
 * </pre>
 */
public class Contant {

    //填写申请表,审核不通过,银行卡异常,订单取消,授权码失效【状态修改成功会自动发送提醒短信】状态下可以修改
    public static String[] readOnly = {"51", "3", "10", "16", "7"};

    //通过审核的状态
    public static String[] isFinish = {"2"};

    //审批状态组---审批流程
    public static Integer SATAUS_TYPE_APPROVAL = 1;
    //审批状态组-- 放款流程
    public static Integer SATAUS_TYPE_LENDERS = 2;

    public static boolean getReadOnly(String status) {

        boolean isReadOnly = true;

        for (String index : readOnly) {
            if (index.equals(status)) {
                isReadOnly = false;
                break;
            }
        }
        return isReadOnly;
    }

}
