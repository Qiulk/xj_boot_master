package com.yong.dao.sys;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysUserRole;

import java.util.List;

public interface TSysUserRoleMapper {
	/**
	 * 根据用户id查询用户拥有的角色
	 */
	public List<TSysUserRole> queryUserRoleByPKUser(long userId);
	/**
	 * 用户分配角色
	 */
	public int save(TSysUserRole userRole);
	/**
	 * 删除用户角色
	 */
	public int remove(Long pkSysUser);

	/**
	 * 根据条件查询记录总数
	 */
	public int countByExample(Criteria criteria);


	/**
	 * 不同条件查询
	 */
	public List<TSysUserRole> selectByCondition(Criteria example);
}