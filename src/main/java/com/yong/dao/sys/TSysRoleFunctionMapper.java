package com.yong.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysRoleFunction;

public interface TSysRoleFunctionMapper {
	/**
	 * 分配角色权限
	 */
	public int save(TSysRoleFunction roleFunction);
	/**
	 *修改角色时，获取功能菜单列表 
	 */
	public List<TSysRoleFunction> queryRoleFunByFkRole(long pkRole);
	/**
	 *删除角色分配的菜单权限
	 */
	public int remove(@Param("pkRole") long pkRole);
	
	public List<TSysRoleFunction> selectByCondition(Criteria cri);

	public List<TSysRoleFunction> queryRoleFunByFkSysFunction(long fkSysFunction);

}