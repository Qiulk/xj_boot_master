package com.yong.dao.sys;

import java.util.List;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysFunction;


public interface TSysFunctionMapper {
	/**
	 * 根据角色id查询对应的菜单
	 */
	public List<TSysFunction> queryTSysRoleFunctionByPKRole(long pkRole);
	/**
	 * 查询用户附加权限
	 * @param userId
	 * @return
	 */
	public List<TSysFunction> queryUserFuncByPKUserAppend(long userId);
	/**
	 * 查询菜单
	 * @return
	 */
	public List<TSysFunction> queryTSysFunction(Criteria cri);

	
	
	public List<TSysFunction> selectByCondition(Criteria cri);
}