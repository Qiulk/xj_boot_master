package com.yong.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysUser;

public interface TSysUserMapper {
	/**
	 * 登录验证用户名
	 */
	public List<TSysUser> loginCheckUserName(String userName);

	/**
	 * 根据条件查询记录总数
	 */
	public int countByExample(Criteria example);
	/**
	 * 根据条件查询记录集
	 */
	public List<TSysUser> selectByExample(Criteria example);
	/**
	 * 获取用户表序列
	 */
	public Long seqSysUser();
	
	/**
	 * 根据主键获取记录
	 */
	public List<TSysUser> get(Long pkSysUser);

	/**
	 * 确认旧密码
	 */
	public Integer checkOldPwd(@Param("pkSysUser") Long pkSysUser, @Param("oldPwd") String oldPwd);
	
	/**
	 * 找回密码确认邮箱和用户名
	 */
	public List<TSysUser> queryByUserNameAndEmail(@Param("userName") String userName, @Param("email") String email);
	
	/**
	 * 保存记录,不管记录里面的属性是否为空
	 */
	public int save(TSysUser record);
	
	/**
	 * 根据主键更新记录
	 */
	public int update(TSysUser sysUser);
	
	/**
	 * 根据主键删除记录
	 */
	public int remove(@Param("pkSysUser") Long pkSysUser);
}