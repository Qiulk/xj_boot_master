package com.yong.dao.sys;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysOperateLog;


public interface TSysOperateLogMapper {

	/**
	 * 不同条件查询
	 */
	public List<TSysOperateLog> selectByCondition(Criteria example);
	
	/**
	 * 根据条件查询记录总数
	 */
	public int countByExample(Criteria example);
	
	/**
	 * 根据条件查询记录
	 */
	public List<TSysOperateLog> selectByExample(Criteria example);
	
	/**
	 * 时间+序列
	 */
	public Long seqAlways();
	
	/**
	 * 保存
	 */
	public int save(TSysOperateLog obj);
	
	
	/**
	 * 查看
	 */
	public List<TSysOperateLog> get(Long id);
	
	/**
	 * 修改
	 */
	public int update(TSysOperateLog obj);
	
	
	/**
	 * 删除
	 */
	public int remove(@Param("id") Long id, @Param("updateBy") String updateBy);

}
