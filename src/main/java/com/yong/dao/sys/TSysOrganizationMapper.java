package com.yong.dao.sys;

import java.util.List;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysOrganization;
import org.apache.ibatis.annotations.Param;



public interface TSysOrganizationMapper {

	/**
	 * 不同条件查询
	 */
	public List<TSysOrganization> selectByCondition(Criteria example);
	
	/**
	 * 根据条件查询记录总数
	 */
	public int countByExample(Criteria example);
	
	/**
	 * 根据条件查询记录
	 */
	public List<TSysOrganization> selectByExample(Criteria example);
	
	/**
	 * 时间+序列
	 */
	public Long seqAlways();
	
	/**
	 * 保存
	 */
	public int save(TSysOrganization obj);
	
	
	/**
	 * 查看
	 */
	public List<TSysOrganization> get(Long id);
	
	/**
	 * 修改
	 */
	public int update(TSysOrganization obj);
	
	
	/**
	 * 删除
	 */
	public int remove(@Param("id") Long id, @Param("updateBy") String updateBy);

	/**
	 * 更新是否含有子节点字段
	 */
	public int upIsHasChild(@Param("id") Long id,@Param("isHasChild") Long isHasChild);
}
