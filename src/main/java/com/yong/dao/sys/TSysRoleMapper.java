package com.yong.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysRole;

public interface TSysRoleMapper {
	/**
	 * 根据条件查询记录总数
	 */
	public int countByExample(Criteria example);
	
	/**
	 * 根据条件查询记录集
	 */
	public List<TSysRole> selectByExample(Criteria example);
	/**
	 * 验证角色名称重复
	 */
	public List<TSysRole> selectRoleByRoleName(Criteria example);
	/**
	 * 时间+序列
	 * @return
	 */
	public long getRoleId();
	
	/**
	 * 根据主键查询记录
	 */
	public TSysRole selectByPrimaryKey(long pkRole);
	
	/**
	 * 查询角色列表
	 */
	public List<TSysRole> queryRoleList(Criteria example);
	
	/**
	 * 保存角色
	 */
	public int save(TSysRole sysRole);
	
	/**
	 * 根据主键更新记录
	 */
	public int update(TSysRole sysRole);
	/**
	 * 根据主键删除记录
	 */
	public int remove(@Param("pkRole") long pkRole);
	
	/**
	 * 查询角色列表
	 */
	public List<TSysRole> selectByCondition(Criteria criteria);


	public List<TSysRole> get(Long pkRole);
}