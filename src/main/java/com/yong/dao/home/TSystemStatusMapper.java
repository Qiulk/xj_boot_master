package com.yong.dao.home;

import com.yong.model.home.Criteria;
import com.yong.model.home.TSystemStatus;
import org.apache.ibatis.annotations.Param;

import java.util.List;



public interface TSystemStatusMapper {

	public List queryByStatusType(String statusType);

	/**
	 * 查看
	 */
	public List<TSystemStatus> selectByCondition(Criteria example) throws Exception;

	/**
	 * 修改内容
	 * @param  status
	 */
	public int update(TSystemStatus status);

	/**
	 * 删除
	 */
	public int remove(@Param("statusType") String statusType);

	public int batchSave(List<TSystemStatus> list);
}