package com.yong.dao.xj;

import com.yong.model.xj.TLoanCckf;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * description: TLoanCckf <br>
 * date: 2019/12/17 22:30 <br>
 * author: LKQiu <br>
 * version: 1.0 <br>
 */
@Component
public interface TLoanCckfMapper {

    List<TLoanCckf> findLoanCckf();

    int updateCckf(TLoanCckf loanCckf);

    int saveLoanCckf(TLoanCckf loanCckf);

}
