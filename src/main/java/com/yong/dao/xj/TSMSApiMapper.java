package com.yong.dao.xj;

import com.yong.model.xj.TSmsContent;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/12/16 14:50
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/12/16	   邱良堃		    		新建
 * </pre>
 */
@Component
public interface TSMSApiMapper {

    List<TSmsContent> findByParams(String status, String smsType, String phoneNum);

    int saveTSmsContent(TSmsContent tSmsContent);

    int updateTSmsContent(@Param(value = "list") List<TSmsContent> list);

}
