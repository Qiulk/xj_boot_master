package com.yong.dao.xj;

import com.yong.model.xj.TContract;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/11 10:35
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/11	   邱良堃		    		新建
 * </pre>
 */
@Component
public interface TContractMapper {

    public List<TContract> findParant();

    public List<TContract> findByParant(Long parantId);

    public int update(TContract tContract);

    public int add(TContract tContract);

    public int remove(Long conId);

    public TContract findById(Long conId);
}
