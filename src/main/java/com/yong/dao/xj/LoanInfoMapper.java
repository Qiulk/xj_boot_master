package com.yong.dao.xj;

import com.yong.model.xj.TLoanFlow;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface LoanInfoMapper {


    public List<TLoanFlow> queryList(Long pkId);

    public int save(TLoanFlow tLoanFlow);

    public Integer isFinishByUserId(Long userId);

}
