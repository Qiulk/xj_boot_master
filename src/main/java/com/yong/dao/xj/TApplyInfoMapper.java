package com.yong.dao.xj;

import com.yong.model.home.Criteria;
import com.yong.model.xj.TApplyInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface TApplyInfoMapper {
    /**
     * 登录验证用户名
     */
    public List<TApplyInfo> loginCheckUserName(String userName);

    /**
     * 根据条件查询记录总数
     */
    public int countByExample(Criteria example);

    public int takeCashCountByExample(Criteria example);

    /**
     * 根据条件查询记录集
     */
    public List<TApplyInfo> selectByExample(Criteria example);

    public List<TApplyInfo> takeCashSelectByExample(Criteria example);

    /**
     * 获取用户表序列
     */
    public Long seqSysUser();

    /**
     * 根据主键获取记录
     */
    public List<TApplyInfo> get(Long pkSysUser);

    /**
     * 保存记录,不管记录里面的属性是否为空
     */
    public int save(TApplyInfo record);

    /**
     * 根据主键更新记录
     */
    public int update(TApplyInfo sysUser);

    /**
     * 根据主键删除记录
     */
    public int remove(@Param("pkSysUser") Long pkSysUser);

    /*Qiulk  */
    /*插入*/
    public int add(TApplyInfo record);

    /*根据主键更新*/
    public int edit(TApplyInfo applyInfo);

    public TApplyInfo queryById(Long id);

    public List<TApplyInfo> queryPageList(Long user_id);


}
