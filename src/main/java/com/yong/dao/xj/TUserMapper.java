package com.yong.dao.xj;

import com.yong.model.xj.TUser;
import org.springframework.stereotype.Component;

@Component
public interface TUserMapper {

    public TUser userLogin(String loginPhone);

    public int save(TUser user);

    public int remove(Long uid);

    public Integer theOne(String loginPhone);

    public TUser findById(Long userId);

}
