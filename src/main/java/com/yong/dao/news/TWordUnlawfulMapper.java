package com.yong.dao.news;

import java.util.List;

import com.yong.model.home.Criteria;
import com.yong.model.news.TWordUnlawful;



public interface TWordUnlawfulMapper {
	/**
	 * 不同条件查询
	 * @param example
	 * @return
	 */
	public List<TWordUnlawful> selectByCondition(Criteria example);
	
}