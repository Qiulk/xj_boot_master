package com.yong.model.home;


import java.io.Serializable;
import java.util.List;

public class ComboPager
  implements Serializable
{
  private long serialVersionUID = 1124L;
  private Pager pager = null;
  private List rs = null;

  public Pager getPager() { return this.pager; }

  public void setPager(Pager pager) {
    this.pager = pager; }

  public List getRs() {
    return this.rs; }

  public void setRs(List rs) {
    this.rs = rs;
  }
}