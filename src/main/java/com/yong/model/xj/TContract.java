package com.yong.model.xj;

import lombok.Data;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/11 10:32
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/11	   邱良堃		    		新建
 * </pre>
 */
@Data
public class TContract {

    private Long id;

    private String content;

    private Long parentId;

    private Integer isFather;

    private Integer number;
}

