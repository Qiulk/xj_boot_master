package com.yong.model.xj;

import lombok.Data;

import java.sql.Timestamp;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/24 22:27
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/24	   邱良堃		    		新建
 * </pre>
 */
@Data
public class TUser {

    //用户ID
    private Long uid;

    //登录电话
    private String loginPhone;

    //用户密码
    private String password;

    //创建时间
    private Timestamp create_Time;

    //最后更新时间
    private Timestamp lastUpdateTime;

    //伪删除字段
    private Integer isDelete;
}
