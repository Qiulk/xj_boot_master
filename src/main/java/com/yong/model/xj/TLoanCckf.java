package com.yong.model.xj;

import lombok.Data;

import java.util.Date;

/**
 * description: TLoanCckf <br>
 * date: 2019/12/17 22:29 <br>
 * author: LKQiu <br>
 * version: 1.0 <br>
 */
@Data
public class TLoanCckf {

    private String id;

    private String companyName;

    private String webid;

    private String wc;

    private Date updateTime;
}
