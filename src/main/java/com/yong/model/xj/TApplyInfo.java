package com.yong.model.xj;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表
 */
@Data
public class TApplyInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long pkId;
	private Long jkAmount;
	private Long qxId;
	private String idcardNegative;
	private String idcardPositive;
	private Integer isValid;
	private Date createDate;
	private String updateBy;
	private Date updateDate;
	private String realName;
	private String idcardNo;
	private String family_type;
	private String family_name;
	private String family_phone;
	private String crash_type;
	private String crash_name;
	private String crash_phone;
	private String bank_name;
	private String tx_card;
	private String phone;
	private String qq;
	private String wechat;
	private Long yq_acount;
	private Long zm_score;
	private String company_phone;
	private String company_address;
	private String company_name;
	private String position;
	private String job_year;
	private String ysr;
	private String cz_address;
	private Long user_id;
	private Integer nowNode;
	private String status;
	private String remark;
	private String statusName;
}
