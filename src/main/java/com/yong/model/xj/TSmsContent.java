package com.yong.model.xj;

import lombok.Data;

import java.util.Date;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/12/16 14:43
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/12/16	   邱良堃		    		新建
 * </pre>
 */
@Data
public class TSmsContent {

    //Key
    private Long id;

    //手机号码
    private String phoneNum;

    //用户ID
    private Long userId;

    //内容
    private String content;

    //返回信息
    private String backMsg;

    //返回值
    private Integer backCode;

    //发送的类型 varCode验证。simple普通
    private String smsType;

    //状态 t_loan_sms_status
    private Integer status;

    //创建时间
    private Date createTime;

    //最后更新时间
    private Date updateTime;
}
