package com.yong.model.xj;

import lombok.Data;

import java.util.Date;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/28 11:12
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/28	   邱良堃		    		新建
 * </pre>
 */
@Data
public class TLoanFlow {

    //流程id
    private Long flowId;

    //借款信息ID
    private Long pkId;

    //处理人id
    private Long dealUserId;

    //当前节点id
    private Long nodeId;

    //状态
    private Integer status;

    //备注
    private String remark;

    //创建时间
    private Date createTime;

    //伪删除字段
    private Integer isValid;

}
