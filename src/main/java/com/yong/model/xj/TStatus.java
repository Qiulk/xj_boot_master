package com.yong.model.xj;

import lombok.Data;

import java.sql.Timestamp;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/22 14:46
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/22	   邱良堃		    		新建
 * </pre>
 */
@Data
public class TStatus {
    //状态编号
    private String status_code;
    //状态名称
    private String status_name;
    //具体描述
    private String status_desc;
    //是否删除
    private Integer is_valid;
    //创建时间
    private Timestamp create_date;
    //排序
    private Integer order_by;
}
