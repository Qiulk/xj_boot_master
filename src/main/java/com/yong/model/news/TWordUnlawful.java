package com.yong.model.news;

import java.io.Serializable;
import java.util.Date;

/**
 * 敏感字符
 */
public class TWordUnlawful implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String msg;
	private Integer isValid;
	private Date insDate;
	private Integer sort;
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	public Date getInsDate() {
		return insDate;
	}
	public void setInsDate(Date insDate) {
		this.insDate = insDate;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	
	
	
}