package com.yong.model.news;



import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 2018-08-20 18:03
 */
public class TNews implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long pkNews;
	private String title;
	private String pic;
	private String msgKind;
	private String fromKind;
	private String fromAuthor;
	private String fromPath;
	private String subContent;
	private String createBy;
	private Date createDate;
	private String updateBy;
	private Date updateDate;
	private String chkStatus;
	private String chkBy;
	private Date chkDate;
	private String chkMsg;
	private String pubDate;
	private String subhead;
	private Long isValid;
	private String isTop;
	
    private String createName;
	private String msgKindName;
	private String fromKindName;
	private String chkStatusName;	
	private String chkUser;//审核人
	private String content;//内容
	private List<TNewsFiles> fileList;
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getMsgKindName() {
		return msgKindName;
	}

	public void setMsgKindName(String msgKindName) {
		this.msgKindName = msgKindName;
	}

	public String getFromKindName() {
		return fromKindName;
	}

	public void setFromKindName(String fromKindName) {
		this.fromKindName = fromKindName;
	}

	

	public String getChkStatusName() {
		return chkStatusName;
	}

	public void setChkStatusName(String chkStatusName) {
		this.chkStatusName = chkStatusName;
	}


	public String getChkUser() {
		return chkUser;
	}

	public void setChkUser(String chkUser) {
		this.chkUser = chkUser;
	}
 
	public Long getPkNews(){
		return this.pkNews;
	}
 
	public void setPkNews(Long pkNews){
		this.pkNews=pkNews;
	}
 
	public String getTitle(){
		return this.title;
	}
 
	public void setTitle(String title){
		this.title=title;
	}
 
	public String getPic(){
		return this.pic;
	}
 
	public void setPic(String pic){
		this.pic=pic;
	}
 
	public String getMsgKind(){
		return this.msgKind;
	}
 
	public void setMsgKind(String msgKind){
		this.msgKind=msgKind;
	}
 
	public String getFromKind(){
		return this.fromKind;
	}
 
	public void setFromKind(String fromKind){
		this.fromKind=fromKind;
	}
 
	public String getFromAuthor(){
		return this.fromAuthor;
	}
 
	public void setFromAuthor(String fromAuthor){
		this.fromAuthor=fromAuthor;
	}
 
	public String getFromPath(){
		return this.fromPath;
	}
 
	public void setFromPath(String fromPath){
		this.fromPath=fromPath;
	}
 
	public String getSubContent(){
		return this.subContent;
	}
 
	public void setSubContent(String subContent){
		this.subContent=subContent;
	}
 
	public String getCreateBy(){
		return this.createBy;
	}
 
	public void setCreateBy(String createBy){
		this.createBy=createBy;
	}
 
	public Date getCreateDate(){
		return this.createDate;
	}
 
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
 
	public String getUpdateBy(){
		return this.updateBy;
	}
 
	public void setUpdateBy(String updateBy){
		this.updateBy=updateBy;
	}
 
	public Date getUpdateDate(){
		return this.updateDate;
	}
 
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
 
	public String getChkStatus(){
		return this.chkStatus;
	}
 
	public void setChkStatus(String chkStatus){
		this.chkStatus=chkStatus;
	}
 
	public String getChkBy(){
		return this.chkBy;
	}
 
	public void setChkBy(String chkBy){
		this.chkBy=chkBy;
	}
 
	public Date getChkDate(){
		return this.chkDate;
	}
 
	public void setChkDate(Date chkDate){
		this.chkDate=chkDate;
	}
 
	public String getChkMsg(){
		return this.chkMsg;
	}
 
	public void setChkMsg(String chkMsg){
		this.chkMsg=chkMsg;
	}
 
	public String getPubDate(){
		return this.pubDate;
	}
 
	public void setPubDate(String pubDate){
		this.pubDate=pubDate;
	}
 
	public String getSubhead(){
		return this.subhead;
	}
 
	public void setSubhead(String subhead){
		this.subhead=subhead;
	}
 
	public Long getIsValid(){
		return this.isValid;
	}
 
	public void setIsValid(Long isValid){
		this.isValid=isValid;
	}
 
	public String getIsTop(){
		return this.isTop;
	}
 
	public void setIsTop(String isTop){
		this.isTop=isTop;
	}

	public List<TNewsFiles> getFileList() {
		return fileList;
	}

	public void setFileList(List<TNewsFiles> fileList) {
		this.fileList = fileList;
	}
	

}
