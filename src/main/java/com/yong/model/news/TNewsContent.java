package com.yong.model.news;



import java.io.Serializable;
import java.util.Date;


/**
 * 2018-08-20 18:03
 */
public class TNewsContent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String msgId;
	private Long contentNo;
	private String msgContent;
	private Long isValid;
	private Date createDate;
	private Long msgType;
	
	public Long getId(){
		return this.id;
	}
 
	public void setId(Long id){
		this.id=id;
	}
 
	public String getMsgId(){
		return this.msgId;
	}
 
	public void setMsgId(String msgId){
		this.msgId=msgId;
	}
 
	public Long getContentNo(){
		return this.contentNo;
	}
 
	public void setContentNo(Long contentNo){
		this.contentNo=contentNo;
	}
 
	public String getMsgContent(){
		return this.msgContent;
	}
 
	public void setMsgContent(String msgContent){
		this.msgContent=msgContent;
	}
 
	public Long getIsValid(){
		return this.isValid;
	}
 
	public void setIsValid(Long isValid){
		this.isValid=isValid;
	}
 
	public Date getCreateDate(){
		return this.createDate;
	}
 
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}

	public Long getMsgType() {
		return msgType;
	}

	public void setMsgType(Long msgType) {
		this.msgType = msgType;
	}
	

}
