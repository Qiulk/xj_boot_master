package com.yong.model.news;

import java.io.Serializable;
import java.util.Date;


/**
 * 2018-08-20 18:03
 */
public class TNewsFiles implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String url;
	private String filePath;
	private String fileExt;
	private String swfPath;
	private String fileName;
	private String sourceName;
	private String fileSize;
	private Long fkFileLib;
	private Long isValid;
	private Date createDate;
	private String createBy;
	private Date updateDate;
	private String updateBy;
	private Long fileType;

	
	public String getId(){
		return this.id;
	}
 
	public void setId(String id){
		this.id=id;
	}
 
	public String getUrl(){
		return this.url;
	}
 
	public void setUrl(String url){
		this.url=url;
	}
 
	public String getFilePath(){
		return this.filePath;
	}
 
	public void setFilePath(String filePath){
		this.filePath=filePath;
	}
 
	public String getFileExt(){
		return this.fileExt;
	}
 
	public void setFileExt(String fileExt){
		this.fileExt=fileExt;
	}
 
	public String getSwfPath(){
		return this.swfPath;
	}
 
	public void setSwfPath(String swfPath){
		this.swfPath=swfPath;
	}
 
	public String getFileName(){
		return this.fileName;
	}
 
	public void setFileName(String fileName){
		this.fileName=fileName;
	}
 
	public String getSourceName(){
		return this.sourceName;
	}
 
	public void setSourceName(String sourceName){
		this.sourceName=sourceName;
	}
 
	public String getFileSize(){
		return this.fileSize;
	}
 
	public void setFileSize(String fileSize){
		this.fileSize=fileSize;
	}
 
	public Long getFkFileLib(){
		return this.fkFileLib;
	}
 
	public void setFkFileLib(Long fkFileLib){
		this.fkFileLib=fkFileLib;
	}
 
	public Long getIsValid(){
		return this.isValid;
	}
 
	public void setIsValid(Long isValid){
		this.isValid=isValid;
	}
 
	public Date getCreateDate(){
		return this.createDate;
	}
 
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
 
	public String getCreateBy(){
		return this.createBy;
	}
 
	public void setCreateBy(String createBy){
		this.createBy=createBy;
	}
 
	public Date getUpdateDate(){
		return this.updateDate;
	}
 
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
 
	public String getUpdateBy(){
		return this.updateBy;
	}
 
	public void setUpdateBy(String updateBy){
		this.updateBy=updateBy;
	}
 
	public Long getFileType(){
		return this.fileType;
	}
 
	public void setFileType(Long fileType){
		this.fileType=fileType;
	}
 

}
