package com.yong.model.sys;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * TSysOrganization entity
 * 2018-11-30 09:13
 */
@Data
public class TSysOrganization implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long orgId;//组织编码
	private String orgName;//组织名称（全称）
	private String abbreviation;//组织简称
	private Long orgLevel;//组织等级
	private String address;//组织地址
	private Long areaCode;//组织区号
	private Long superId;//上级组织编码
	private Long isValid;
	private String createBy;
	private Date createDate;
	private String updateBy;
	private Date updateDate;
	private String descCode;//组织备注说明
	private Long orderBy;//组织排序
	private Long isHasChild;//是否含有下级组织
}
