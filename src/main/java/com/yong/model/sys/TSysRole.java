package com.yong.model.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * 角色表
 */
public class TSysRole implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long pkRole;//角色ID
	private String roleName;//角色名称
	private String roleDesc;//角色描述
	private Integer isValid;//有效标识
	private String createBy;//CREATE_BY
	private Date createDate;//CREATE_DATE
	private String createDateStr;
	private String updateBy;//UPDATE_BY
	private Date updateDate;//UPDATE_DATE
	public Long getPkRole() {
		return pkRole;
	}
	public void setPkRole(Long pkRole) {
		this.pkRole = pkRole;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleDesc() {
		return roleDesc;
	}
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
}