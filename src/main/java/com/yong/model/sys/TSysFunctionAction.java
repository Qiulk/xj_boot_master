package com.yong.model.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统操作表
 */
public class TSysFunctionAction implements Serializable {
	private static final long serialVersionUID = 1L;
	private String pkSysFunction;// 
	private String pkSysAction;// 
	private Integer isValid;//有效标识
	private String createBy;//CREATE_BY
	private Date createDate;//CREATE_DATE
	private String updateBy;//UPDATE_BY
	private Date updateDate;//UPDATE_DATE
	public String getPkSysFunction() {
		return pkSysFunction;
	}
	public void setPkSysFunction(String pkSysFunction) {
		this.pkSysFunction = pkSysFunction;
	}
	public String getPkSysAction() {
		return pkSysAction;
	}
	public void setPkSysAction(String pkSysAction) {
		this.pkSysAction = pkSysAction;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}