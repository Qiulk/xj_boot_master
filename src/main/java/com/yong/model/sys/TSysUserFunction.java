package com.yong.model.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户菜单表
 */
public class TSysUserFunction implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long pkUserFun;//主键
	private Long fkSysUser;//用户ID
	private String fkSysFunction;//功能代码
	private Integer isValid;//有效标识
	private String createBy;//CREATE_BY
	private Date createDate;//CREATE_DATE
	private String updateBy;//UPDATE_BY
	private Date updateDate;//UPDATE_DATE
	public Long getPkUserFun() {
		return pkUserFun;
	}
	public void setPkUserFun(Long pkUserFun) {
		this.pkUserFun = pkUserFun;
	}
	public Long getFkSysUser() {
		return fkSysUser;
	}
	public void setFkSysUser(Long fkSysUser) {
		this.fkSysUser = fkSysUser;
	}
	public String getFkSysFunction() {
		return fkSysFunction;
	}
	public void setFkSysFunction(String fkSysFunction) {
		this.fkSysFunction = fkSysFunction;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
 
}