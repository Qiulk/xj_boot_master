package com.yong.model.sys;

import java.io.Serializable;
import java.util.Date;


/**
 * TSysOperateLog entity
 * 2018-03-09 15:05
 */
public class TSysOperateLog implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long logId;
	private Long userId;
	private Date logDate;
	private Long logType;
	private String logTab;
	private Long logSource;
	private Long isValid;
	private Long createBy;
	private Date createDate;
	private Long updateBy;
	private Date updateDate;
	private String remark;
	private Long dataId;
	
	public Long getLogId(){
		return this.logId;
	}
 
	public void setLogId(Long logId){
		this.logId=logId;
	}
 
	public Long getUserId(){
		return this.userId;
	}
 
	public void setUserId(Long userId){
		this.userId=userId;
	}
 
	public Date getLogDate(){
		return this.logDate;
	}
 
	public void setLogDate(Date logDate){
		this.logDate=logDate;
	}
 
	public Long getLogType(){
		return this.logType;
	}
 
	public void setLogType(Long logType){
		this.logType=logType;
	}
 
	public String getLogTab(){
		return this.logTab;
	}
 
	public void setLogTab(String logTab){
		this.logTab=logTab;
	}
 
	public Long getLogSource(){
		return this.logSource;
	}
 
	public void setLogSource(Long logSource){
		this.logSource=logSource;
	}
 
	public Long getIsValid(){
		return this.isValid;
	}
 
	public void setIsValid(Long isValid){
		this.isValid=isValid;
	}
 
	public Long getCreateBy(){
		return this.createBy;
	}
 
	public void setCreateBy(Long createBy){
		this.createBy=createBy;
	}
 
	public Date getCreateDate(){
		return this.createDate;
	}
 
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
 
	public Long getUpdateBy(){
		return this.updateBy;
	}
 
	public void setUpdateBy(Long updateBy){
		this.updateBy=updateBy;
	}
 
	public Date getUpdateDate(){
		return this.updateDate;
	}
 
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
 
	public String getRemark(){
		return this.remark;
	}
 
	public void setRemark(String remark){
		this.remark=remark;
	}

	public Long getDataId() {
		return dataId;
	}

	public void setDataId(Long dataId) {
		this.dataId = dataId;
	}
	

}
