package com.yong.model.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * 菜单表
 */
public class TSysFunction implements Serializable {
	private static final long serialVersionUID = 1L;
	private String pkSysFunction;//功能代码
	private String functionName;//功能名称
	private String functionDesc;//功能描述
	private String pkParentFunction;//父功能代码
	private Integer functionLevel;//功能级别
	private String url;//URL
	private Integer isValid;//有效标识
	private String createBy;//CREATE_BY
	private Date createDate;//CREATE_DATE
	private String updateBy;//UPDATE_BY
	private Date updateDate;//UPDATE_DATE
	private Integer orderBy;
	private String functionIcon;
	
	public Integer getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	public String getFunctionIcon() {
		return functionIcon;
	}
	public void setFunctionIcon(String functionIcon) {
		this.functionIcon = functionIcon;
	}
	public String getPkSysFunction() {
		return pkSysFunction;
	}
	public void setPkSysFunction(String pkSysFunction) {
		this.pkSysFunction = pkSysFunction;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getFunctionDesc() {
		return functionDesc;
	}
	public void setFunctionDesc(String functionDesc) {
		this.functionDesc = functionDesc;
	}
	public String getPkParentFunction() {
		return pkParentFunction;
	}
	public void setPkParentFunction(String pkParentFunction) {
		this.pkParentFunction = pkParentFunction;
	}
	public Integer getFunctionLevel() {
		return functionLevel;
	}
	public void setFunctionLevel(Integer functionLevel) {
		this.functionLevel = functionLevel;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}