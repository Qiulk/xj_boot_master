package com.yong.model.sys;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表
 */
@Data
public class TSysUser implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long pkSysUser;
	private String userName;
	private String password;
	private String phone;
	private String area;
	private Integer isValid;
	private String createBy;
	private Date createDate;
	private String updateBy;
	private Date updateDate;
	private String roleName;
	private String allRoleName;

}