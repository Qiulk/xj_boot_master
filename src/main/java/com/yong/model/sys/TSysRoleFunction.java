package com.yong.model.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * 角色菜单表
 */
public class TSysRoleFunction implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long pkRoleFun;//主键
	private Long fkRole;//角色ID
	private String fkSysFunction;//功能代码
	private Integer isValid;//有效标识
	private String createBy;//CREATE_BY
	private Date createDate;//CREATE_DATE
	private String updateBy;//UPDATE_BY
	private Date updateDate;//UPDATE_DATE
	public Long getPkRoleFun() {
		return pkRoleFun;
	}
	public void setPkRoleFun(Long pkRoleFun) {
		this.pkRoleFun = pkRoleFun;
	}
	public Long getFkRole() {
		return fkRole;
	}
	public void setFkRole(Long fkRole) {
		this.fkRole = fkRole;
	}
	public String getFkSysFunction() {
		return fkSysFunction;
	}
	public void setFkSysFunction(String fkSysFunction) {
		this.fkSysFunction = fkSysFunction;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	

 
}