package com.yong.model.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户角色表
 */
public class TSysUserRole implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long pkUserRole;//主键
	private Long fkSysUser;//用户ID
	private Long fkRole;//角色ID
	private Integer isValid;//有效标识
	private String createBy;//CREATE_BY
	private Date createDate;//CREATE_DATE
	private String updateBy;//UPDATE_BY
	private Date updateDate;//UPDATE_DATE
	public Long getPkUserRole() {
		return pkUserRole;
	}
	public void setPkUserRole(Long pkUserRole) {
		this.pkUserRole = pkUserRole;
	}
	public Long getFkSysUser() {
		return fkSysUser;
	}
	public void setFkSysUser(Long fkSysUser) {
		this.fkSysUser = fkSysUser;
	}
	public Long getFkRole() {
		return fkRole;
	}
	public void setFkRole(Long fkRole) {
		this.fkRole = fkRole;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}