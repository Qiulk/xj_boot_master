package com.yong.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserVO {
    @JsonProperty("id") //返回属性名重定义
    private Long pkSysUser;
}
