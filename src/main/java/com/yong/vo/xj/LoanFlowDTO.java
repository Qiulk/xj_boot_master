package com.yong.vo.xj;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/28 10:56
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/28	   邱良堃		    		新建
 * </pre>
 */
@Data
public class LoanFlowDTO {

    //id
    private Long pkId;
    //贷款金额
    private Long jkAmount;
    //借款期限
    private Long qxId;
    //当前流程
    private String nowNode;
    //备注信息
    private String remark;
    //是否能修改
    private Boolean isEdit;
}
