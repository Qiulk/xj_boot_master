package com.yong.vo.xj;

import lombok.Data;

/**
 * description: CckfVO <br>
 * date: 2019/12/17 23:07 <br>
 * author: LKQiu <br>
 * version: 1.0 <br>
 */
@Data
public class CckfVO {

    private String webid;

    private String wc;

}
