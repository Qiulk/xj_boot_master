package com.yong.vo.xj;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;

import java.util.Date;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/23 23:18
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/23	   邱良堃		    		新建
 * </pre>
 */
@Data
public class ApplyInfoDTO {

    //id
    private Long pkId;
    //借款金额
    private Long jkAmount;
    //利率期限id
    private Long qxId;
    //身份证正面路径
    private String idcardNegative;
    //身份证反面路径
    private String idcardPositive;
    //真实姓名
    private String realName;
    //身份证号
    private String idcardNo;
    //直系亲属类型
    private String family_type;
    //直系亲属名字
    private String family_name;
    //直系亲属电话
    private String family_phone;
    //紧急联系人关系
    private String crash_type;
    //紧急联系人姓名
    private String crash_name;
    //紧急联系人电话
    private String crash_phone;
    //提现银行名称
    private String bank_name;
    //提现银行卡号
    private String tx_card;
    //紧急联系电话
    private String phone;
    //紧急联系qq
    private String qq;
    //紧急联系微信
    private String wechat;
    //预期金额
    private Long yq_acount;
    //芝麻信用分
    private Long zm_score;
    //公司电话
    private String company_phone;
    //公司地址
    private String company_address;
    //公司名称
    private String company_name;
    //职务
    private String position;
    //工作年龄
    private String job_year;
    //月收入
    private String ysr;
    //常驻地址
    private String cz_address;
    private String remark;
    private String statusName;

    private Boolean readOnly;


    @Override
    public String toString() {
        return "ApplyInfoDTO{" +
                "pkId=" + pkId +
                ", jkAmount=" + jkAmount +
                ", qxId=" + qxId +
                ", idcardNegative='" + idcardNegative + '\'' +
                ", idcardPositive='" + idcardPositive + '\'' +
                ", realName='" + realName + '\'' +
                ", idcardNo='" + idcardNo + '\'' +
                ", family_type='" + family_type + '\'' +
                ", family_name='" + family_name + '\'' +
                ", family_phone='" + family_phone + '\'' +
                ", crash_type='" + crash_type + '\'' +
                ", crash_name='" + crash_name + '\'' +
                ", crash_phone='" + crash_phone + '\'' +
                ", bank_name='" + bank_name + '\'' +
                ", tx_card='" + tx_card + '\'' +
                ", phone='" + phone + '\'' +
                ", qq='" + qq + '\'' +
                ", wechat='" + wechat + '\'' +
                ", yq_acount=" + yq_acount +
                ", zm_score=" + zm_score +
                ", company_phone='" + company_phone + '\'' +
                ", company_address='" + company_address + '\'' +
                ", company_name='" + company_name + '\'' +
                ", position='" + position + '\'' +
                ", job_year='" + job_year + '\'' +
                ", ysr='" + ysr + '\'' +
                ", cz_address='" + cz_address + '\'' +
                ", remark='" + remark + '\'' +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}
