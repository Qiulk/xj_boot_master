package com.yong.vo;

import lombok.Data;


@Data
public class ResulVO {
    private Integer code;
    private String msg;
    private Object rs;
}
