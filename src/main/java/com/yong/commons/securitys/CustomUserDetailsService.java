package com.yong.commons.securitys;

import com.yong.model.sys.TSysFunction;
import com.yong.model.sys.TSysUser;
import com.yong.service.sys.TSysFunctionService;
import com.yong.service.sys.TSysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 自定义userDetailsService
 * @author jitwxs
 * @since 2018/5/9 9:36
 */
@Service("userDetailsService")
@Slf4j
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private TSysUserService userService;

    @Autowired
    private TSysFunctionService tSysFunctionService;

    @Override
    public MyUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        // 从数据库中取出用户信息
        List<TSysUser> sysUsers = null;
        try {
            sysUsers = userService.getUserListByUserName(username);
        } catch (Exception e) {
            log.error("查询用户失败{}",e);
            throw new DisabledException("查询用户失败");
        }

        // 判断用户是否存在
        if(sysUsers == null || sysUsers.size()==0) {
            throw new DisabledException("用户名不存在");
        }
        TSysUser sysUser = sysUsers.get(0);

        /*if(sysUser.getIsOpen().longValue() == 0L){
            throw new DisabledException("该用户已被停用，请联系管理员启用!");
        }*/

        // 添加权限
        Collection<GrantedAuthority> grantedAuthorities = null;
        try {
            grantedAuthorities = obtionGrantedAuthorities(sysUser);
        } catch (Exception e) {
            log.error("MyUserDetailServiceImpl获取用户权限菜单异常"+e);
            e.printStackTrace();
            throw new DisabledException("获取用户权限菜单异常!");
        }

        // 返回UserDetails实现类
        //return new User(sysUser.getUserName(), sysUser.getPassword(), grantedAuthorities);
        return new MyUserDetails(sysUser.getUserName(), sysUser.getPassword(),sysUser.getPkSysUser(), grantedAuthorities);
    }

    /**
     * 取得用户的权限
     */
    private Set<GrantedAuthority> obtionGrantedAuthorities(TSysUser user) throws Exception {
        List<TSysFunction> list = null;
        list = tSysFunctionService.queryALLUserFunctionByPKUser(user.getPkSysUser());
        /**
         * 过滤重复菜单
         */
        if(!list.isEmpty()){
            Map<String,TSysFunction> map = new TreeMap<>();
            for (Iterator iterator = list.iterator(); iterator.hasNext();) {
                TSysFunction sysFun = (TSysFunction) iterator.next();
                map.put(sysFun.getPkSysFunction(), sysFun);
            }
            list.clear();
            int mapSize = map.size();
            Iterator dbmIt = map.entrySet().iterator();
            for(int i = 0; i < mapSize; i++){
                Map.Entry entry = (Map.Entry) dbmIt.next();
                list.add((TSysFunction)entry.getValue());
            }
        }
        Set<GrantedAuthority> authSet = new HashSet<GrantedAuthority>();
        for (TSysFunction res : list) {
            // TODO:ZZQ 用户可以访问的资源名称（或者说用户所拥有的权限） 注意：必须"ROLE_"开头
            authSet.add(new SimpleGrantedAuthority("ROLE_" +res.getPkSysFunction()));
        }
        return authSet;
    }
}