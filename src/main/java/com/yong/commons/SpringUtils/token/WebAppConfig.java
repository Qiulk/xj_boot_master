package com.yong.commons.SpringUtils.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan(useDefaultFilters = true)
public class WebAppConfig implements WebMvcConfigurer {

    @Autowired
    private LoanInterceptor loanInterceptor;

    /**
     * token 拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new TokenInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(loanInterceptor).addPathPatterns("/loan/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/loan/uploadImg/**").addResourceLocations("file:D:/jiedai/upload/");
    }

}
