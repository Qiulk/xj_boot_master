package com.yong.commons.SpringUtils.token;

import com.alibaba.fastjson.JSON;
import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.ResulVOUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/24 21:59
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/24	   邱良堃		    		新建
 * </pre>
 */
@Component
public class LoanInterceptor implements HandlerInterceptor {

    /**
     * 当前请求进行处理的前置方法
     */

    //填写申请表,审核不通过,银行卡异常,订单取消,授权码失效【状态修改成功会自动发送提醒短信】状态下可以修改
    static String[] unFiller = {"/loan/userLogin", "/loan/upload", "/loan/userSave"
            , "/loan/sms/sendSMSVarCode", "/loan/sms/sendSMSSimple"
            ,"/loan/loanInfo/ccKeFu" };
                                                                                      //loan/sms/sendSMSVarCode
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        String servletPath = request.getServletPath();
        System.out.println(session.getId());


        //过滤不需要user_id的请求
        for (String index : unFiller) {
            if (index.equals(servletPath)) {
                return true;
            }
        }

        if (session.getAttribute("user_Id") == null) {
            response.setContentType("application/json;charset=UTF-8");
            Map<String, Object> resutlt = new HashMap<>();
            resutlt.put("code", ApiResultStatus.LOGIN_LOSE);
            response.getWriter().println(JSON.toJSONString(resutlt));
            return false;
//            session.setAttribute("user_Id", 18250915508L);
        }
        return true;
    }

    /**
     * 在当前请求进行处理之后，也就是Controller方法调用之后执行，但是它会在DispatcherServlet进行视图返回渲染之前被调用。
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    /**
     * 该方法将在整个请求结束之后，也就是在DispatcherServlet 渲染了对应的视图之后执行,可用于清理资源。
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
