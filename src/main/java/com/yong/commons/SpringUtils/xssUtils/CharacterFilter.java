package com.yong.commons.SpringUtils.xssUtils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@Component
@WebFilter(filterName = "characterFilter")
@Order(1)
@Slf4j
public class CharacterFilter implements Filter{


           @Override
           public void destroy() {
          }

           @Override
           public void doFilter(ServletRequest req, ServletResponse res,
                             FilterChain chain) throws IOException, ServletException {
                   HttpServletRequest request = (HttpServletRequest)req;
           		String uri = request.getRequestURI();
           		String contextPath = request.getContextPath();
           		String startUrl = contextPath + "/file/scanupload";
//           		String startUrl2 = contextPath + "/kindeditorUpFiles";
//           		String startUrl3 =contextPath + "/admin/dataCollect/importExcel";
           		if(uri.startsWith(startUrl)){//上传附件
           			chain.doFilter(request, res);
           		}else{
           			TsRequest wrapRequest= new TsRequest(request,request.getParameterMap());
                    chain.doFilter(wrapRequest, res);
           		}
                   
          }

           @Override
           public void init(FilterConfig arg0) throws ServletException {
          }
}
