package com.yong.commons.SpringUtils.xssUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.net.URLDecoder;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;


public class TsRequest extends HttpServletRequestWrapper {

    String filterParams = "RemoteFile,ImageName";
	
           private Map params;
           public TsRequest(HttpServletRequest request, Map newParams) {
                    super(request);
                    this.params = newParams;
          }

           public Map getParameterMap() {
                    return params ;
          }

           public Enumeration getParameterNames() {
                    Vector l = new Vector( params.keySet());
                    return l.elements();
          }
           
          //过滤特殊字符  
//          public  static  String StringFilter(String   str)   throws   PatternSyntaxException   {     
//               //只允许字母和数字       
//               // String   regEx  =  "[^a-zA-Z0-9]";                     
//               //清除掉所有特殊字符  
//	         String regEx="[`~@$^*()+|{}';'\\[\\]<>~！@#￥……*（）——+|{}【】‘；：”“’。，、？]";  
//	         Pattern   p   =   Pattern.compile(regEx);     
//	         Matcher   m   =   p.matcher(str);     
//	         return   m.replaceAll("").trim();     
//          }    
           
//           public static String cleanXSS(String value,String paramName) {  
//        	   if(paramName.equals("logoutRequest")){//不用过滤的参数
//        		   return value;
//        	   }
//               value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");  
//               value = value.replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;");  
//               value = value.replaceAll("'", "&#39;");  
//               value = value.replaceAll("eval\\((.*)\\)", "");  
//               value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");  
//               value = value.replaceAll("script", "");  
//               return value;  
//           }

           public  String HTMLEncode(String aText,String paramName){
        	   String [] paramArr = filterParams.split(",");
        	   if(null != paramArr && paramArr.length>0){
        		   for (int i = 0; i < paramArr.length; i++) {
        			   String filterParam = paramArr[i].trim();
        			   //System.out.println("filterParam======"+filterParam);
        			   if(paramName.equals(filterParam)){//不用过滤的参数
            		       return aText;
            	       }
        		   }
        	   }
        	     
	    	     final StringBuilder result = new StringBuilder();
	    	     final StringCharacterIterator iterator = new StringCharacterIterator(aText);
	    	     char character =  iterator.current();
	    	     while (character != CharacterIterator.DONE ){
		    	       if (character == '<') {
		    	         result.append("&lt;");
		    	       }
		    	       else if (character == '>') {
		    	         result.append("&gt;");
		    	       }
		    	       //else if (character == '&') {
		    	        // result.append("&amp;");
		    	      //}
		    	       else if (character == '\"') {
		    	         result.append("&quot;");
		    	       }
		    	       else {
		    	         //the char is not a special one
		    	         //add it to the result as is
		    	         result.append(character);
		    	       }
		    	       character = iterator.next();
	    	     }
	    	     return URLDecoder.decode(result.toString());
    	  }

           public String[] getParameterValues(String name) {
                   Object v = params.get(name);
                    if (v == null ) {
                              return null ;
                   } else if (v instanceof String[]) {
                             String[] value = (String[]) v;
                              for (int i = 0; i < value.length; i++) {
                            	  value[i] = this.HTMLEncode(value[i],name);
                            	  //System.out.println("value111111111=================================="+value[i]);
//                                      value[i] = value[i].replaceAll( "<", "&lt;" );
//                                      value[i] = value[i].replaceAll( ">", "&gt;" );
                             }
                              return (String[]) value;
                   } else if (v instanceof String) {
                             String value = (String) v;
                             value = this.HTMLEncode(value,name);
                             //System.out.println("value222222222=================================="+value);
//                             value = value.replaceAll( "<", "&lt;" );
//                             value = value.replaceAll( ">", "&gt;" );
                              return new String[] { (String) value };
                   } else {
                              return new String[] { v.toString() };
                   }
          }

           public String getParameter(String name) {
        	   //System.out.println("param33333333=================================="+name);
                   Object v = params.get(name);
                    if (v == null ) {
                              return null ;
                   } else if (v instanceof String[]) {
                             String[] strArr = (String[]) v;
                              if (strArr.length > 0) {
                                      String value = strArr[0];
                                      //System.out.println("value33333333=================================="+value);
                                      value = this.HTMLEncode(value,name);
//                                      value = value.replaceAll( "<", "&lt;" );
//                                      value = value.replaceAll( "<", "&gt;" );
                                       return value;
                             } else {
                                       return null ;
                             }
                   } else if (v instanceof String) {
                             String value = (String) v;
                             //System.out.println("value44444=================================="+value);
                             value = this.HTMLEncode(value,name);
//                             value = value.replaceAll( "<", "&lt;" );
//                             value = value.replaceAll( ">", "&gt;" );
                              return (String) value;
                   } else {
                              return v.toString();
                   }
          }
}
