package com.yong.commons.utils.AuthCode;

import com.yong.commons.utils.ResulVOUtils;
import com.yong.vo.ResulVO;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/2 23:57
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/02	   邱良堃		    		新建
 * </pre>
 */
@Component
public class SendAuthCode {

    public ResulVO sendAuthCode() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("accountSid").append("=").append(Config.ACCOUNT_SID);
//        sb.append("&to").append("=").append("18965145657");
//        sb.append("&param").append("=").append(URLEncoder.encode("", "UTF-8"));
//        sb.append("&templateid").append("=").append("1251");
////		sb.append("&smsContent").append("=").append( URLEncoder.encode("【秒嘀科技】您的验证码为123456，该验证码5分钟内有效。请勿泄漏于他人。","UTF-8"));
//        String body = sb.toString() + HttpUtil.createCommonParam(Config.ACCOUNT_SID, Config.AUTH_TOKEN);
//        String result = HttpUtil.post(Config.BASE_URL, body);
//        System.out.println(result);
        return ResulVOUtils.success();
    }

}
