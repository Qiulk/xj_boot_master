package com.yong.commons.utils.AuthCode;


public class Config {
	/**
	 * url前半部分
	 */
	public static final String BASE_URL = "https://openapi.miaodiyun.com/distributor/sendSMS";

	/**
	 * 开发者注册后系统自动生成的账号，可在官网登录后查看
	 */
	public static final String ACCOUNT_SID = "4097b2bab263597cc8afd495a689df8e";

	/**
	 * 开发者注册后系统自动生成的TOKEN，可在官网登录后查看
	 */
	public static final String AUTH_TOKEN = "4c28126b5d46810d255949842a0e3104";

	/**
	 * 响应数据类型, JSON或XML
	 */
	public static final String RESP_DATA_TYPE = "JSON";
}
