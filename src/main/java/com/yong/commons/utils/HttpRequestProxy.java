package com.yong.commons.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * <pre>
 * HTTP请求代理类
 * </pre>
 */
public class HttpRequestProxy
{
    public final static String HTTP_REQUEST_CONTENTTYPE_APPLICATION_GZIP = "application/x-gzip";

    /**
     * 连接超时
     */
    private static int connectTimeOut = 60000;


    private final static Log logger = LogFactory.getLog(HttpRequestProxy.class);

    /**
     * 读取数据超时
     */
    private static int readTimeOut = 60000;


    /**
     * <pre>
     * 发送HTTP请求
     * </pre>
     * @param requestURL 请求地址
     * @param requestContent 请求内容
     * @param requestMethod 请求方式:POST或者GET
     * @param connectTimeout 连接超时时间限制
     * @param readTimeout 读取数据超时时间限制
     * @param useGZip 是否使用GZip进行压缩
     * @return 响应内容
     */
    private static String connect(String requestURL, String requestContent, String requestMethod,
                                  int connectTimeout, int readTimeout, boolean useGZip)
    {
        HttpURLConnection url_con = null;
        String responseContent = null;
        URL url = null;
        BufferedWriter bw = null;
        BufferedReader br = null;
        OutputStream out = null;
        try
        {
            url = new URL(requestURL);
            url_con = (HttpURLConnection) url.openConnection();
            url_con.setRequestProperty("User-Agent", "Mozilla/4.0");
            url_con.setConnectTimeout(connectTimeout);
            url_con.setReadTimeout(readTimeout);
            url_con.setRequestMethod(requestMethod);
            if ((requestContent != null) && (!requestContent.equalsIgnoreCase("")))
            {
//				logger.info("给商户发消息（增）："+requestContent+"\n");
                if (useGZip)
                {
                    url_con.setRequestProperty("Content-Type", HTTP_REQUEST_CONTENTTYPE_APPLICATION_GZIP);
                    url_con.setDoOutput(true);
                    out = url_con.getOutputStream();
                    GZIPOutputStream gzip = new GZIPOutputStream(out);
                    bw = new BufferedWriter(new OutputStreamWriter(gzip));
                    bw.write(requestContent);
                }
                else
                {
                    url_con.setDoOutput(true);
                    out = url_con.getOutputStream();
                    bw = new BufferedWriter(new OutputStreamWriter(out));
                    bw.write(requestContent);
                }

                bw.flush();
                bw.close();
                out.close();
            }

            url_con.connect();
            if (HTTP_REQUEST_CONTENTTYPE_APPLICATION_GZIP.equalsIgnoreCase(url_con.getContentType()))
            {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(url_con.getInputStream())));
            }
            else
            {
                br = new BufferedReader(new InputStreamReader(url_con.getInputStream()));
            }

            StringBuffer sb = new StringBuffer();
            char[] re = new char[1024];
            int len = 0;
            while ((len = br.read(re)) != -1)
            {
                sb.append(re, 0, len);
            }
            responseContent = sb.toString();
            br.close();
        }
        catch (IOException e)
        {
            logger.error("连接URL{" + requestURL + "}时遇到网络故障, 异常消息{" + e.getMessage() + "}");
        }
        finally
        {
            if (url_con != null)
            {
                url_con.disconnect();
            }
            bw = null;
            out = null;
            br = null;
            url_con = null;
            url = null;
        }
        return responseContent;
    }

    /**
     * <pre>
     * 发送不带参数的GET的HTTP请求
     * </pre>
     * @param reqUrl HTTP请求URL
     * @return HTTP响应的字符串
     */
    public static String doGet(String reqUrl)
    {
        int paramIndex = reqUrl.indexOf("?");
        if (paramIndex != -1)
        {
            return HttpRequestProxy.doGet(reqUrl.substring(0, paramIndex), reqUrl.substring(paramIndex + 1, reqUrl
                    .length()));
        }
        else
        {
            String requestContent = null;
            return HttpRequestProxy.doGet(reqUrl, requestContent);
        }
    }

    /**
     * <pre>
     * 发送带参数键值对的GET的HTTP请求
     * </pre>
     * @param reqUrl HTTP请求URL
     * @param parameters 参数映射表
     * @param requestEncoding 请求编码
     * @return HTTP响应的字符串
     */
    public static String doGet(String reqUrl, Map<String, String> parameters, String requestEncoding)throws Exception
    {

        StringBuffer params = new StringBuffer();
        for (Iterator<Entry<String, String>> iter = parameters.entrySet().iterator(); iter.hasNext();)
        {
            Entry<String, String> element = iter.next();
            params.append(element.getKey());
            params.append("=");
            params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
            params.append("&");
        }

        if (params.length() > 0)
        {
            params = params.deleteCharAt(params.length() - 1);
        }
        return doGet(reqUrl, params.toString());


    }

    /**
     * <pre>
     * 发送只有参数值无参数名的GET的HTTP请求
     * </pre>
     * @param reqUrl HTTP请求URL
     * @param parameter 参数值
     * @return HTTP响应的字符串
     */
    public static String doGet(String reqUrl, String parameter)
    {
        return HttpRequestProxy.connect(reqUrl, parameter, "GET", HttpRequestProxy.connectTimeOut, HttpRequestProxy.readTimeOut,
                false);
    }

    /**
     * <pre>
     * 发送带参数键值对的POST的HTTP请求
     * </pre>
     * @param reqUrl HTTP请求URL
     * @param parameters 参数映射表
     * @param requestEncoding 请求编码类型, 响应时使用相同编码
     * @return HTTP响应的字符串
     */
    public static String doPost(String reqUrl, Map<String, String> parameters, String requestEncoding)throws Exception
    {
        return HttpRequestProxy.doPost(reqUrl, parameters, requestEncoding, false);
    }

    /**
     * <pre>
     * 发送带参数键值对的POST的HTTP请求
     * </pre>
     * @param reqUrl HTTP请求URL
     * @param parameters 参数映射表
     * @return HTTP响应的字符串
     */
    public static String doPost(String reqUrl, Map<String, String> parameters)throws Exception
    {
        return HttpRequestProxy.doPost(reqUrl, parameters, "utf-8", false);
    }



    /**
     * <pre>
     * 发送带参数键值对的POST的HTTP请求
     * </pre>
     * @param reqUrl HTTP请求URL
     * @param parameters 参数映射表
     * @param requestEncoding 请求编码类型
     * @param useGZip 是否使用GZip进行压缩
     * @return HTTP响应的字符串
     */
    public static String doPost(String reqUrl, Map<String, String> parameters, String requestEncoding, boolean useGZip)throws Exception
    {

        StringBuffer params = new StringBuffer();
        if (useGZip)
        {
            for (Iterator<Entry<String, String>> iter = parameters.entrySet().iterator(); iter.hasNext();)
            {
                Entry<String, String> element = iter.next();
                params.append(element.getKey());
                params.append("=");
                params.append(element.getValue().toString());
                params.append("&");
            }
        }
        else
        {
            for (Iterator<Entry<String, String>> iter = parameters.entrySet().iterator(); iter.hasNext();)
            {
                Entry<String, String> element = iter.next();
                params.append(element.getKey());
                params.append("=");
                params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
                params.append("&");
            }
        }

        if (params.length() > 0)
        {
            params = params.deleteCharAt(params.length() - 1);
        }
        return connect(reqUrl, params.toString(), "POST", HttpRequestProxy.connectTimeOut,
                HttpRequestProxy.readTimeOut, useGZip);


    }

    /**
     * <pre>
     * 发送只有参数值无参数名的POST的HTTP请求
     * </pre>
     * @param reqUrl HTTP请求URL
     * @param parameter 参数值
     * @return HTTP响应的字符串
     */
    public static String doPost(String reqUrl, String parameter)
    {
        return connect(reqUrl, parameter, "POST", HttpRequestProxy.connectTimeOut, HttpRequestProxy.readTimeOut,
                false);
    }

    public static String getParameter(String source, String parameter)
    {
        String paramValue = null;
        String[] keyvalus = source.split("&");
        for (int i = 0; i < keyvalus.length; i++)
        {
            String string = keyvalus[i];
            int index = string.indexOf("=");
            if (index > 0)
            {
                String key = string.substring(0, index);
                if (key.equalsIgnoreCase(parameter))
                {
                    paramValue = string.substring(index + 1, string.length());
                    break;
                }
            }
        }
        return paramValue;
    }

    public static String doPostJosn(String requestURL, String requestContent, String requestMethod)
    {
        HttpURLConnection url_con = null;
        String responseContent = null;
        URL url = null;
        BufferedWriter bw = null;
        BufferedReader br = null;
        OutputStream out = null;
        try
        {
            url = new URL(requestURL);
            url_con = (HttpURLConnection) url.openConnection();
            url_con.setRequestProperty("User-Agent", "Mozilla/4.0");
            url_con.setConnectTimeout(60000);
            url_con.setReadTimeout(60000);
            url_con.setRequestMethod(requestMethod);
            url_con.setRequestProperty("Content-Type", "application/json");
            if ((requestContent != null) && (!requestContent.equalsIgnoreCase("")))
            {
                url_con.setDoOutput(true);
                out = url_con.getOutputStream();
                bw = new BufferedWriter(new OutputStreamWriter(out));
                bw.write(requestContent);
                bw.flush();
                bw.close();
                out.close();
            }

            url_con.connect();
            br = new BufferedReader(new InputStreamReader(url_con.getInputStream()));

            StringBuffer sb = new StringBuffer();
            char[] re = new char[1024];
            int len = 0;
            while ((len = br.read(re)) != -1)
            {
                sb.append(re, 0, len);
            }
            responseContent = sb.toString();
            br.close();
        }
        catch (IOException e)
        {
            logger.error("连接URL{" + requestURL + "}时遇到网络故障, 异常消息{" + e.getMessage() + "}");
        }
        finally
        {
            if (url_con != null)
            {
                url_con.disconnect();
            }
            bw = null;
            out = null;
            br = null;
            url_con = null;
            url = null;
        }
        return responseContent;
    }
}
