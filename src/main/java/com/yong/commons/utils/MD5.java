package com.yong.commons.utils;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

public class MD5 {
    private final static String[] hexDigits = {"0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    /**
     *
     */

    public static String byteArrayToHexString(byte[] b) {
        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            resultSb.append(byteToHexString(b[i]));
        }
        return resultSb.toString();
    }

    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0)
            n = 256 + n;
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }

    public static String MD5Encode(String origin) {
        String resultString = null;

        try {
            resultString = new String(origin);
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteArrayToHexString(md.digest(resultString
                    .getBytes()));
        } catch (Exception ex) {

        }
        return resultString;
    }

    public static String encrypt(String password, String userName) {
        String md5Pwd = MD5.MD5Encode(password);
        return DigestUtils.md5Hex(md5Pwd + "{" + userName + "}");
    }

    public static void main(String[] args) throws Exception {
        String result = "123456";
//        byte[] i = MD5.encryptForAES();

//        System.out.println(MD5.encrypt("xy_000", "admin"));
    }

/*加密数据
	 *
    @param
    content 需要加密的内容
	 *
    @param
    password 加密密码
	 *@return
             */

    public static byte[] encryptForAES(byte[] content, String password) {
        if (content == null || content.length == 0 || password == null) {
            return null;
        }
        try {//不需要这么复杂的key
            byte[] enCodeFormat = getBytesForUTF8(password);
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");// 创建密码器,ECB模式,PKCS5Padding填充方式 AES/ECB/PKCS5Padding
            cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化为加密模式
            byte[] result = cipher.doFinal(content);
            return result; // 加密
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] decryptForAES(byte[] content, String password) {
        if (content == null || content.length == 0 || password == null) {
            return null;
        }
        try {//不需要这么复杂的key
            byte[] enCodeFormat = getBytesForUTF8(password);
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");// 创建密码器 AES/ECB/PKCS5Padding
            cipher.init(Cipher.DECRYPT_MODE, key);// 初始化为解密模式
            byte[] result = cipher.doFinal(content);
            return result; // 解密
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static byte[] getBytesForUTF8(String password) {
        return password.getBytes(StandardCharsets.UTF_8);
    }


    /**
     * @param s               明文密码
     * @param encodedPassword 数据库加密的密码
     * @param username        用户主键
     * @return
     */
    public static boolean checkPasswordWithSalt(String s, String encodedPassword, String username) {
        String encryptstr = encrypt(s, username);
        if (encryptstr.equals(encodedPassword)) {
            return true;
        }
        return false;
    }
}
