package com.yong.commons.utils;

/**
 * web中使用的常量
 * 
 * @author hongbo
 * @date 2011-6-16 下午01:55:19
 */
public interface WebConstants {

	public static final String SESSION_USER_NO = "SESSION_USER_NO";
	public static final String CURRENT_USER = "CURRENT_USER";
	public static final String SESSION_MENU = "SESSION_MENU";
	public static final Long LOG_SOURCE = 1L;
	public static final String FUNCTION_TYPE_NODE = "3";
	public static final String SESSION_ACTION = "SESSION_ACTION";
	public static final String APPLY_STATUS = "status";
}
