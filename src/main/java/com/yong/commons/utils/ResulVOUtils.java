package com.yong.commons.utils;

import com.yong.vo.ResulVO;

import javax.servlet.http.HttpSession;
import java.util.UUID;

public class ResulVOUtils {

    /**
     * 请求成功带返回信息
     * @param object
     * @return
     */
    public static ResulVO success(Object object){
        ResulVO resulVO =new ResulVO();
        resulVO.setRs(object);
        resulVO.setMsg("请求成功");
        resulVO.setCode(200);
        return resulVO;
    }

    /**
     * 请求成功不带返回信息
     * @return
     */
    public static ResulVO success(){
        ResulVO resulVO =new ResulVO();
        resulVO.setMsg("请求成功");
        resulVO.setCode(200);
        return resulVO;
    }

    /**
     * 请求失败
     * @param code
     * @param msg
     * @return
     */
    public static ResulVO error(Integer code,String msg){
        ResulVO resulVO =new ResulVO();
        resulVO.setCode(code);
        resulVO.setMsg(msg);
        return resulVO;
    }
    /**
     * 请求失败返回token
     * @param code
     * @param msg
     * @param  session
     * @return
     */

    public static ResulVO error(Integer code, String msg, HttpSession session){
        ResulVO resulVO =new ResulVO();
        resulVO.setCode(code);
        resulVO.setMsg(msg);
        String token = UUID.randomUUID().toString();
        session.setAttribute("token", token);
        resulVO.setRs(token);
        return resulVO;
    }

}
