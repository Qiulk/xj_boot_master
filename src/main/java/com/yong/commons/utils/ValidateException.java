package com.yong.commons.utils;

public class ValidateException extends Exception {

	private String code;
	/**
	 * 验证异常类
	 */
	private static final long serialVersionUID = 1L;
	
	public ValidateException(String message){
		super(message);
	}
	
	public ValidateException(String message,String code){
		super(message);
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
}
