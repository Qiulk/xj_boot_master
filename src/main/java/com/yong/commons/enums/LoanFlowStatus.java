package com.yong.commons.enums;


public enum LoanFlowStatus implements CodeEnum {
    STATUS_ZZSH(0, "正在审核"),
    STATUS_SHTG(1, "审核通过"),
    STATUS_SQMSX(16, "授权码失效"),
    STATUS_SQTK(15, "申请退款"),
    STATUS_YF_THREE(14, "预付前3期费用"),
    STATUS_YF_TWO(13, "预付前2期费用"),
    STATUS_YF_ONE(12, "预付前1期费用"),
    SATATUS_SQBXF(11, "收取保险费"),
    STATUS_YHKHYC(10, "银行卡号异常"),
    STATUS_DKZ(9, "打款中"),
    STATUS_XYLS(8, "信用流水"),
    STATUS_DDQX(7, "订单取消"),
    STATUS_JDCH(6, "解冻成功"),
    STATUS_JDZ(5, "解冻中"),
    STATUS_DJ(4, "冻结"),
    STATUS_SHBTG(3, "审核不通过"),
    STATUS_FKCG(2, "放款成功"),
    STATUS_TX(51, "填写申请表"),
    STATUS_FQSQ(52, "发起申请"),
    STATUS_DDWC(99, "订单完成"),
    STATUS_FQTX(53, "发起提现"),
    STATUS_YDK(54, "已完成");

    private Integer code;
    private String msg;

    LoanFlowStatus(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getValue() {
        return this.code;
    }

    public String getMessage() {
        return this.msg;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }
}
