package com.yong.commons.enums;

import lombok.Getter;

@Getter
public enum ResulEnum {

    NO_SYS_USER(1000,"用户不存在"),
    SAVE_USER_FAIL(1001,"新增用户失败"),
    USER_LIST_FAIKL(1002,"用户列表获取失败"),
    ROLE_LIST_FAIL(1003,"角色列表获取失败"),
    ROLE_NAME_IS_NULL(1004,"角色名称不能为空"),
    ROLE_NAME_CHECK_FAIL(1005,"验证角色名称异常"),
    ROLE_NAME_RESAVE(1006,"该角色名称已被占用!"),
    ROLE_NOT_MODULE(1007,"请为该角色分配模块权限"),
    ROLE_SAVE_FAIL(1008,"角色保存失败"),
    ROLE_UPDATE_GETFAIL(1009,"修改页面获取角色信息异常"),
    ROLE_UPDATE_FAIL(1010,"角色修改失败"),
    ROLE_DEL_FAIL(1011,"删除角色异常"),
    NO_ROLE_ID(1012,"角色ID异常"),
    FIND_USER_LIST_FAIL(1013,"客户经理信息获取失败"),
    DATE_CONFIG_FAIL(2001,"配置时间失败");

    private Integer code;
    private String msg;

    ResulEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
