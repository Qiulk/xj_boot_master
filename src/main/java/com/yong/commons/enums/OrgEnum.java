package com.yong.commons.enums;

import lombok.Getter;

@Getter
public enum OrgEnum {
    ORG_CHECK_NAME(1001,"组织机构名称查询异常"),
    ORG_SAVE_ERROR(1002,"组织机构保存失败"),
    ORG_NAME(1003,"组织机构组织结构全称不得为空"),
    ORG_NAME_JC(1004,"组织机构组织结构简称不得为空"),
    ORG_SUP_NULL(1005,"上级组织不存在"),
    ORG_NULL(1006,"组织机构不存在"),
    ORG_UP(1007,"组织机构修改失败"),
    ORG_DEL(1008,"组织机构删除失败"),
    ORG_DEL_BICH(1009,"请选择要删除的记录"),
    ORG_DEL_ERROR(1010,"删除失败"),
    ORG_HAS_CHILD(1011,"该机构含有下级，不可删除")
    ;
    private Integer code;
    private String msg;

    OrgEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
