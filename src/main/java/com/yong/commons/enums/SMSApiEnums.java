package com.yong.commons.enums;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/12/16 16:03
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/12/16	   邱良堃		    		新建
 * </pre>
 */
public class SMSApiEnums {

    //短信息类型_验证码
    public final static String SMSTYPE_VARCODE = "varCode";

    //短信类型_普通
    public final static String SMSTYPE_SIMPLE = "simple";

    //短信状态_已经发送
    public final static Integer SMS_STATUS_SEND = 10;

    //短信状态_已验证
    public final static Integer SMS_STATUS_AUTHSUCCES = 11;

    //短信状态_已经过期
    public final static Integer SMS_STATUS_TIMEOUT = 12;

    //短信状态_失败
    public final static Integer SMS_STATUS_FAIL = 13;
}
