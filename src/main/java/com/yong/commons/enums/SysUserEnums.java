package com.yong.commons.enums;

import lombok.Getter;


@Getter
public enum SysUserEnums {
    LONGIN_OUT(1001,"登录超时"),
    SYS_ERROR(9999,"系统异常"),
    ROLE_TREE_ERRO(1002,"获取角色树异常!"),
    GET_PKID_ISNULL(1003,"主键不得为空"),
    CHECK_DEL_RECODE(1004,"请选择要删除的用户"),
    FILE_UPLOD_FAIL(1005,"文件上传失败"),
    USER_NAME_NULL(1006,"用户名不得为空"),
    PWD_NULL(1007,"密码不得为空"),
    USER_ROLE_NOLL(1011,"用户角色未分配"),
    USER_ROLE_NOT(1012,"用户所分配的角色不存在或已被删除")
    ;
    private Integer code;
    private String msg;

    SysUserEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
