package com.yong.commons.enums;

import lombok.Getter;


@Getter
public enum FunctionEnum {

    ADD_USER_FUN("001","添加用户"),

    ;
    private String functionCode;
    private String functionName;

    FunctionEnum(String functionCode, String functionName) {
        this.functionCode = functionCode;
        this.functionName = functionName;
    }
}
