package com.yong.commons.enums;

import com.yong.commons.enums.test.EnumMessage;
import lombok.Getter;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/22 23:12
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/22	   邱良堃		    		新建
 * </pre>
 */
@Getter
public class ApiResultStatus {

    //成功
    public static final Integer SC_OK_200 = 200;

    //错误
    public static final Integer FAIL_ERRO_500 = 500;

    //非法进入
    public static final Integer FAIL_NO_AUTH = 501;

    //登录失效
    public static final Integer LOGIN_LOSE = 700;

    //重复发起订单
    public static final Integer DOUBLE_REQUEST = 203;
}
