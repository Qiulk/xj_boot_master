package com.yong.commons.enums.test;

public interface EnumMessage {
    Integer getValue();
    String getMessage();
}
