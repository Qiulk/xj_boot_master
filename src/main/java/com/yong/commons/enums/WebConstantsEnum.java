package com.yong.commons.enums;

import lombok.Getter;

@Getter
public enum WebConstantsEnum {
    //验证码session
    SESSION_VERIFY_CODE("SESSION_VERIFY_CODE"),
    //用户信息session
    SESSION_USER("CURRENT_USER"),
    //用户菜单信息
    SESSION_USER_MENU("SESSION_MENU"),
    SESSION_USER_ON("SESSION_USER_ON"),
    SESSION_ACTION("SESSION_ACTION"),
    SESSION_USER_ROLE("SESSION_USER_ROLE"),
    SESSION_ROLE_TOP("SESSION_ROLE_TOP")
    ;
    private String msg;


    WebConstantsEnum(String msg) {
        this.msg = msg;
    }
}
