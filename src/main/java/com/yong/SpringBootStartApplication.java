package com.yong;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/4 20:05
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/04	   邱良堃		    		新建
 * </pre>
 */
public class SpringBootStartApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(YongBootApplication.class);
    }
}
