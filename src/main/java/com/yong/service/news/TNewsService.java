package com.yong.service.news;



import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.news.TNews;
import com.yong.model.news.TNewsContent;
import com.yong.model.news.TNewsFiles;



public interface TNewsService {

	/**
	 * 根据文件的访问路径转移到文件服务器
	 */
	public boolean transferFileToImage(String image, HttpServletRequest req) throws Exception;
 
	/**
	 * 本地地址
	 */
	public String localUrl() throws Exception;
	/**
	 * 文件服务器访问路径
	 */
	public String fileServerUrl() throws Exception;
	/**
	 * 临时访问路径
	 */
	public String tmpUrl() throws Exception;
	
	/**
	 * 正式访问路径
	 */
	public String realUrl() throws Exception;
 
	/**
	 * 正式存储路径
	 */
	public String realPath() throws Exception;


}
