package com.yong.service.news.impl;



import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.yong.commons.utils.WebConstants;
import com.yong.dao.news.TWordUnlawfulMapper;
import com.yong.dao.sys.TSysOperateLogMapper;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.news.TNews;
import com.yong.model.news.TNewsContent;
import com.yong.model.news.TNewsFiles;
import com.yong.model.sys.TSysOperateLog;
import com.yong.model.sys.TSysUser;
import com.yong.service.news.TNewsContentService;
import com.yong.service.news.TNewsService;
import com.yong.service.sys.TSysUserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;



@Service
public class TNewsServiceImpl implements TNewsService {

	@Autowired
	private TSysUserService tSysUserService;
	@Autowired
	private TNewsContentService newsContentService;
	@Autowired
	private TWordUnlawfulMapper wordUnlawfulMapper;
	@Autowired
	private TSysOperateLogMapper sysOperateLogMapper;
 
	@Value("${tmp_url:/resources/tmp_upload/}")
	private  String tmp_url;
	@Value("${real_url:/resources/upload/}")
	private  String real_url;
	//正式路径
	@Value("${real_path:}")
	private  String real_path;

	@Value("${local_url:}")
	private  String localUrl;
	
	@Value("${file_server_url:}")
	private  String fileServerUrl;
	
	private static final Logger logger = LoggerFactory.getLogger(TNewsServiceImpl.class);
	
	/**
	 * 根据文件的访问路径转移到文件服务器
	 */
	@Override
	public boolean transferFileToImage(String image,HttpServletRequest req) throws Exception {
		boolean flag = false;
		//物理路径
		String sourcePath = req.getSession().getServletContext().getRealPath("/");
    	String tmp_path = sourcePath + tmp_url; //临时存储路径
		try {
			if(!image.equals("")){//有图片，把图片转移到正式的文件夹下
				String[] imageArr = image.split("#");
				for (int i = 0; i < imageArr.length; i++) {
					String imageUrl = imageArr[i];
					/*if(StringUtils.isNotBlank(imageUrl)){
						logger.info("imageUrl---------------------------"+imageUrl+"----------------");
						if(imageUrl.indexOf(tmp_url)>-1){
							imageUrl = imageUrl.substring(imageUrl.indexOf(tmp_url));
							String backUrl = imageUrl.replaceAll(tmp_url, "");
							String oldPath = tmp_path + backUrl;
							String newPath = real_path + backUrl;
							logger.info("file old path==============================+"+oldPath);
							logger.info("file new  path==============================+"+newPath);
							FileUtil.transferFile(oldPath, newPath);//转移
						}
					}*/
				}
			}
			flag = true;
		} catch (Exception e) {
			flag = false;
			throw e;
		}
		return flag;
	}
	

	/**
	 * 临时访问路径
	 */
	@Override
	public String tmpUrl() throws Exception {
		return this.tmp_url;
	} 
	
	/**
	 * 正式访问路径
	 */
	@Override
	public String realUrl() throws Exception {
		return this.real_url;
	} 
 
	/**
	 * 正式存储路径
	 */
	@Override
	public String realPath() throws Exception {
		return this.real_path;
	}

	
	/**
	 * 本地地址
	 */
	@Override 
	public String localUrl() throws Exception{
		return this.localUrl;
	}
	
	/**
	 * 文件服务器访问路径
	 */
	@Override
	public String fileServerUrl() throws Exception{
		return this.fileServerUrl;
	}
}
