package com.yong.service.xj;

import com.yong.model.xj.TAccountMsg;
import com.yong.vo.ResulVO;

import javax.servlet.http.HttpSession;

public interface TAccountMsgService {

    public ResulVO queryAccountByUid(HttpSession session);

    public ResulVO add(TAccountMsg accountMsg);

    public ResulVO selectByExample(Integer start, Integer pageSize);

    public ResulVO update(TAccountMsg accountMsg);

    public ResulVO queryMyLoan(HttpSession session);
}
