package com.yong.service.xj;

import com.yong.commons.utils.ResulVOUtils;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.ApplyInfoDTO;

import javax.servlet.http.HttpSession;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/23 23:38
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/23	   邱良堃		    		新建
 * </pre>
 */
public interface LoanInfoService {

    public ResulVO add(ApplyInfoDTO applyInfoDTO, HttpSession session);

    public ResulVO edit(ApplyInfoDTO applyInfoDTO, HttpSession session);

    public ResulVO queryById(Long id);


}
