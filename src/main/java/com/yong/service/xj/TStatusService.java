package com.yong.service.xj;

import com.yong.vo.ResulVO;

import java.util.List;

public interface TStatusService {

    ResulVO queryStatusList(Integer queryType);

}
