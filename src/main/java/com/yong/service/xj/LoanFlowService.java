package com.yong.service.xj;

import com.yong.vo.ResulVO;

import javax.servlet.http.HttpSession;

public interface LoanFlowService {

    public ResulVO queryPageList(Integer start, Integer pageSize, HttpSession session);

}
