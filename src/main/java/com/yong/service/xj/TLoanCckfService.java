package com.yong.service.xj;

import com.yong.model.xj.TLoanCckf;
import com.yong.vo.ResulVO;

/**
 * description: TLoanCckfService <br>
 * date: 2019/12/17 23:02 <br>
 * author: LKQiu <br>
 * version: 1.0 <br>
 */
public interface TLoanCckfService {

    ResulVO findLoanCckf();

    ResulVO findLoanCckf(String index);

    ResulVO updateLoanCckf(TLoanCckf cckf);

    ResulVO saveLoanCckf(TLoanCckf cckf);
}
