package com.yong.service.xj;

import com.yong.model.xj.TContract;
import com.yong.vo.ResulVO;

import java.util.List;

public interface TContractService {

    public List<TContract> findParant();

    public ResulVO update(TContract tContract);

    public ResulVO findByParant(Long parantId);

    public ResulVO add(TContract tContract);

    public ResulVO remove(Long conId);

    public TContract findById(Long conId);

}
