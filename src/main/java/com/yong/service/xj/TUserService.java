package com.yong.service.xj;

import com.yong.model.xj.TUser;
import com.yong.vo.ResulVO;
import com.yong.vo.Result;
import com.yong.vo.xj.UserDTO;

import javax.servlet.http.HttpSession;

public interface TUserService {

    public ResulVO userLogin(HttpSession session, String loginPhone, String password);

    public ResulVO save(TUser user, HttpSession session);

    public ResulVO remove(Long uid);

    public ResulVO findById(Long uid);

}
