package com.yong.service.xj.impl;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.DateFormatUtil;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.config.xj.Contant;
import com.yong.dao.xj.TAccountMsgMapper;
import com.yong.dao.xj.TApplyInfoMapper;
import com.yong.model.xj.TAccountMsg;
import com.yong.model.xj.TApplyInfo;
import com.yong.service.xj.TAccountMsgService;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.TAccountMsgDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/31 15:22
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/31	   邱良堃		    		新建
 * </pre>
 */
@Service
public class TAccountmsgServiceImpl implements TAccountMsgService {

    @Autowired
    private TAccountMsgMapper accountMsgMapper;

    @Autowired
    private TApplyInfoMapper applyInfoMapper;

    @Override
    public ResulVO queryAccountByUid(HttpSession session) {

        Long userId = (Long) session.getAttribute("user_Id");
        TAccountMsg accountMsg = accountMsgMapper.queryByUid(userId);
        if (accountMsg != null && accountMsg.getId() != null) {
            TAccountMsgDTO accountMsgDTO = new TAccountMsgDTO();
            BeanUtils.copyProperties(accountMsg, accountMsgDTO);
            //获取用户当前订单的备注
            List<TApplyInfo> applyInfoList = applyInfoMapper.queryPageList(userId);
            if (applyInfoList != null && applyInfoList.size() > 0) {
                TApplyInfo index = applyInfoList.get(0);
                if (index.getRemark() != null && index.getRemark() != "") {
                    accountMsgDTO.setRemark(index.getRemark());
                }
            }
            return ResulVOUtils.success(accountMsgDTO);
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "获取异常");
    }

    @Override
    public ResulVO add(TAccountMsg accountMsg) {

        this.setAccountMsg(accountMsg);
        int i = accountMsgMapper.add(accountMsg);
        if (i > 0) {
            return ResulVOUtils.success();
        } else {
            return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "插入失败");
        }
    }

    @Override
    public ResulVO update(TAccountMsg accountMsg) {
        return null;
    }

    @Override
    public ResulVO selectByExample(Integer start, Integer pageSize) {
        return null;
    }

    @Override
    public ResulVO queryMyLoan(HttpSession session) {
        //根据用户ID取出贷款记录
        Long userId = (Long) session.getAttribute("user_Id");
        List<TApplyInfo> applyInfoList = applyInfoMapper.queryPageList(43477574580L);

        List<Map> result = new ArrayList<>();
        //处理数据
        applyInfoList.forEach(applyInfo -> {
            if (this.isFinish(applyInfo.getStatus())) {
                HashMap<String, Object> re = new HashMap<>();
                re.put("JkAmount", applyInfo.getJkAmount());
                re.put("jkDate", DateFormatUtil.getDateString(applyInfo.getCreateDate()));
                result.add(re);
            }
        });
        return ResulVOUtils.success(result);
    }


    public Long setAccountMsg(TAccountMsg accountMsg) {
        long min = 10000000000L;
        long max = 99999999999L;
        long acId = min + (((long) (new Random().nextDouble() * (max - min))));
        accountMsg.setId(acId);
        accountMsg.setApplicationCount(0L);
        accountMsg.setAbleCase(0L);
        accountMsg.setTakeCase(0L);
        accountMsg.setWaitCase(0L);
        return acId;
    }

    //判断流程是否成功借款
    public boolean isFinish(String status) {
        for (String index : Contant.isFinish) {
            if (index.equals(status)) {
                return true;
            }
        }
        return false;
    }
}
