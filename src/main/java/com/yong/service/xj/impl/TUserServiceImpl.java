package com.yong.service.xj.impl;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.DateFormatUtil;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.dao.xj.TUserMapper;
import com.yong.model.xj.TAccountMsg;
import com.yong.model.xj.TUser;
import com.yong.service.xj.TUserService;
import com.yong.vo.ResulVO;
import com.yong.vo.Result;
import com.yong.vo.xj.UserDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.*;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/24 22:32
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/24	   邱良堃		    		新建
 * </pre>
 */
@Service
public class TUserServiceImpl implements TUserService {

    @Autowired
    private TUserMapper userMapper;

    @Autowired
    private TAccountmsgServiceImpl tAccountmsgService;

    @Override
    public ResulVO userLogin(HttpSession session, String loginPhone, String password) {

        TUser user = userMapper.userLogin(loginPhone);
        if (user != null && user.getPassword().equals(password)) {
            //设置返回值
            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(user, userDTO);
            //设置Session
            session.setAttribute("user_Id", user.getUid());
            return ResulVOUtils.success(userDTO);
        } else {
            return ResulVOUtils.error(704, "用户名密码错误");
        }
    }


    @Override
    public ResulVO save(TUser user, HttpSession session) {

        if (user != null && user.getLoginPhone() != null && user.getPassword() != null) {
            Long uid = this.setUserInfo(user);

            Integer theOne = userMapper.theOne(user.getLoginPhone());
            if (theOne > 0) {
                return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "该账户已经注册过了");
            }
            //插入数据库
            int i = userMapper.save(user);
            if (i > 0) {
                //放入session
                session.setAttribute("user_Id", uid);
                //新增账户
                TAccountMsg accountMsg = new TAccountMsg();
                accountMsg.setUserId(uid);
                tAccountmsgService.add(accountMsg);
                return ResulVOUtils.success("注册成功");
            } else {
                return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "注册失败");
            }
        } else {
            return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "手机或密码为空");
        }
    }

    @Override
    public ResulVO remove(Long uid) {
        return null;
    }

    @Override
    public ResulVO findById(Long uid) {

        TUser user = userMapper.findById(uid);
        Map<String, Object> result = new HashMap<>();
        result.put("loginPhone", user.getLoginPhone());
        result.put("createTime", DateFormatUtil.getDateString(user.getCreate_Time()));
        return ResulVOUtils.success(result);
    }

    public Long setUserInfo(TUser user) {
        long min = 10000000000L;
        long max = 99999999999L;
        long uid = min + (((long) (new Random().nextDouble() * (max - min))));
        user.setUid(uid);

        user.setIsDelete(1);
        user.setCreate_Time(new Timestamp(System.currentTimeMillis()));
        user.setLastUpdateTime(new Timestamp(System.currentTimeMillis()));
        return uid;
    }
}
