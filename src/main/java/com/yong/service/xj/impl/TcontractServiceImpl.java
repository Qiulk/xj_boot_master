package com.yong.service.xj.impl;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.dao.xj.TContractMapper;
import com.yong.model.xj.TApplyInfo;
import com.yong.model.xj.TContract;
import com.yong.service.xj.TContractService;
import com.yong.vo.ResulVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/11 11:44
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/11	   邱良堃		    		新建
 * </pre>
 */
@Service
public class TcontractServiceImpl implements TContractService {

    @Autowired
    private TContractMapper tContractMapper;

    @Override
    public List<TContract> findParant() {
        List<TContract> tContracts = tContractMapper.findParant();
        return tContracts;
    }

    @Override
    public ResulVO findByParant(Long parantId) {
        List<TContract> tContracts = tContractMapper.findByParant(parantId);
        return ResulVOUtils.success(tContracts);
    }

    @Override
    public ResulVO update(TContract tContract) {
        int i = tContractMapper.update(tContract);
        if (i > 0) {
            return ResulVOUtils.success();
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "更新失败");
    }

    @Override
    public ResulVO add(TContract tContract) {
//        tContract
        this.setFiled(tContract);
        int i = tContractMapper.add(tContract);
        if (i > 0) {
            return ResulVOUtils.success();
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "插入失败");
    }

    @Override
    public ResulVO remove(Long conId) {
        int i = tContractMapper.remove(conId);
        if (i > 0) {
            return ResulVOUtils.success();
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "移除失败");
    }

    @Override
    public TContract findById(Long conId) {
        return tContractMapper.findById(conId);
    }


    private void setFiled(TContract tContract) {

        long min = 10000000000L;
        long max = 99999999999L;
        long id = min + (((long) (new Random().nextDouble() * (max - min))));
        tContract.setId(id);
    }

}
