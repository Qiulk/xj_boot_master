package com.yong.service.xj.impl;

import com.github.pagehelper.PageHelper;
import com.yong.commons.enums.LoanFlowStatus;
import com.yong.commons.utils.EnumUtil;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.config.xj.Contant;
import com.yong.dao.xj.LoanInfoMapper;
import com.yong.dao.xj.TApplyInfoMapper;
import com.yong.model.xj.TApplyInfo;
import com.yong.service.xj.LoanFlowService;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.LoanFlowDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/28 11:20
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/28	   邱良堃		    		新建
 * </pre>
 */
@Service
public class LoanFlowServiceImpl implements LoanFlowService {

    @Autowired
    private TApplyInfoMapper applyInfoMapper;

    @Override
    public ResulVO queryPageList(Integer start, Integer pageSize, HttpSession session) {
        //设置查询条件
        Long user_id = (Long) session.getAttribute("user_Id");
        PageHelper.startPage(start, pageSize);
        List<TApplyInfo> applyInfoList = applyInfoMapper.queryPageList(user_id);
        //处理数据
        List<LoanFlowDTO> loanFlowDTOS = new ArrayList<>();
        if (applyInfoList != null && applyInfoList.size() > 0) {
            applyInfoList.forEach(applyInfo -> {
                LoanFlowDTO dto = new LoanFlowDTO();
                BeanUtils.copyProperties(applyInfo, dto);
                dto.setNowNode(this.getStatus(Integer.parseInt(applyInfo.getStatus())));
                boolean isReadOnly = Contant.getReadOnly(applyInfo.getStatus());
                dto.setIsEdit(isReadOnly);
                loanFlowDTOS.add(dto);
            });
        }
        return ResulVOUtils.success(loanFlowDTOS);
    }

    private String getStatus(Integer nodeId) {
        //根据id取出对应流程名称
        LoanFlowStatus status = EnumUtil.getByCode(nodeId, LoanFlowStatus.class);
        String nowNode = status.getMessage();
        return nowNode;
    }
}
