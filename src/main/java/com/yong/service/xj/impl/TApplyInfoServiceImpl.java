package com.yong.service.xj.impl;

import com.yong.commons.utils.MD5;
import com.yong.commons.utils.WebConstants;
import com.yong.dao.sys.*;
import com.yong.dao.xj.TAccountMsgMapper;
import com.yong.dao.xj.TApplyInfoMapper;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.*;
import com.yong.model.xj.TAccountMsg;
import com.yong.model.xj.TApplyInfo;
import com.yong.service.home.TSystemStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.yong.service.xj.TApplyInfoService;

import javax.servlet.http.HttpSession;
import java.awt.geom.RectangularShape;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TApplyInfoServiceImpl implements TApplyInfoService {
    @Autowired
    private TApplyInfoMapper applyInfoMapper;

    @Autowired
    private TSystemStatusService systemStatusService;

    @Autowired
    private TAccountMsgMapper accountMsgMapper;

    String xyszr_code;
//	/** 读取配置文件的值，冒号后面为没有此配置项时的默认值 */
//	@Value("${email.host:}")
//	private String emailHost;
//	@Value("${email.account:}")
//	private String emailAccount;
//	@Value("${email.password:}")
//	private String emailPassword;
//	/** 重置的密码 */
//	@Value("${reset.password:654321}")
//	private String resetPassword;
//	@Value("${email.url:}")
//	private String emailUrl;
//	@Value("${email.title:}")
//	private String emailTitle;

    private static final Logger logger = LoggerFactory.getLogger(TApplyInfoServiceImpl.class);

//	@Override
//	public String selectUserByLogin(Criteria criteria) throws Exception{
//		String userName = criteria.getAsString("userName");
//		String passwordIn = criteria.getAsString("passwordIn");
//		// 条件查询
//		List<TSysUser> list = tSysUserMapper.loginCheckUserName(userName);
//		if (null == list || list.size() != 1) {
//			// 没有此用户名
//			return "00";
//		}
//		TSysUser dataBaseUser = list.get(0);
//		String md5Pwd = MD5.encrypt(passwordIn, dataBaseUser.getPkSysUser().toString());
//		if (!md5Pwd.equals(dataBaseUser.getPassword())) {
//			// 密码不正确
//			return "11";
//		}
//		// controller中取出放到session中
//		criteria.put("baseUser", dataBaseUser);
//		return "01";
//	}
//
//
//	@Override
//	public int updateByPrimaryKey(TSysUser sysUser) throws Exception{
//		return tSysUserMapper.update(sysUser);
//	}

    @Override
    public ComboPager queryUserByPager(Criteria example, Pager pager)
            throws Exception {
        int totalRows = applyInfoMapper.countByExample(example);
        pager.setTotalRows(totalRows);
        example.setStartRow((pager.getCurrentPage() - 1) * pager.getPageSize());
        example.setEndRow(pager.getPageSize());
        List<TApplyInfo> rs = applyInfoMapper.selectByExample(example);
        if (rs.size() > 0) {
            Map statusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.APPLY_STATUS);
            for (int i = 0; i < rs.size(); i++) {
                TApplyInfo obj = (TApplyInfo) rs.get(i);
                String statusName = (String) statusMap.get(String.valueOf(obj.getStatus()));
                obj.setStatusName(statusName);
            }
        }
        ComboPager comboPager = new ComboPager();
        comboPager.setPager(pager);
        comboPager.setRs(rs);
        return comboPager;
    }

    @Override
    public ComboPager takeCachqueryUserByPager(Criteria example, Pager pager)
            throws Exception {
        int totalRows = applyInfoMapper.takeCashCountByExample(example);
        pager.setTotalRows(totalRows);
        example.setStartRow((pager.getCurrentPage() - 1) * pager.getPageSize());
        example.setEndRow(pager.getPageSize());
        List<TApplyInfo> rs = applyInfoMapper.takeCashSelectByExample(example);
        if (rs.size() > 0) {
            Map statusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.APPLY_STATUS);
            for (int i = 0; i < rs.size(); i++) {
                TApplyInfo obj = (TApplyInfo) rs.get(i);
                String statusName = (String) statusMap.get(String.valueOf(obj.getStatus()));
                obj.setStatusName(statusName);
            }
        }
        ComboPager comboPager = new ComboPager();
        comboPager.setPager(pager);
        comboPager.setRs(rs);
        return comboPager;
    }

    //	@Override
//	public List getUserListByUserName(String userName) throws Exception {
//		return tSysUserMapper.loginCheckUserName(userName);
//	}
//	/**
//	 * 保存用户和用户角色
//	 */
//	@Override
//	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
//	public void saveUser(TSysUser sysUser, String[] pkRoleArray)
//			throws Exception {
//		Long pkSysUser = tSysUserMapper.seqSysUser();
//		sysUser.setCreateDate(new Date());
//		sysUser.setPkSysUser(pkSysUser);
//		//
//		String password = sysUser.getPassword();
//		password = MD5.encrypt(password, sysUser.getUserName());//加密
//		sysUser.setPassword(password);
//		tSysUserMapper.save(sysUser);
//		for (int i = 0; i < pkRoleArray.length; i++) {
//			TSysUserRole userRole = new TSysUserRole();
//			long pkUserRole = tSysUserMapper.seqSysUser();
//			userRole.setPkUserRole(pkUserRole);
//			userRole.setFkSysUser(pkSysUser);
//			userRole.setFkRole(Long.parseLong(pkRoleArray[i]));
//			userRole.setCreateBy(sysUser.getCreateBy());
//			tSysUserRoleMapper.save(userRole);
//		}
//	}
//
    @Override
    public TApplyInfo getUserInfoByPk(Long pkSysUser) throws Exception {
        List list = applyInfoMapper.get(pkSysUser);
        if (list.size() > 0) {
            TApplyInfo use = (TApplyInfo) list.get(0);
            return use;
        } else {
            return null;
        }
    }

    /**
     * 修改用户和用户角色
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void modify(TApplyInfo obj) throws Exception {


        String status = obj.getStatus();
        if ("1".equals(status)) {
            //审核通过，给审核账户增加可提现余额，并把正在审核删除
            TApplyInfo oldAppInfo = applyInfoMapper.queryById(obj.getPkId());
            TAccountMsg tAccountMsg = accountMsgMapper.queryByUid(oldAppInfo.getUser_id());

            tAccountMsg.setAbleCase(tAccountMsg.getAbleCase() + tAccountMsg.getApplicationCount());
            tAccountMsg.setApplicationCount(0L);

            accountMsgMapper.update(tAccountMsg);
        }
        if ("54".equals(status) || "2".equals(status)) {
            TApplyInfo oldAppInfo = applyInfoMapper.queryById(obj.getPkId());
            TAccountMsg tAccountMsg = accountMsgMapper.queryByUid(oldAppInfo.getUser_id());

            tAccountMsg.setTakeCase(tAccountMsg.getAbleCase() + tAccountMsg.getTakeCase());
            tAccountMsg.setAbleCase(0L);
            accountMsgMapper.update(tAccountMsg);
        }
        applyInfoMapper.update(obj);
    }
//
//	@Override
//	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
//	public void deleteUserAndUserRoleByPkSysUser(String[] pkSysUserArray,
//												 String updateBy) throws Exception {
//		for (int i = 0; i < pkSysUserArray.length; i++) {
//			Long pkSysUser = Long.parseLong(pkSysUserArray[i].trim());
//			tSysUserMapper.remove(pkSysUser);
//			//删除用户角色
//			tSysUserRoleMapper.remove(pkSysUser);
//			//
//			TSysOperateLog sysLog = new TSysOperateLog();
//			sysLog.setLogId(sysOperateLogMapper.seqAlways());
//			sysLog.setUserId(Long.parseLong(updateBy));
//			sysLog.setLogType(2L);
//			sysLog.setLogTab("T_SYS_USER");
//			sysLog.setDataId(pkSysUser);
//			sysLog.setLogSource(WebConstants.LOG_SOURCE);
//			sysOperateLogMapper.save(sysLog);
//		}
//	}
//
//	@Override
//	public Integer checkOldPwd(Long pkSysUser, String oldPwd) throws Exception {
//		return tSysUserMapper.checkOldPwd(pkSysUser, oldPwd);
//	}
//
//	@Override
//	public void modifyPwd(TSysUser sysUser) throws Exception {
//		tSysUserMapper.update(sysUser);
//	}
//
//	@Override
//	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
//	public String findPassword(Criteria example) throws Exception {
//		return "01";
//	}
//
//	/**
//	 *
//	 * 发送邮件
//	 */
//	@SuppressWarnings("finally")
//	@Override
//	public boolean sendEmail(String address, String title, String body) {
//		boolean flag = false;
//		try{
////			//logger.info("address===="+address);
////			//logger.info("title===="+title);
////			Properties props = new Properties();
////			// 定义邮件服务器的地址
////			props.put("mail.smtp.host", emailHost);
////			props.put("mail.smtp.auth", "true");
////			// 取得Session
////			Session session = Session.getDefaultInstance(props, new Authenticator() {
////				public PasswordAuthentication getPasswordAuthentication() {
////					return new PasswordAuthentication(emailAccount, emailPassword);
////				}
////			});
////			MimeMessage message = new MimeMessage(session);
////			// 邮件标题
////			message.setSubject(title);
////			// 发件人的邮件地址
////			message.setFrom(new InternetAddress(emailAccount));
////			// 接收邮件的地址
////			message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
////			// 邮件发送的时间日期
////			message.setSentDate(new Date());
////			// 新建一个MimeMultipart对象用来存放BodyPart对象 related意味着可以发送html格式的邮件
////			Multipart mp = new MimeMultipart("related");
////			// 新建一个存放信件内容的BodyPart对象
////			BodyPart bodyPart = new MimeBodyPart();// 正文
////			// 给BodyPart对象设置内容和格式/编码方式
////			bodyPart.setContent(body, "text/html;charset=utf-8");
////			// 将BodyPart加入到MimeMultipart对象中
////			mp.addBodyPart(bodyPart);
////			// 设置邮件内容
////			message.setContent(mp);
////			// 发送邮件
////			Transport.send(message);
////			logger.info("向邮件地址:{}发送邮件成功！",address);
//			flag = true;
//		}catch(Exception e){
//			e.printStackTrace();
//			logger.error("TSysUserServiceImpl.java-sendEmail-Exception:"+e);
//		}finally{
//			return flag;
//		}
//	}
//
//
//	@Override
//	public void updateOpen(TSysUser sysUser) throws Exception {
//		tSysUserMapper.update(sysUser);
//		TSysOperateLog sysLog = new TSysOperateLog();
//		sysLog.setLogId(sysOperateLogMapper.seqAlways());
//		sysLog.setUserId(Long.parseLong(sysUser.getUpdateBy()));
//		sysLog.setLogType(2L);
//		sysLog.setLogTab("T_SYS_USER");
//		sysLog.setDataId(sysUser.getPkSysUser());
//		sysLog.setLogSource(WebConstants.LOG_SOURCE);
//		sysOperateLogMapper.save(sysLog);
//	}
//
//	@Override
//	public List selectByExample(Criteria example) throws Exception {
//		return tSysUserMapper.selectByExample(example);
//	}
//
//	@Override
//	public Integer countByExample(Criteria example) {
//		return tSysUserMapper.countByExample(example);
//	}
}
