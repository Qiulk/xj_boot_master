package com.yong.service.xj.impl;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.config.xj.Contant;
import com.yong.dao.xj.LoanInfoMapper;
import com.yong.dao.xj.TAccountMsgMapper;
import com.yong.dao.xj.TApplyInfoMapper;
import com.yong.model.xj.TAccountMsg;
import com.yong.model.xj.TApplyInfo;
import com.yong.service.xj.LoanInfoService;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.ApplyInfoDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Random;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/23 23:41
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/23	   邱良堃		    		新建
 * </pre>
 */
@Service
public class LoanInfoServiceImpl implements LoanInfoService {

    @Autowired
    private TApplyInfoMapper applyInfoMapper;

    @Autowired
    private LoanInfoMapper loanInfoMapper;

    @Autowired
    private TAccountMsgMapper accountMsgMapper;

    //上传路径
    @Value("${loan_upload_fw_url:}")
    private String fwUrl;

    @Override
    public ResulVO add(ApplyInfoDTO applyInfoDTO, HttpSession session) {

        //判断该用户是否有没完成的借款
        Long userId = (Long) session.getAttribute("user_Id");
        Integer count = loanInfoMapper.isFinishByUserId(userId);
        if (count != null && count > 0) {
            return ResulVOUtils.error(ApiResultStatus.DOUBLE_REQUEST, "您有未完成的订单,不能再发起申请");
        }

        //拷贝
        TApplyInfo applyInfo = new TApplyInfo();
        BeanUtils.copyProperties(applyInfoDTO, applyInfo);
        //添加必要字段
        Long pkid = this.setFiled(applyInfo, session);
        //插入数据库
        int result = applyInfoMapper.add(applyInfo);

        if (result > 0) {
            return ResulVOUtils.success(pkid);
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "发送意外情况,请重新发起");
    }

    @Override
    public ResulVO queryById(Long id) {

        TApplyInfo applyInfo = applyInfoMapper.queryById(id);
        if (applyInfo != null && applyInfo.getPkId() != null) {
            ApplyInfoDTO applyInfoDTO = new ApplyInfoDTO();
            //拷贝
            BeanUtils.copyProperties(applyInfo, applyInfoDTO);

            //判断状态是否可修改
            boolean isReadOnly = Contant.getReadOnly(applyInfo.getStatus());

            if (isReadOnly) {
                applyInfoDTO.setReadOnly(true);
            } else {
                applyInfoDTO.setReadOnly(false);
            }
//            tmpUrl
            applyInfoDTO.setIdcardNegative(applyInfoDTO.getIdcardNegative());
            applyInfoDTO.setIdcardPositive(applyInfoDTO.getIdcardPositive());
            //返回
            return ResulVOUtils.success(applyInfoDTO);
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "发送意外情况,请重试");
    }

    @Override
    public ResulVO edit(ApplyInfoDTO applyInfoDTO, HttpSession session) {

        //拷贝
        TApplyInfo applyInfo = new TApplyInfo();
        BeanUtils.copyProperties(applyInfoDTO, applyInfo);

        int result = 0;

        if (applyInfoDTO.getReadOnly() != null && applyInfoDTO.getReadOnly()) {
            //设置借款金额
            applyInfo.setJkAmount(applyInfoDTO.getYq_acount());

            TApplyInfo OldApplyInfo = applyInfoMapper.queryById(applyInfo.getPkId());
            String oldStatus = OldApplyInfo.getStatus();
            boolean isFrist = false;
            if ("51".equals(oldStatus)) {
                isFrist = true;
            }
            applyInfo.setStatus("52");

            applyInfo.setUpdateDate(new Date());
            result = applyInfoMapper.edit(applyInfo);

            //更新账户中申请中额度
            if (isFrist) {
                Long userId = (Long) session.getAttribute("user_Id");
                TAccountMsg accountMsg = accountMsgMapper.queryByUid(userId);
                Long count = accountMsg.getApplicationCount() + applyInfo.getYq_acount();
                accountMsg.setApplicationCount(count);
                accountMsgMapper.update(accountMsg);
            }
        } else {
            result = applyInfoMapper.edit(applyInfo);
        }
        if (result > 0) {
            return ResulVOUtils.success();
        }


        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "发送意外情况,请重新发起");
    }

    private Long setFiled(TApplyInfo applyInfo, HttpSession session) {

        long min = 10000000000L;
        long max = 99999999999L;
        long pkId = min + (((long) (new Random().nextDouble() * (max - min))));
        //id
        applyInfo.setPkId(pkId);
        //createTime
        applyInfo.setCreateDate(new Date());
        //updateTime
        applyInfo.setUpdateDate(new Date());
        //user_id
        applyInfo.setUser_id((Long) session.getAttribute("user_Id"));
        //status
        applyInfo.setStatus("51");
        return pkId;
    }

}
