package com.yong.service.xj.impl;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.dao.xj.TLoanCckfMapper;
import com.yong.model.xj.TLoanCckf;
import com.yong.service.xj.TLoanCckfService;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.CckfVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * description: TLoanCckfServiceImpl <br>
 * date: 2019/12/17 23:02 <br>
 * author: LKQiu <br>
 * version: 1.0 <br>
 */
@Service
public class TLoanCckfServiceImpl implements TLoanCckfService {

    @Autowired
    private TLoanCckfMapper cckfMapper;

    @Override
    public ResulVO findLoanCckf() {

        List<TLoanCckf> cckfs = cckfMapper.findLoanCckf();
        CckfVO cckfVO;
        if (cckfs != null && cckfs.size() > 0) {
            cckfVO = new CckfVO();
            cckfVO.setWebid(cckfs.get(0).getWebid());
            cckfVO.setWc(cckfs.get(0).getWc());
            return ResulVOUtils.success(cckfVO);
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "未找到CC客服ID");
    }

    @Override
    public ResulVO findLoanCckf(String index) {
        List<TLoanCckf> cckfs = cckfMapper.findLoanCckf();

        if (cckfs != null && cckfs.size() > 0) {
            return ResulVOUtils.success(cckfs.get(0));
        }

        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "未找到CC客服ID");
    }

    @Override
    public ResulVO updateLoanCckf(TLoanCckf cckf) {
        int i = cckfMapper.updateCckf(cckf);
        if (i > 0) {
            return ResulVOUtils.success();
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "修改失败");
    }

    @Override
    public ResulVO saveLoanCckf(TLoanCckf cckf) {
        return null;
    }
}
