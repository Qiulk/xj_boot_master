package com.yong.service.xj.impl;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.dao.xj.TAccountMsgMapper;
import com.yong.dao.xj.TApplyInfoMapper;
import com.yong.model.xj.TAccountMsg;
import com.yong.model.xj.TApplyInfo;
import com.yong.service.xj.WalletService;
import com.yong.vo.ResulVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/5 21:08
 * @versison 1.0.0
 * 修改记录X
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/05	   邱良堃		    		新建
 * </pre>
 */
@Service
public class WalletServiceImpl implements WalletService {

    @Autowired
    private TApplyInfoMapper applyInfoMapper;

    @Autowired
    private TAccountMsgMapper accountMsgMapper;

    @Override
    public ResulVO takeCash(HttpSession session) {
        Long userId = (Long) session.getAttribute("user_Id");
        List<TApplyInfo> applyInfoList = applyInfoMapper.queryPageList(userId);
        Boolean haveTake = false;
        for (TApplyInfo applyInfo : applyInfoList) {
            String status = applyInfo.getStatus();
            if ("1".equals(status)) {
                haveTake = true;
                TAccountMsg accountMsg = accountMsgMapper.queryByUid(userId);
                if (!applyInfo.getYq_acount().equals(accountMsg.getAbleCase())) {
                    return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "借款值与取现金额不一致");
                }
                if (accountMsg.getAbleCase() < 10) {
                    return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "没有可提现金额");
                }
                accountMsg.setWaitCase(accountMsg.getAbleCase() + accountMsg.getWaitCase());
                accountMsg.setAbleCase(0L);
                accountMsgMapper.update(accountMsg);
                applyInfo.setStatus("53");
                applyInfoMapper.update(applyInfo);
                return ResulVOUtils.success();
            }
        }
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "没有可提现金额");
    }
}
