package com.yong.service.xj.impl;

import com.alibaba.fastjson.JSONObject;
import com.yong.commons.SpringUtils.xssUtils.TsRequest;
import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.enums.SMSApiEnums;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.commons.utils.SMSUtil;
import com.yong.dao.xj.TSMSApiMapper;
import com.yong.model.xj.TAccountMsg;
import com.yong.model.xj.TSmsContent;
import com.yong.service.xj.SMSApiService;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.SMSApiBaseDTO;
import com.yong.vo.xj.SMSApiSimpleDTO;
import com.yong.vo.xj.SMSApiVarCodeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ObjectUtils;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/12/16 14:27
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/12/16	   邱良堃		    		新建
 * </pre>
 */
@Service
public class SMSApiServiceImpl implements SMSApiService {

    @Value("${sms_url_var:}")
    private String sms_url_var;

    @Value("${sms_url_simple:}")
    private String sms_url_simple;

    @Value("${sms_companyVarcode:}")
    private String sms_companyVarcode;


    @Autowired
    private TSMSApiMapper tsmsApiMapper;


    @Override
    public ResulVO sendSMSVarCode(SMSApiVarCodeDTO dto) {

        Map params = new HashMap();//请求参数

        params.put("phoneNum", dto.getPhoneNum());//接收短信的手机号码
        params.put("companyVarCode", sms_companyVarcode);//短信模板ID，请参考个人中心短信模板设置

        if (dto != null && dto.getPhoneNum() != null) {
            try {
                TSmsContent content = new TSmsContent();
                String result = SMSUtil.net(sms_url_var, params, "GET");
                JSONObject object = JSONObject.parseObject(result);
                //用户ID
                content.setUserId(dto.getUserId());
                //用户phoneNum
                content.setPhoneNum(dto.getPhoneNum());
                //发送type
                content.setSmsType(SMSApiEnums.SMSTYPE_VARCODE);
                if (object.get("code").equals(200)) {
                    //返回值
                    content.setBackCode(200);
                    //返回信息
                    content.setBackMsg((String) object.get("msg"));
                    //返回验证码
                    content.setContent(((Integer) object.get("rs")).toString());
                    //设置状态
                    content.setStatus(SMSApiEnums.SMS_STATUS_SEND);
                    saveTSmsContent(content);
                    //修改同一个手机号其他号码的验证状态
                    updateTsmsContentStatus(dto.getPhoneNum());
                    return ResulVOUtils.success();
                } else {
                    //返回值
                    content.setBackCode(500);
                    //设置状态
                    content.setStatus(SMSApiEnums.SMS_STATUS_FAIL);
                    //返回信息
                    content.setBackMsg(object.get("msg") == null ? "" : (String) object.get("msg"));
                    saveTSmsContent(content);
                    return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "调用短信出错");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "调用短信出错");
        } else {
            return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "手机号为空");
        }

    }

    @Override
    public ResulVO sendSMSSimple(SMSApiSimpleDTO dto) {
        return null;
    }

    @Override
    public ResulVO authVarCode(String phoneNum, String varCode) {
        List<TSmsContent> content = tsmsApiMapper
                .findByParams(SMSApiEnums.SMS_STATUS_SEND.toString(), SMSApiEnums.SMSTYPE_VARCODE, phoneNum);
        if (content != null && content.size() > 0) {
            boolean result = varCode.equals(content.get(0).getContent());
            if (result) {
                return null;
            } else {
                return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "验证码错误!");
            }
        } else {
            return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "验证码错误!");
        }
    }

    public boolean updateTsmsContentStatus(String phoneNum) {
        List<TSmsContent> content = tsmsApiMapper
                .findByParams(SMSApiEnums.SMS_STATUS_SEND.toString(), SMSApiEnums.SMSTYPE_VARCODE, phoneNum);
        if (content != null && content.size() > 1) {
            for (int i = 1; i < content.size(); i++) {
                content.get(i).setStatus(SMSApiEnums.SMS_STATUS_TIMEOUT);
                content.get(i).setUpdateTime(new Date());
            }
        } else {
            return true;
        }
        int i = tsmsApiMapper.updateTSmsContent(content);
        if (i > 0) {
            return true;
        }
        return false;
    }

    public boolean saveTSmsContent(TSmsContent content) {
        long min = 10000000000L;
        long max = 99999999999L;
        long acId = min + (((long) (new Random().nextDouble() * (max - min))));
        content.setId(acId);
        content.setUpdateTime(new Date());
        content.setCreateTime(new Date());
        int i = tsmsApiMapper.saveTSmsContent(content);

        if (i > 0) {
            return true;
        }
        return false;
    }

}
