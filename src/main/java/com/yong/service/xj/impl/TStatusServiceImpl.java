package com.yong.service.xj.impl;

import com.yong.commons.utils.ResulVOUtils;
import com.yong.config.xj.Contant;
import com.yong.dao.xj.TStatusMapper;
import com.yong.service.xj.TStatusService;
import com.yong.vo.ResulVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/22 15:09
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/22	   邱良堃		    		新建
 * </pre>
 */
@Service
public class TStatusServiceImpl implements TStatusService {

    @Autowired
    private TStatusMapper statusMapper;

    @Override
    public ResulVO queryStatusList(Integer queryType) {
        if (Contant.SATAUS_TYPE_APPROVAL.equals(queryType) || Contant.SATAUS_TYPE_LENDERS.equals(queryType)) {
            return ResulVOUtils.success(statusMapper.queryStatusList("1"));
        } else {
            return ResulVOUtils.error(500, "请求的状态出错");
        }
    }
}
