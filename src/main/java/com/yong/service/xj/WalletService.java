package com.yong.service.xj;

import com.yong.vo.ResulVO;

import javax.servlet.http.HttpSession;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/5 21:08
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/05	   邱良堃		    		新建
 * </pre>
 */
public interface WalletService {

    public ResulVO takeCash(HttpSession session);

}
