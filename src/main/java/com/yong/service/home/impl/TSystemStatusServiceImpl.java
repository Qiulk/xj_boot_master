package com.yong.service.home.impl;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.yong.dao.home.TSystemStatusMapper;
import com.yong.model.home.Criteria;
import com.yong.model.home.TSystemStatus;
import com.yong.service.home.TSystemStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TSystemStatusServiceImpl implements TSystemStatusService {


	@Autowired
	private TSystemStatusMapper tSystemStatusMapper;
	/**
	 * 获取字典map
	 */
	@Override
	public Map statusCodeAndStatusNameMap(String statusType) throws Exception {
		Map map = new LinkedHashMap();
		List list = this.queryByStatusType(statusType);
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			TSystemStatus status = (TSystemStatus) iterator.next();
			map.put(status.getStatusCode(), status.getStatusName());
		}
		return map;
	}
	
	@Override
	public List queryByStatusType(String statusType) throws Exception{
		return tSystemStatusMapper.queryByStatusType(statusType);
	}

	@Override
	public List selectByContion(Criteria cri) throws Exception {
		return tSystemStatusMapper.selectByCondition(cri);
	}


	@Override
	public TSystemStatus get(String statusType) throws Exception {
		Criteria cri = new Criteria();
		cri.clear();
		cri.put("statusType",statusType);
		List<TSystemStatus> list = tSystemStatusMapper.selectByCondition(cri);
		TSystemStatus status = null ;
		if(list.size() > 0){
			status = list.get(0);
		}
		return status;
	}

	@Override
	public void delete(String statusType) throws Exception {
		tSystemStatusMapper.remove(statusType);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public void batchSave(List<TSystemStatus> staList,String statusType) throws Exception {
		tSystemStatusMapper.remove(statusType);
		tSystemStatusMapper.batchSave(staList);
	}
}