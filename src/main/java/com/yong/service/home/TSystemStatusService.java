package com.yong.service.home;

import com.yong.model.home.Criteria;
import com.yong.model.home.TSystemStatus;

import java.util.List;
import java.util.Map;


public interface TSystemStatusService {
	/**
	 * 获取字典map
	 */
	public Map statusCodeAndStatusNameMap(String statusType) throws Exception;
	
	public List<TSystemStatus> queryByStatusType(String statusType) throws Exception;

	public List<TSystemStatus> selectByContion(Criteria cri) throws Exception;

	public TSystemStatus get(String id)  throws Exception;


	public void delete(String statusType ) throws Exception;
	public void batchSave(List<TSystemStatus> staList,String statusType) throws Exception;
}