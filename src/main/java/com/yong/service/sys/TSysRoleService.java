package com.yong.service.sys;

import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysRole;

import java.util.List;

public interface TSysRoleService {
	/**
	 * 分页查询角色列表
	 */
	public ComboPager queryRoleByPager(Criteria criteria, Pager pager) throws Exception;
	/**
	 * 验证角色名称是否重复
	 */
	public List<TSysRole> checkRoleByRoleName(Criteria criteria) throws Exception;
	/**
	 * 保存角色和功能权限
	 */
	public void saveRole(TSysRole sysRole, String[] pkFunArray)  throws Exception;
	/**
	 * 获取角色信息
	 */
	public TSysRole selectByPrimaryKey(long pkRole) throws Exception;
	/**
	 * 根据主键更新角色信息和角色功能权限
	 */
	public void updateRoleAndRoleFunByPkRole(TSysRole sysRole, String[] pkFunArray) throws Exception;
	/**
	 * 根据主键伪删除角色信息和角色功能权限
	 */
	public void deleteRoleAndRoleFunByPkRole(String[] pkRoleArray, String updateBy) throws Exception;
	
	/**
	 * 查询角色列表
	 */
	public List<TSysRole> queryRoleList(Criteria criteria) throws Exception;
	
	/**
	 * 查询角色列表
	 */
	public List<TSysRole> selectByCondition(Criteria criteria) throws Exception;

	/**
	 * 判断角色是否已分配
	 * @param pkRoleArray
	 * @return true 已分配 false 未分配
	 * @throws Exception
	 */
	public Boolean checkUserROleOn(String[] pkRoleArray) throws Exception;
}