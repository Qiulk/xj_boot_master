package com.yong.service.sys;

import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysOrganization;

import java.util.List;



public interface TSysOrganizationService {
	
	/**
	 * 不同条件查询
	 */
	public List<TSysOrganization> selectByCondition(Criteria example) throws Exception;
	
	/**
	 * 分页查询列表
	 */
	public ComboPager queryListByPager(Criteria example, Pager pager) throws Exception;
	
	/**
	 * 时间+序列
	 */
	public Long seqAlways() throws Exception;
	
	/**
	 * 保存
	 */
	public void save(TSysOrganization obj) throws Exception;
	
	/**
	 * 查看
	 */
	public TSysOrganization get(Long id) throws Exception;
	
	/**
	 * 查看基本信息
	 */
	public TSysOrganization getBaseInfo(Long id) throws Exception;
	
	/**
	 * 查看详细信息
	 */
	public TSysOrganization getDetailInfo(Long id) throws Exception;
	
	/**
	 * 修改
	 */
	public void update(TSysOrganization obj) throws Exception;
	
	/**
	 * 删除
	 */
	public void remove(String[] idsArray, String updateBy) throws Exception;

	/**
	 * 根据条件查询记录总数
	 */
	public Integer countByExample(Criteria example);
}
