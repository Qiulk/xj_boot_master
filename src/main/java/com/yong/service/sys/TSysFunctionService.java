package com.yong.service.sys;

import java.util.List;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysFunction;


public interface TSysFunctionService {
 
	public List queryALLUserFunctionByPKUser(long userId) throws Exception;
	/**
	 * 查询所有菜单
	 * @return
	 * @throws Exception
	 */
	public List queryTSysFunction(Criteria cri) throws Exception;
	
	public List selectByCondition(Criteria cri) throws Exception;

	public List<TSysFunction> queryUserRoleByZw(long roleId) throws Exception;
}