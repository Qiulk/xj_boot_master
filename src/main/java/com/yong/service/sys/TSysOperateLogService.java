package com.yong.service.sys;

import java.util.List;

import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysOperateLog;


public interface TSysOperateLogService {
	
	/**
	 * 不同条件查询
	 */
	public List<TSysOperateLog> selectByCondition(Criteria example) throws Exception;
	
	/**
	 * 分页查询列表
	 */
	public ComboPager queryListByPager(Criteria example, Pager pager) throws Exception;
	
	/**
	 * 时间+序列
	 */
	public Long seqAlways() throws Exception;
	
	/**
	 * 保存
	 */
	public void save(TSysOperateLog obj) throws Exception;
	
	/**
	 * 查看
	 */
	public TSysOperateLog get(Long id) throws Exception;
	
	/**
	 * 查看基本信息
	 */
	public TSysOperateLog getBaseInfo(Long id) throws Exception;
	
	/**
	 * 查看详细信息
	 */
	public TSysOperateLog getDetailInfo(Long id) throws Exception;
	
	/**
	 * 修改
	 */
	public void update(TSysOperateLog obj) throws Exception;
	
	/**
	 * 删除
	 */
	public void remove(String[] idsArray, String updateBy) throws Exception;

}
