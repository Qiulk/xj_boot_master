package com.yong.service.sys;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysUserRole;

import java.util.List;

public interface TSysUserRoleService {
 
	/**
	 * 获取用户角色
	 */
	public List<TSysUserRole> queryUserRoleByPKUser(long userId) throws Exception;

	/**
	 * 不同条件查询
	 */
	public List<TSysUserRole> selectByCondition(Criteria example) throws Exception;

	/**
	 * 根据角色ID 和 组织ID 查询用户ID
	 */
	public List<TSysUserRole> selectByRole(Long fkRole,String orgOneId) throws Exception;
}