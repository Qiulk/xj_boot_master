package com.yong.service.sys.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.yong.dao.sys.TSysOperateLogMapper;
import com.yong.model.sys.TSysOperateLog;
import com.yong.service.sys.TSysOperateLogService;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
//import com.yong.service.TSystemStatusService;


@Service
public class TSysOperateLogServiceImpl implements TSysOperateLogService {

	@Autowired
	private TSysOperateLogMapper sysOperateLogMapper;
	
//	@Autowired
//	private TSystemStatusService systemStatusService;
	
	private static final Logger logger = LoggerFactory.getLogger(TSysOperateLogServiceImpl.class);
	
	/**
	 * 不同条件查询
	 */
	@Override
	public List<TSysOperateLog> selectByCondition(Criteria example) throws Exception {
		return sysOperateLogMapper.selectByCondition(example);
	}

	/**
	 * 分页查询
	 */
	@Override
	public ComboPager queryListByPager(Criteria example, Pager pager)
			throws Exception {
		int totalRows = sysOperateLogMapper.countByExample(example);
		pager.setTotalRows(totalRows);
		example.setStartRow((pager.getCurrentPage()-1)*pager.getPageSize());
		example.setEndRow(pager.getPageSize());
		List rs = sysOperateLogMapper.selectByExample(example);
		//激活状态
//		Map statusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.T_USER_STATUS);
//		for (Iterator iterator = rs.iterator(); iterator.hasNext();) {
//			TGzzhpz obj = (TGzzhpz) iterator.next();
//			String statusName = (String)statusMap.get(String.valueOf(obj.getStatus()));
//			obj.setStatusName(statusName);
//		}
		ComboPager comboPager = new ComboPager();
		comboPager.setPager(pager);
		comboPager.setRs(rs);
		return comboPager;
	}

	/**
	 * 保存
	 */
	@Override
	//@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public void save(TSysOperateLog obj) throws Exception {
		sysOperateLogMapper.save(obj);
		
	}
	
	/**
	 * 查看
	 */
	@Override
	public TSysOperateLog get(Long id) throws Exception {
		List list = sysOperateLogMapper.get(id);
		TSysOperateLog obj = null;
		if(list.size()>0){
			obj = (TSysOperateLog)list.get(0);
			//审核状态
//			Map chkStatusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.T_USER_CHK_STATUS);
//			String chkStatusName = (String)chkStatusMap.get(obj.getChkStatus());
//			obj.setChkStatusName(chkStatusName);
//			//审核人
//			if(StringUtils.isNotBlank(obj.getChkBy())){
//				TSysUser chkUser = tSysUserService.geTGzzhpzInfoByPk(Long.parseLong(obj.getChkBy()));
//				obj.setChkUser(chkUser.geTGzzhpzName());
//			}
		}
		return obj;
	}
	
	/**
	 * 查看基本信息
	 */
	@Override
	public TSysOperateLog getBaseInfo(Long id) throws Exception {
		List list = sysOperateLogMapper.get(id);
		TSysOperateLog obj = null;
		if(list.size()>0){
			obj = (TSysOperateLog)list.get(0);
		}
		return obj;
	}
	
	/**
	 * 查看详细信息
	 */
	@Override
	public TSysOperateLog getDetailInfo(Long id) throws Exception {
		List list = sysOperateLogMapper.get(id);
		TSysOperateLog obj = null;
		if(list.size()>0){
			obj = (TSysOperateLog)list.get(0);
			//审核状态
//			Map chkStatusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.T_USER_CHK_STATUS);
//			String chkStatusName = (String)chkStatusMap.get(obj.getChkStatus());
//			obj.setChkStatusName(chkStatusName);
//			//审核人
//			if(StringUtils.isNotBlank(obj.getChkBy())){
//				TSysUser chkUser = tSysUserService.geTGzzhpzInfoByPk(Long.parseLong(obj.getChkBy()));
//				obj.setChkUser(chkUser.geTGzzhpzName());
//			}
		}
		return obj;
	}
	
	/**
	 * 修改
	 */
	@Override
	//@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public void update(TSysOperateLog obj) throws Exception {
		sysOperateLogMapper.update(obj);
	}
	
	/**
	 * 删除
	 */
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public void remove(String[] idsArray,String updateBy) throws Exception {
		for (int i = 0; i < idsArray.length; i++) {
			sysOperateLogMapper.remove(Long.parseLong(idsArray[i]),updateBy);
		}
	}
	
	/**
	 * 序列
	 */
	@Override
	public Long seqAlways() throws Exception{
		return sysOperateLogMapper.seqAlways();
	}
	
}
