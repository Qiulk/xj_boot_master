package com.yong.service.sys.impl;

import com.yong.commons.utils.DateFormatUtil;
import com.yong.commons.utils.WebConstants;
import com.yong.dao.sys.TSysOperateLogMapper;
import com.yong.dao.sys.TSysRoleFunctionMapper;
import com.yong.dao.sys.TSysRoleMapper;
import com.yong.dao.sys.TSysUserRoleMapper;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysOperateLog;
import com.yong.model.sys.TSysRole;
import com.yong.model.sys.TSysRoleFunction;
import com.yong.service.sys.TSysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TSysRoleServiceImpl implements TSysRoleService {
	@Autowired
	private TSysRoleMapper tSysRoleMapper;
	@Autowired
	private TSysRoleFunctionMapper tSysRoleFunctionMapper;
	@Autowired
	private TSysOperateLogMapper sysOperateLogMapper;
	@Autowired
	private TSysUserRoleMapper sysUserRoleMapper;

	@Override
	public ComboPager queryRoleByPager(Criteria example,Pager pager) throws Exception {
		int totalRows = this.tSysRoleMapper.countByExample(example);
		pager.setTotalRows(totalRows);
		example.setCurrentPage(pager.getCurrentPage());
		example.setPageSize(pager.getPageSize());
		List<TSysRole> rs = this.tSysRoleMapper.selectByExample(example);
		if(rs.size()>0){
			TSysRole role=null;
			for(int i=0;i<rs.size();i++){
				role = rs.get(i);
				String startTimeStr = DateFormatUtil.getDateTimeString(role.getCreateDate(),"yyyy-mm-dd hh:mm:ss");
				role.setCreateDateStr(startTimeStr);
			}
		}
		ComboPager comboPager = new ComboPager();
		comboPager.setPager(pager);
		comboPager.setRs(rs);
		return comboPager;
	}
	
	@Override
	public List<TSysRole> checkRoleByRoleName(Criteria cri) throws Exception {
		return tSysRoleMapper.selectRoleByRoleName(cri);
	}
	/**
	 * 保存角色和角色权限
	 */
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	public void saveRole(TSysRole sysRole, String[] pkFunArray)
			throws Exception {
		long pkRole = tSysRoleMapper.getRoleId();
		sysRole.setPkRole(pkRole);
		tSysRoleMapper.save(sysRole);
		for (int i = 0; i < pkFunArray.length; i++) {
			TSysRoleFunction roleFunction = new TSysRoleFunction();
			long pkRoleFun = tSysRoleMapper.getRoleId();
			roleFunction.setPkRoleFun(pkRoleFun);
			roleFunction.setFkRole(pkRole);
			roleFunction.setFkSysFunction(pkFunArray[i]);
			roleFunction.setCreateBy(sysRole.getCreateBy());
			tSysRoleFunctionMapper.save(roleFunction);
		}
	}

	@Override
	public TSysRole selectByPrimaryKey(long pkRole) throws Exception {
		TSysRole sysRole= tSysRoleMapper.selectByPrimaryKey(pkRole);
		return sysRole;
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	public void updateRoleAndRoleFunByPkRole(TSysRole sysRole, String[] pkFunArray) throws Exception {
		//修改角色信息
		tSysRoleMapper.update(sysRole);
		long pkRole = sysRole.getPkRole();
		//删除旧的角色功能权限
		tSysRoleFunctionMapper.remove(pkRole);
		//新增新的功能权限
		for (int i = 0; i < pkFunArray.length; i++) {
			TSysRoleFunction roleFunction = new TSysRoleFunction();
			long pkRoleFun = tSysRoleMapper.getRoleId();
			roleFunction.setPkRoleFun(pkRoleFun);
			roleFunction.setFkRole(pkRole);
			roleFunction.setFkSysFunction(pkFunArray[i]);
			roleFunction.setCreateBy(sysRole.getUpdateBy());
			tSysRoleFunctionMapper.save(roleFunction);
		}
		//
		TSysOperateLog sysLog = new TSysOperateLog();
		sysLog.setLogId(sysOperateLogMapper.seqAlways());
		sysLog.setUserId(Long.parseLong(sysRole.getUpdateBy()));
		sysLog.setLogType(1L);
		sysLog.setLogTab("T_SYS_ROLE");
		sysLog.setDataId(pkRole);
		sysLog.setLogSource(WebConstants.LOG_SOURCE);
		sysOperateLogMapper.save(sysLog);
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { Exception.class })
	public void deleteRoleAndRoleFunByPkRole(String[] pkRoleArray,String updateBy) throws Exception {
		for (int i = 0; i < pkRoleArray.length; i++) {
			Long pkRole = Long.parseLong(pkRoleArray[i].trim());
			if(pkRole.longValue() == 1000000l &&pkRole.longValue() == 2000000l &&pkRole.longValue() == 3000000l &&pkRole.longValue() == 4000000l&&pkRole.longValue() == 5000000l  ){
				continue;
			}
			tSysRoleMapper.remove(pkRole);
			tSysRoleFunctionMapper.remove(pkRole);
			//
			TSysOperateLog sysLog = new TSysOperateLog();
			sysLog.setLogId(sysOperateLogMapper.seqAlways());
			sysLog.setUserId(Long.parseLong(updateBy));
			sysLog.setLogType(2L);
			sysLog.setLogTab("T_SYS_ROLE");
			sysLog.setDataId(pkRole);
			sysLog.setLogSource(WebConstants.LOG_SOURCE);
			sysOperateLogMapper.save(sysLog);
		}
	}

	@Override
	public List<TSysRole> queryRoleList(Criteria example) throws Exception {
		return tSysRoleMapper.queryRoleList(example);
	}
	
	@Override
	public List<TSysRole> selectByCondition(Criteria criteria) throws Exception {
		return tSysRoleMapper.selectByCondition(criteria);
	}

	@Override
	public Boolean checkUserROleOn(String[] pkRoleArray) throws Exception {
		Criteria cri = new Criteria();
		cri.clear();;
		for (int i=0;i < pkRoleArray.length;i++){
			String pkrol = pkRoleArray[i];
			cri.put("fkRole",pkrol);
			int num = sysUserRoleMapper.countByExample(cri);
			if (num > 0){
				return true;
			}
		}
		return false;
	}
}