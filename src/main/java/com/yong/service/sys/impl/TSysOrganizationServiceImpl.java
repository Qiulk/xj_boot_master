package com.yong.service.sys.impl;

import com.yong.dao.sys.TSysOperateLogMapper;
import com.yong.dao.sys.TSysOrganizationMapper;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysOrganization;
import com.yong.service.home.TSystemStatusService;
import com.yong.service.sys.TSysOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class TSysOrganizationServiceImpl implements TSysOrganizationService {

    @Autowired
    private TSysOrganizationMapper sysOrganizationMapper;

    @Autowired
    private TSystemStatusService systemStatusService;

    @Autowired
    private TSysOperateLogMapper sysOperateLogMapper;

    private static final Logger logger = LoggerFactory.getLogger(TSysOrganizationServiceImpl.class);

    /**
     * 不同条件查询
     */
    @Override
    public List<TSysOrganization> selectByCondition(Criteria example) throws Exception {
        return sysOrganizationMapper.selectByCondition(example);
    }

    /**
     * 分页查询
     */
    @Override
    public ComboPager queryListByPager(Criteria example, Pager pager)
            throws Exception {
        int totalRows = sysOrganizationMapper.countByExample(example);
        pager.setTotalRows(totalRows);
        example.setCurrentPage(pager.getCurrentPage());
        example.setPageSize(pager.getPageSize());
        List rs = sysOrganizationMapper.selectByExample(example);
        //激活状态
//		Map statusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.T_USER_STATUS);
//		for (Iterator iterator = rs.iterator(); iterator.hasNext();) {
//			TGzzhpz obj = (TGzzhpz) iterator.next();
//			String statusName = (String)statusMap.get(String.valueOf(obj.getStatus()));
//			obj.setStatusName(statusName);
//		}
        ComboPager comboPager = new ComboPager();
        comboPager.setPager(pager);
        comboPager.setRs(rs);
        return comboPager;
    }

    /**
     * 保存
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
    public void save(TSysOrganization obj) throws Exception {
        //保存新组织
        sysOrganizationMapper.save(obj);
        //更新上级组织 IS_HAS_CHILD字段
        sysOrganizationMapper.upIsHasChild(obj.getSuperId(),1L);
    }

    /**
     * 查看
     */
    @Override
    public TSysOrganization get(Long id) throws Exception {
        List list = sysOrganizationMapper.get(id);
        TSysOrganization obj = null;
        if (list.size() > 0) {
            obj = (TSysOrganization) list.get(0);
            //审核状态
//			Map chkStatusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.T_USER_CHK_STATUS);
//			String chkStatusName = (String)chkStatusMap.get(obj.getChkStatus());
//			obj.setChkStatusName(chkStatusName);
//			//审核人
//			if(StringUtils.isNotBlank(obj.getChkBy())){
//				TSysUser chkUser = tSysUserService.geTGzzhpzInfoByPk(Long.parseLong(obj.getChkBy()));
//				obj.setChkUser(chkUser.geTGzzhpzName());
//			}
        }
        return obj;
    }

    /**
     * 查看基本信息
     */
    @Override
    public TSysOrganization getBaseInfo(Long id) throws Exception {
        List list = sysOrganizationMapper.get(id);
        TSysOrganization obj = null;
        if (list.size() > 0) {
            obj = (TSysOrganization) list.get(0);
        }
        return obj;
    }

    /**
     * 查看详细信息
     */
    @Override
    public TSysOrganization getDetailInfo(Long id) throws Exception {
        List list = sysOrganizationMapper.get(id);
        TSysOrganization obj = null;
        if (list.size() > 0) {
            obj = (TSysOrganization) list.get(0);
            //审核状态
//			Map chkStatusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.T_USER_CHK_STATUS);
//			String chkStatusName = (String)chkStatusMap.get(obj.getChkStatus());
//			obj.setChkStatusName(chkStatusName);
//			//审核人
//			if(StringUtils.isNotBlank(obj.getChkBy())){
//				TSysUser chkUser = tSysUserService.geTGzzhpzInfoByPk(Long.parseLong(obj.getChkBy()));
//				obj.setChkUser(chkUser.geTGzzhpzName());
//			}
        }
        return obj;
    }

    /**
     * 修改
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
    public void update(TSysOrganization obj) throws Exception {
        sysOrganizationMapper.update(obj);
    }

    /**
     * 删除
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
    public void remove(String[] idsArray, String updateBy) throws Exception {
        for (int i = 0; i < idsArray.length; i++) {
            Long dataId = Long.parseLong(idsArray[i]);
            //需删除的组织
            TSysOrganization org = this.get(dataId);
            if (org != null){
                Long superId = org.getSuperId();
                Criteria cri = new Criteria();
                cri.clear();
                cri.put("superId",superId);
                Integer num = this.countByExample(cri);
                if (num==1){
                    sysOrganizationMapper.upIsHasChild(superId,0L);
                }
            }
            sysOrganizationMapper.remove(dataId, updateBy);

        }
    }

    @Override
    public Integer countByExample(Criteria example) {
        return sysOrganizationMapper.countByExample(example);
    }

    /**
     * 序列
     */
    @Override
    public Long seqAlways() throws Exception {
        return sysOrganizationMapper.seqAlways();
    }

}
