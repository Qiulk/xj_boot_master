package com.yong.service.sys.impl;

import java.util.List;

import com.yong.model.home.Criteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yong.dao.sys.TSysUserRoleMapper;
import com.yong.model.sys.TSysUserRole;
import com.yong.service.sys.TSysUserRoleService;

@Service
public class TSysUserRoleServiceImpl implements TSysUserRoleService {
	@Autowired
	private TSysUserRoleMapper tSysUserRoleMapper;

	private static final Logger logger = LoggerFactory.getLogger(TSysUserRoleServiceImpl.class);

	@Override
	public List<TSysUserRole> queryUserRoleByPKUser(long userId)
			throws Exception {
		return tSysUserRoleMapper.queryUserRoleByPKUser(userId);
	}

	@Override
	public List<TSysUserRole> selectByCondition(Criteria example) throws Exception {
		return tSysUserRoleMapper.selectByCondition(example);
	}

	@Override
	public List<TSysUserRole> selectByRole(Long fkRole, String orgOneId) throws Exception {
		Criteria criteria = new Criteria();
		criteria.clear();
		criteria.put("fkRole", fkRole);
		if(null != orgOneId && !orgOneId.equals("")){
			criteria.put("orgOneId", orgOneId);
		}
		return tSysUserRoleMapper.selectByCondition(criteria);
	}


}