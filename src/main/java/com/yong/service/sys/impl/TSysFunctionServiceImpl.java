package com.yong.service.sys.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import com.yong.dao.sys.TSysRoleMapper;
import com.yong.model.sys.TSysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yong.model.home.Criteria;
import com.yong.dao.sys.TSysFunctionMapper;
import com.yong.dao.sys.TSysUserRoleMapper;
import com.yong.model.sys.TSysFunction;
import com.yong.model.sys.TSysUserRole;
import com.yong.service.sys.TSysFunctionService;

@Service
public class TSysFunctionServiceImpl implements TSysFunctionService {


	@Autowired
	private TSysFunctionMapper tSysFunctionMapper;
	@Autowired
	private TSysUserRoleMapper tSysUserRoleMapper;
	@Autowired
	private TSysRoleMapper tSysRoleMapper;
	
	/**
	 * 根据用户ID查询用户所有功能 
	 * @param atx
	 * @param userid
	 * @return
	 * @throws Exception 24客户经理 16信用社主任
	 */
	@Override 
	public List<TSysFunction> queryALLUserFunctionByPKUser(long userId) throws Exception{
		List<TSysUserRole> userRoleList = tSysUserRoleMapper.queryUserRoleByPKUser(userId);//查询用户拥有的角色
		List<TSysFunction> list = new LinkedList<TSysFunction>();//查询用户所拥有的功能
		if(userRoleList != null){
			for(int i = 0; i < userRoleList.size(); i++){
				TSysUserRole userRole = (TSysUserRole)userRoleList.get(i);
				List<TSysFunction> tmpLstFunction = tSysFunctionMapper.queryTSysRoleFunctionByPKRole(userRole.getFkRole());
				list.addAll(tmpLstFunction);
			}
		}
		List<TSysFunction> normalList = new ArrayList<TSysFunction>(new HashSet<TSysFunction>(list));
		
		List<TSysFunction> appendList = tSysFunctionMapper.queryUserFuncByPKUserAppend(userId);
		normalList.addAll(appendList);
		return normalList;
	}
	/**
	 * 根据职务查询用户的角色
	 * @return
	 * @throws Exception 24客户经理 16信用社主任
	 */
	@Override
	public List<TSysFunction> queryUserRoleByZw(long roleId) throws Exception{
		TSysRole sysRole = tSysRoleMapper.selectByPrimaryKey(roleId);//查询用户拥有的角色
		List<TSysFunction> list = new LinkedList<TSysFunction>();//查询用户所拥有的功能
		List<TSysFunction> tmpLstFunction = tSysFunctionMapper.queryTSysRoleFunctionByPKRole(sysRole.getPkRole());
		list.addAll(tmpLstFunction);
		List<TSysFunction> normalList = new ArrayList<TSysFunction>(new HashSet<TSysFunction>(list));
		return normalList;
	}
	/**
	 * 查询所有菜单
	 */
	@Override
	public List queryTSysFunction(Criteria cri) throws Exception {
		List funList = tSysFunctionMapper.queryTSysFunction(cri);
		return funList;
	}
	
	@Override
	public List selectByCondition(Criteria cri) throws Exception {
		List funList = tSysFunctionMapper.selectByCondition(cri);
		return funList;
	}
	
	

}