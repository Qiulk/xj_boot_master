package com.yong.service.sys.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yong.model.home.Criteria;
import com.yong.dao.sys.TSysRoleFunctionMapper;
import com.yong.model.sys.TSysRoleFunction;
import com.yong.service.sys.TSysRoleFunctionService;

@Service
public class TSysRoleFunctionServiceImpl implements TSysRoleFunctionService {
	@Autowired
	private TSysRoleFunctionMapper tSysRoleFunctionMapper;
	
	@Override
	public List queryRoleFunByPkRole(long pkRole) throws Exception {
		List list = tSysRoleFunctionMapper.queryRoleFunByFkRole(pkRole);
		return list;
	}
 

	/**
	 * 查询角色列表
	 */
	public List<TSysRoleFunction> selectByCondition(Criteria cri) throws Exception{
		List list = tSysRoleFunctionMapper.selectByCondition(cri);
		return list;
	}

	@Override
	public List<TSysRoleFunction> queryRoleFunByFkSysFunction(long fkSysFunction) throws Exception {
		List list = tSysRoleFunctionMapper.queryRoleFunByFkSysFunction(fkSysFunction);
		return list;
	}
}