package com.yong.service.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yong.dao.sys.TSysActionMapper;
import com.yong.model.sys.TSysAction;
import com.yong.model.sys.TSysFunction;
import com.yong.service.sys.PermissionService;
import com.yong.service.sys.TSysFunctionService;

@Service
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private TSysActionMapper tSysActionMapper;
	
	@Override
	public Map<String,String> queryActionByPKUser(TSysFunctionService funService, long userid) throws Exception{
		List list = funService.queryALLUserFunctionByPKUser(userid);
		List listAll = new ArrayList(new HashSet(list));
		Map<String,String> actionMap = null;
		if(listAll != null){
			actionMap = new HashMap<String, String>();
			int size = listAll.size();
			for(int i = 0; i < size; i++){
				TSysFunction fun = (TSysFunction)listAll.get(i);
				String pkFun = fun.getPkSysFunction();
				List actionList = tSysActionMapper.queryActionByFunction(pkFun);
				if(actionList!=null){
					int actionSize = actionList.size();
					for(int j = 0; j < actionSize; j++){
						TSysAction action = (TSysAction)actionList.get(j);
						String pkAction = action.getPkSysAction();
						String actionName = action.getActionName();
						actionMap.put(pkAction, actionName);
					}
				}
			}
		}
		return actionMap;
	}

}
