package com.yong.service.sys;

import java.util.List;

import com.yong.model.home.Criteria;
import com.yong.model.sys.TSysRoleFunction;


public interface TSysRoleFunctionService {
	/**
	 * 根据角色id获取功能菜单
	 */
	public List queryRoleFunByPkRole(long pkRole) throws Exception;
	
	/**
	 * 查询角色列表
	 */
	public List<TSysRoleFunction> selectByCondition(Criteria criteria) throws Exception;

	public List<TSysRoleFunction> queryRoleFunByFkSysFunction(long fkSysFunction)throws Exception;
}