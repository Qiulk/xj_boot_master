package com.yong.service.sys;

import java.util.List;

import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysUser;

public interface TSysUserService {

	/**
	 * 用户登录查找
	 *  
	 * @param criteria
	 * @return 00：失败，01：成功 ,其他情况
	 */
	public String selectUserByLogin(Criteria criteria) throws Exception;
	
	/**
	 * 登录更新登录ip和登录时间
	 */
	public int updateByPrimaryKey(TSysUser record) throws Exception;
	
	/**
	 * 分页查询用户列表
	 */
	public ComboPager queryUserByPager(Criteria example, Pager pager) throws Exception;
	/**
	 * 根据用户名获取用户列表
	 */
	public List getUserListByUserName(String userName) throws Exception;
	/**
	 * 用户列表
	 */
	public List selectByExample(Criteria example) throws Exception;
	/**
	 * 保存用户和用户角色
	 */
	public void saveUser(TSysUser sysUser, String[] pkRoleArray)  throws Exception;
	
	/**
	 * 根据主键获取记录信息
	 */
	public TSysUser getUserInfoByPk(Long pkSysUser)  throws Exception;
	
	/**
	 * 修改用户和用户角色
	 */
	public void modify(TSysUser sysUser, String[] pkRoleArray)  throws Exception;
	
	/**
	 * 根据主键伪删除用户和用户角色
	 */
	public void deleteUserAndUserRoleByPkSysUser(String[] pkSysUserArray, String updateBy) throws Exception;
	/**
	 * 确认旧密码
	 */
	public Integer checkOldPwd(Long pkSysUser, String oldPwd) throws Exception;
	
	/**
	 * 修改密码
	 */
	public void modifyPwd(TSysUser sysUser)  throws Exception;
	/**
	 * 找回密码
	 */
	public String findPassword(Criteria example) throws Exception;
	
	public boolean sendEmail(String address, String title, String body);


    void updateOpen(TSysUser sysUser) throws Exception;

	/**
	 * 根据条件查询记录总数
	 */
	public Integer countByExample(Criteria example);
}