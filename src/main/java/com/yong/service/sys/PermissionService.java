package com.yong.service.sys;

import java.util.Map;

public interface PermissionService {
	public Map<String,String> queryActionByPKUser(TSysFunctionService atx, long userid) throws Exception;
		
}
