package com.yong.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.WebUtils;
import com.yong.model.home.Criteria;
import com.yong.service.sys.TSysFunctionService;
import com.yong.service.sys.TSysRoleFunctionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 后台资源、系统菜单相关
 * 
 * @author hongbo
 * @date 2011-10-31 下午10:16:24
 */
@Controller
@RequestMapping("/admin/function")
public class FunctionController {

	private static final Logger logger = LoggerFactory.getLogger(FunctionController.class);
	@Autowired
	private TSysFunctionService tSysFunctionService;
	@Autowired
	private TSysRoleFunctionService tSysRoleFunctionService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		//binder.registerCustomEditor(Date.class, new DateConvertEditor());
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	/**
	 * 新增角色，获取dtree菜单
	 */
	@RequestMapping(value = "/funTree", method = RequestMethod.POST,produces = "text/html;charset=utf-8")
	@ResponseBody
	public String funTree(HttpSession session) {
		try {
			String sessionUserNo = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(WebUtils.isNull(sessionUserNo)){
				return WebUtils.getData(false,"登录超时",null);
			}
			Criteria cri = new Criteria();
			cri.put("createBy", sessionUserNo);
			List funList = tSysFunctionService.queryTSysFunction(cri);
			return WebUtils.getData(true, "", funList);
		} catch (Exception e) {
			logger.error("funTree-method-Exception: ", e);
			return WebUtils.getData(false, "获取dtree菜单异常", null);
		}
	}
	
	/**
	 * 修改角色，获取dtree菜单
	 */
	@RequestMapping(value = "/queryFunTreeByPkRole", method = RequestMethod.POST)
	@ResponseBody
	public String queryFunTreeByPkRole(HttpServletRequest request,HttpSession session) {
		try {
			String sessionUserNo = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(WebUtils.isNull(sessionUserNo)){
				return WebUtils.getData(false,"登录超时",null);
			}
			String pkRole = request.getParameter("pkRole").trim();
			Criteria cri = new Criteria();
			if(!sessionUserNo.equals("9000000")){
				cri.put("createBy", sessionUserNo);
			}
			List funList = tSysFunctionService.queryTSysFunction(cri);
			List roleFunList = tSysRoleFunctionService.queryRoleFunByPkRole(Long.parseLong(pkRole));
			Map map = new HashMap();
			map.put("funList", funList);
			map.put("roleFunList", roleFunList);
			return WebUtils.getData(true, "", map);
		} catch (Exception e) {
			logger.error("funTree-method-Exception: ", e);
			return WebUtils.getData(false, "修改角色页面，获取dtree菜单异常",null);
		}
	}
	
	
}
