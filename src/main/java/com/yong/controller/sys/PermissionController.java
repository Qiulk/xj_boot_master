package com.yong.controller.sys;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.yong.commons.springmvc.DateConvertEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yong.service.sys.PermissionService;
import com.yong.service.sys.TSysFunctionService;
import com.yong.commons.utils.WebUtils;
import com.yong.commons.enums.WebConstantsEnum;
/**
 *权限控制
 *
 */
@Controller
@RequestMapping("/admin")
public class PermissionController {

	private static final Logger logger = LoggerFactory.getLogger(PermissionController.class);
	@Autowired
	private PermissionService permissionService;
	@Autowired
	private TSysFunctionService tSysFunctionService;
	
	@Value("${common.nologin:对不起，您还未登录或登录超时，请重新登录}")
	private String nologinMsg;

	@SuppressWarnings("unused")
	@Autowired
	private MessageSource messageSource;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new DateConvertEditor());
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	/**
	 * 验证权限
	 */
	@RequestMapping(value = "/validatorPermission", method = RequestMethod.POST,produces = "text/html;charset=utf-8")
	@ResponseBody
	public String validatorPermission(@RequestParam String PERMISSION_NAME, @RequestParam String ACTION_NAME,
			HttpSession session,  HttpServletRequest request) {
		try {
			String[] permissionNameArray = PERMISSION_NAME.split(",");
			String[] actionNameArray = ACTION_NAME.split(",");
			
			if(permissionNameArray.length != actionNameArray.length){
				return WebUtils.getData(false, "传入验证参数有误，权限个数和权限名称个数不匹配！",null);
			}
			Map<String,String> permissionMap = (Map<String,String>)session.getAttribute(WebConstantsEnum.SESSION_ACTION.getMsg());
			if(permissionMap == null){
				String sessionUserNo = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
				if(sessionUserNo == null){
					return WebUtils.getData(false, nologinMsg,null);
				}
				long pkUser = Long.parseLong(sessionUserNo); 
				permissionMap = permissionService.queryActionByPKUser(tSysFunctionService,pkUser);
				//保存SESSION
				session.setAttribute(WebConstantsEnum.SESSION_ACTION.getMsg(), permissionMap);
				
			}
			Collection collection = permissionMap.values();
			List permissionList = new ArrayList();
			permissionList.addAll(collection);
			Map dbm = new HashMap();
			for(int i = 0; i < permissionNameArray.length; i++){
				if(permissionList.contains(actionNameArray[i])){
					dbm.put(permissionNameArray[i], true);
				}
				else{
					dbm.put(permissionNameArray[i], false);
				}
			}
			return WebUtils.getData(true, "", dbm);
		} catch (Exception e) {
			logger.error("validatorPermission-method-Exception: ", e);
			return WebUtils.getData(false, "验证权限异常！",null);
		}
	}

}
