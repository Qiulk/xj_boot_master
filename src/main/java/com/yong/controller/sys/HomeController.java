package com.yong.controller.sys;

import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.WebConstants;
import com.yong.commons.utils.WebUtils;
import com.yong.model.sys.TSysFunction;
import com.yong.model.sys.TSysUser;
import com.yong.service.sys.TSysFunctionService;
import com.yong.service.sys.TSysRoleService;
import com.yong.service.sys.TSysUserRoleService;
import com.yong.service.sys.TSysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Controller
@Slf4j
public class HomeController {
    @Autowired
    private TSysFunctionService sysFunctionService;
    @Autowired
    private TSysUserService sysUserService;
    @RequestMapping("/")
    public String doLogin(Model model, HttpServletResponse response, HttpSession session, HttpServletRequest request) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        TSysUser sysUser = null;
        try {
            List<TSysUser> sysUsers = sysUserService.getUserListByUserName(name);
            sysUser = sysUsers.get(0);
            session.setAttribute(WebConstantsEnum.SESSION_USER.getMsg(),sysUser);
            session.setAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg(),sysUser.getPkSysUser().toString());
        } catch (Exception e) {
            log.error("获取用户异常："+e);
            e.printStackTrace();
        }
        long pkUser = sysUser.getPkSysUser();
        List<TSysFunction> list = null;
        list = (List<TSysFunction>)session.getAttribute("SESSION_MENU");
        if(list != null){
            model.addAttribute("menu",list);
        }else{
            try {
                list = sysFunctionService.queryALLUserFunctionByPKUser(sysUser.getPkSysUser());
            } catch (Exception e) {
                log.error("获取菜单异常："+e);
                e.printStackTrace();
            }
            if(list != null){
                for(int i = list.size()-1; i >= 0; i--){
                    TSysFunction sysFun = (TSysFunction)list.get(i);
                    String level = String.valueOf(sysFun.getFunctionLevel());
                    if(!WebConstants.FUNCTION_TYPE_NODE.equals(level)){
                        list.remove(i);
                    }
                }
            }
            //过滤重复菜单
            if(!list.isEmpty()){
                Map<String,TSysFunction> map = new TreeMap<>();
                for (Iterator iterator = list.iterator(); iterator.hasNext();) {
                    TSysFunction sysFun = (TSysFunction) iterator.next();
                    map.put(sysFun.getPkSysFunction(), sysFun);
                }
                list.clear();
                int mapSize = map.size();
                Iterator dbmIt = map.entrySet().iterator();
                for(int i = 0; i < mapSize; i++){
                    Map.Entry entry = (Map.Entry) dbmIt.next();
                    list.add((TSysFunction)entry.getValue());
                }
            }
            model.addAttribute("menu".toString(),list);
            session.setAttribute("SESSION_MENU",list);
        }

        model.addAttribute("name",name);
        return "index";
    }

    /**
     * 登录后的主页-菜单栏目框架
     * @param response
     * @return
     */
    @RequestMapping(value = "/index")
    public String index(Model model, HttpServletResponse response, HttpSession session, HttpServletRequest request) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        TSysUser sysUser = null;
        try {
            List<TSysUser> sysUsers = sysUserService.getUserListByUserName(name);
            sysUser = sysUsers.get(0);
            session.setAttribute(WebConstantsEnum.SESSION_USER.getMsg(),sysUser);
            session.setAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg(),sysUser.getPkSysUser().toString());
        } catch (Exception e) {
            log.error("获取用户异常："+e);
            e.printStackTrace();
        }
        long pkUser = sysUser.getPkSysUser();
        List<TSysFunction> list = null;
        list = (List<TSysFunction>)session.getAttribute("SESSION_MENU");
        if(list != null){
            model.addAttribute("menu",list);
        }else{
            try {
                list = sysFunctionService.queryALLUserFunctionByPKUser(sysUser.getPkSysUser());
            } catch (Exception e) {
                log.error("获取菜单异常："+e);
                e.printStackTrace();
            }
            if(list != null){
                for(int i = list.size()-1; i >= 0; i--){
                    TSysFunction sysFun = (TSysFunction)list.get(i);
                    String level = String.valueOf(sysFun.getFunctionLevel());
                    if(!WebConstants.FUNCTION_TYPE_NODE.equals(level)){
                        list.remove(i);
                    }
                }
            }
            //过滤重复菜单
            if(!list.isEmpty()){
                Map<String,TSysFunction> map = new TreeMap<>();
                for (Iterator iterator = list.iterator(); iterator.hasNext();) {
                    TSysFunction sysFun = (TSysFunction) iterator.next();
                    map.put(sysFun.getPkSysFunction(), sysFun);
                }
                list.clear();
                int mapSize = map.size();
                Iterator dbmIt = map.entrySet().iterator();
                for(int i = 0; i < mapSize; i++){
                    Map.Entry entry = (Map.Entry) dbmIt.next();
                    TSysFunction func = (TSysFunction)entry.getValue();
                    list.add(func);
                }
            }
            model.addAttribute("menu".toString(),list);
            session.setAttribute("SESSION_MENU",list);
        }

        model.addAttribute("name",name);
        return "index";
    }

    /**
     * 获取当前登录的用户信息
     */
    @RequestMapping(value = "/admin/getUserSession", method = RequestMethod.POST)
    @ResponseBody
    public String getUserSession(HttpSession session, HttpServletRequest request) {
        try {
            String sessionUserNo = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if(sessionUserNo==null){
                return WebUtils.getData(false, "登录超时",null);
            }
            TSysUser currentUser = (TSysUser)session.getAttribute(WebConstantsEnum.SESSION_USER.getMsg());
            return WebUtils.getData(true, "",currentUser);
        } catch (Exception e) {
            log.error("getUserSession-method-Exception: ", e);
            return WebUtils.getData(false, "获取当前登录的用户信息异常！",null);
        }
    }


    @Autowired
    private TSysUserRoleService tSysUserRoleService;
    @Autowired
    private TSysRoleService tSysRoleService;
    /**
     *  主页-我都桌面
     * @return
     */
    @RequestMapping(value = "/welcome")
    public String welcome(HttpSession session,HttpServletRequest request) throws Exception{
        String name = SecurityContextHolder.getContext().getAuthentication().getName();

        String sysUserId = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
        if(StringUtils.isBlank(sysUserId)){
            return "redirect:index";
        }
        TSysUser user = (TSysUser)session.getAttribute(WebConstantsEnum.SESSION_USER.getMsg());
        request.setAttribute("user",user);
        return "welcome";
    }
}
