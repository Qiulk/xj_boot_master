package com.yong.controller.sys;

import com.yong.commons.SpringUtils.token.Token;
import com.yong.commons.enums.ResulEnum;
import com.yong.commons.enums.SysUserEnums;
import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.commons.utils.WebUtils;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysRole;
import com.yong.model.sys.TSysUser;
import com.yong.service.sys.TSysRoleService;
import com.yong.service.sys.TSysUserRoleService;
import com.yong.vo.ResulVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping(value = "/admin/role")
public class RoleController {

	private static final Logger logger = LoggerFactory.getLogger(RoleController.class);
	private int pageSize = 10;
	private int currentPage = 1;

	@Autowired
	private TSysRoleService tSysRoleService;

	@Autowired
	private TSysUserRoleService tSysUserRoleService;

	/**
	 * 角色列表
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String role(HttpServletRequest request) {
		return "sys/admin-role";
	}


	/**
	 * 新增角色
	 */
	@RequestMapping(value = "/add",method = RequestMethod.GET)
	@Token(save=true)
	public String addRole() {
		return "sys/admin-role-add";
	}

	/**
	 * 修改角色
	 */
	@RequestMapping(value = "/update",method = RequestMethod.GET)
	@Token(save=true)
	public String updateRole(HttpServletRequest request) {
		String pkRole = request.getParameter("id")==null?"":request.getParameter("id").trim();
		request.setAttribute("pkRole", pkRole);
		return "sys/admin-role-update";
	}

	/**
	 * 分页查询角色列表
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public ResulVO queryRoleListByPager(@RequestParam String PAGE_SIZE,
										@RequestParam String CURRENT_PAGE,
										@RequestParam String current,
										HttpServletRequest request,HttpSession session) {
		try {
			String roleName = request.getParameter("roleName")==null?"":request.getParameter("roleName").trim();
			roleName = WebUtils.filterCondition(roleName);
//			if(StringUtils.isEmpty(current)){
//				CURRENT_PAGE = current;
//			}
			String createBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(WebUtils.isBlank(createBy)){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			Criteria criteria = new Criteria();
			criteria.clear();
			//criteria.put("createBy", createBy);
			roleName=java.net.URLDecoder.decode(roleName,"UTF-8");
			if(WebUtils.isNotBlank(roleName)){
				criteria.put("roleNameLike", roleName);
			}
			if(WebUtils.isNotBlank(PAGE_SIZE)){
				pageSize = Integer.valueOf(PAGE_SIZE);
			}
			if(WebUtils.isNotBlank(CURRENT_PAGE)){
				currentPage = Integer.valueOf(CURRENT_PAGE);
			}
			Pager pager = new Pager(pageSize,currentPage);
			ComboPager comboPager = tSysRoleService.queryRoleByPager(criteria,pager);
			return ResulVOUtils.success(comboPager);
		} catch (Exception e) {
			logger.error("queryRoleListByPager-method-Exception: ", e);
			return ResulVOUtils.error(ResulEnum.ROLE_LIST_FAIL.getCode(),ResulEnum.ROLE_LIST_FAIL.getMsg());
		}
	}


	/**
	 * 验证角色名称是否重复
	 */
	@RequestMapping(value = "/checkRoleName", method = RequestMethod.GET)
	@ResponseBody
	public ResulVO checkRoleName(@RequestParam String ROLE_NAME,HttpServletRequest request,HttpSession session) {
		try {
			String createBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(createBy==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			String id = request.getParameter("id")==null?"":request.getParameter("id").trim();
			if(WebUtils.isBlank(ROLE_NAME)){
				return ResulVOUtils.error(ResulEnum.ROLE_NAME_IS_NULL.getCode(),ResulEnum.ROLE_NAME_IS_NULL.getMsg());
			}
			Criteria cri = new Criteria();
			cri.put("roleName", ROLE_NAME.trim());
			if(WebUtils.isNotBlank(id)){
				cri.put("roleIdNot", id);
			}
			//TODO 优化点：查询数量请用count函数
			List<TSysRole> list = tSysRoleService.checkRoleByRoleName(cri);
			if(WebUtils.isNotBlank(id)){
				if(list.size()>0){
					TSysRole sysRole = (TSysRole)list.get(0);
					if(id.equals(sysRole.getPkRole().toString())){
						return ResulVOUtils.success(1);
					}
				}
			}
			Integer integer = list.size();
			return ResulVOUtils.success(integer);
		} catch (Exception e) {
			logger.error("checkRoleName-method-Exception: ", e);
			return ResulVOUtils.error(ResulEnum.ROLE_NAME_CHECK_FAIL.getCode(),ResulEnum.ROLE_NAME_CHECK_FAIL.getMsg());
		}
	}


	/**
	 * 保存角色信息
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@Token(remove=true)
	public ResulVO saveRole(HttpSession session,HttpServletRequest request) {
		try {
			String createBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(createBy==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			String roleName = request.getParameter("ROLE_NAME")==null?"":request.getParameter("ROLE_NAME").trim();
			String roleDesc = request.getParameter("ROLE_DESC")==null?"":request.getParameter("ROLE_DESC").trim();
			String[] pkFunArray = request.getParameterValues("PK_FUNCTION");
			if (WebUtils.isBlank(roleName)) {

				return ResulVOUtils.error(ResulEnum.ROLE_NAME_IS_NULL.getCode(),ResulEnum.ROLE_NAME_IS_NULL.getMsg());
			}
			Criteria cri = new Criteria();
			cri.put("roleName", roleName);
			//cri.put("roleIdNot", id);
			List<TSysRole> list = tSysRoleService.checkRoleByRoleName(cri);
			if(list.size()>0){
				return ResulVOUtils.error(ResulEnum.ROLE_NAME_RESAVE.getCode(),ResulEnum.ROLE_NAME_RESAVE.getMsg());
			}
//			if(pkFunArray.length==0){
//				return ResulVOUtils.error(ResulEnum.ROLE_NOT_MODULE.getCode(),ResulEnum.ROLE_NOT_MODULE.getMsg());
//			}

			TSysRole sysRole = new TSysRole();
			sysRole.setRoleName(roleName);
			sysRole.setRoleDesc(roleDesc);
			sysRole.setCreateBy(createBy);
			//保存角色和角色权限
			tSysRoleService.saveRole(sysRole, pkFunArray);
			return ResulVOUtils.success();
		} catch (Exception e) {
			logger.error("saveRole-method-Exception: ", e);
			String token = UUID.randomUUID().toString();
			session.setAttribute("token", token);
			return ResulVOUtils.error(ResulEnum.ROLE_SAVE_FAIL.getCode(),ResulEnum.ROLE_SAVE_FAIL.getMsg());
		}
	}


	/**
	 * 修改页面获取角色信息
	 */
	@RequestMapping(value = "/getRole", method = RequestMethod.POST)
	@ResponseBody
	public ResulVO getRole(HttpSession session,HttpServletRequest request) {
		try {
			String pkRole = request.getParameter("pkRole")==null?"":request.getParameter("pkRole").trim();
			if (WebUtils.isBlank(pkRole)) {
				return ResulVOUtils.error(SysUserEnums.GET_PKID_ISNULL.getCode(),SysUserEnums.GET_PKID_ISNULL.getMsg());
			}
			TSysRole role = tSysRoleService.selectByPrimaryKey(Long.parseLong(pkRole));
			return ResulVOUtils.success(role);
		} catch (Exception e) {
			logger.error("getRole-method-Exception: ", e);
			return ResulVOUtils.error(ResulEnum.ROLE_UPDATE_GETFAIL.getCode(),ResulEnum.ROLE_UPDATE_GETFAIL.getMsg());
		}
	}

	/**
	 * 修改角色信息
	 */
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	@ResponseBody
	@Token(remove=true)
	public ResulVO modify(HttpSession session,HttpServletRequest request) {
		try {
			String updateBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(updateBy==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			String pkRole = request.getParameter("pkRole")==null?"":request.getParameter("pkRole").trim();
			String roleName = request.getParameter("ROLE_NAME")==null?"":request.getParameter("ROLE_NAME").trim();
			String roleDesc = request.getParameter("ROLE_DESC")==null?"":request.getParameter("ROLE_DESC").trim();
			String[] pkFunArray = request.getParameterValues("PK_FUNCTION");
			if (WebUtils.isBlank(pkRole)) {
				return ResulVOUtils.error(SysUserEnums.GET_PKID_ISNULL.getCode(),SysUserEnums.GET_PKID_ISNULL.getMsg());
			}
			if (WebUtils.isBlank(roleName)) {
				return ResulVOUtils.error(ResulEnum.ROLE_NAME_IS_NULL.getCode(),ResulEnum.ROLE_NAME_IS_NULL.getMsg());
			}
			Criteria cri = new Criteria();
			cri.put("roleName", roleName);
			cri.put("roleIdNot", pkRole);
			List<TSysRole> list = tSysRoleService.checkRoleByRoleName(cri);
			if(WebUtils.isNotBlank(pkRole)){
				if(list.size()>0){
					TSysRole sysRole = (TSysRole)list.get(0);
					if(!pkRole.equals(sysRole.getPkRole().toString())){
						return ResulVOUtils.error(ResulEnum.ROLE_NAME_RESAVE.getCode(),ResulEnum.ROLE_NAME_RESAVE.getMsg());
					}
				}
			}
//			if(pkFunArray.length==0){
//				return ResulVOUtils.error(ResulEnum.ROLE_NOT_MODULE.getCode(),ResulEnum.ROLE_NOT_MODULE.getMsg());
//			}
			TSysRole sysRole = new TSysRole();
			sysRole.setPkRole(Long.parseLong(pkRole));
			sysRole.setRoleName(roleName);
			sysRole.setRoleDesc(roleDesc);
			sysRole.setUpdateBy(updateBy);
			tSysRoleService.updateRoleAndRoleFunByPkRole(sysRole, pkFunArray);
			return ResulVOUtils.success();
		} catch (Exception e) {
			logger.error("modify-method-Exception: ", e);
			String token = UUID.randomUUID().toString();
			session.setAttribute("token", token);
			return ResulVOUtils.error(ResulEnum.ROLE_UPDATE_FAIL.getCode(),ResulEnum.ROLE_UPDATE_FAIL.getMsg());
		}
	}




	/**
	 * 新增用户，获取dtree
	 */
	@RequestMapping(value = "/roleTree",method = RequestMethod.POST)
	@ResponseBody
	public ResulVO roleTree(HttpSession session) {
		try {
			TSysUser sysUser = (TSysUser)session.getAttribute(WebConstantsEnum.SESSION_USER.getMsg());
			if(sysUser==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			Criteria cri = new Criteria();
			//cri.put("createBy", sysUser.getPkSysUser());
			List roleList = tSysRoleService.queryRoleList(cri);

			return ResulVOUtils.success(roleList);
		} catch (Exception e) {
			logger.error("roleTree-method-Exception: ", e);
			return ResulVOUtils.error(SysUserEnums.ROLE_TREE_ERRO.getCode(),SysUserEnums.ROLE_TREE_ERRO.getMsg());
		}
	}

	/**
	 * 修改用户，获取dtree
	 */
	@RequestMapping(value = "/updateRoleTree", method = RequestMethod.POST)
	@ResponseBody
	public String updateRoleTree(HttpServletRequest request,HttpSession session) {
		try {
			String pkSysUser = request.getParameter("pkSysUser")==null?"":request.getParameter("pkSysUser").trim();
			if(pkSysUser==null){
				return WebUtils.getData(false,"获取角色树异常，主键不能为空!",null);
				//return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			String createBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(createBy==null){
				return WebUtils.getData(false,"登录超时",null);
				//return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			Criteria cri = new Criteria();
			//cri.put("createBy", createBy);
			List roleList = tSysRoleService.queryRoleList(cri);
			List userRoleList = tSysUserRoleService.queryUserRoleByPKUser(Long.parseLong(pkSysUser));
			Map rs = new HashMap();
			rs.put("roleList", roleList);
			rs.put("userRoleList", userRoleList);
			return WebUtils.getData(true,"",rs);
		} catch (Exception e) {
			logger.error("updateRoleTree-method-Exception: ", e);
			return WebUtils.getData(false,"修改页面，获取角色树异常!",null);
			//return ResulVOUtils.error(SysUserEnums.ROLE_TREE_ERRO.getCode(),SysUserEnums.ROLE_TREE_ERRO.getMsg());
		}
	}

	/**
	 * 单条删除和批量删除记录
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResulVO delete(HttpSession session,HttpServletRequest request) {
		try {
			String updateBy = (String)session.getAttribute(WebConstantsEnum.
					SESSION_USER_ON.getMsg());
			if(updateBy==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			String[] pkRoleArray = request.getParameter("PK_ROLE").split(",");
			if (pkRoleArray.length==0) {
				return ResulVOUtils.error(SysUserEnums.CHECK_DEL_RECODE.getCode(),SysUserEnums.CHECK_DEL_RECODE.getMsg());
			}
			//判断该角色是否已分配
			Boolean check = tSysRoleService.checkUserROleOn(pkRoleArray);
			if(check){
				if (pkRoleArray.length > 1) {
					return ResulVOUtils.error(9999,"您选择删除的角色中，存在已分配的角色");
				}else{
					return ResulVOUtils.error(9998,"该角色已被分配");
				}
			}
			tSysRoleService.deleteRoleAndRoleFunByPkRole(pkRoleArray, updateBy);
			return ResulVOUtils.success();
		} catch (Exception e) {
			logger.error("delete-method-Exception: ", e);
			return ResulVOUtils.error(ResulEnum.ROLE_DEL_FAIL.getCode(),ResulEnum.ROLE_DEL_FAIL.getMsg());
		}
	}


//	/**
//	 * 导入excel
//	 */
//	@RequestMapping(value = "/importExcel", method = RequestMethod.POST,produces = "text/html;charset=utf-8")
//	@ResponseBody
//	public String importExcel(HttpSession session,HttpServletRequest request) {
//		try {
//			String filePath = request.getParameter("filePath")==null?"":request.getParameter("filePath").trim();
//			//System.out.println("filePath222==============="+filePath);
//			ImportExcelUtil importExcel = new ImportExcelUtil(filePath);
//			List dataList = importExcel.getDataList();
//			for (Iterator iterator = dataList.iterator(); iterator.hasNext();) {
//				Map<String, String> row = (Map<String, String>) iterator.next();
//				System.out.println("row0==="+row.get("0").toString());
//				System.out.println("row1==="+row.get("1").toString());
//				System.out.println("row2==="+row.get("2").toString());
//			}
//			return WebUtils.getData(true,"",null);
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("RoleController-importExcel-Exception: ", e);
//			return WebUtils.getData(false,"导入角色异常!",null);
//		}
//	}
}
