package com.yong.controller.sys;

import java.io.PrintWriter;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.yong.commons.SpringUtils.token.Token;
import com.yong.commons.enums.OrgEnum;
import com.yong.commons.enums.SysUserEnums;
import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.commons.utils.WebConstants;
import com.yong.commons.utils.WebUtils;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysOrganization;
import com.yong.model.sys.TSysUser;
import com.yong.service.sys.TSysOrganizationService;
import com.yong.vo.ResulVO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.beans.factory.annotation.Value;

/**
 * TSysOrganization Controller
 * 2018-11-30 09:13
 */
@Controller
@RequestMapping("/admin/org")
public class OrgController {

	private static final Logger logger = LoggerFactory.getLogger(OrgController.class);
	private int pageSize = 10;
	private int currentPage = 1;

	@Value("${sys_error:}")
	private String sysError;
	
	@Value("${login_timeout:}")
	private String loginTimeout;

	@Autowired
	private TSysOrganizationService sysOrganizationService;
	
	/**
	 * 列表
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list(HttpServletRequest request) {
		String current = request.getParameter("current")==null?"":request.getParameter("current").trim();
		request.setAttribute("current", current);
		//传递查询参数
//		String schKeyword = request.getParameter("schKeyword")==null?"":request.getParameter("schKeyword").trim();
//		String schMatchFlag = request.getParameter("schMatchFlag")==null?"":request.getParameter("schMatchFlag").trim();
//		request.setAttribute("schKeyword", schKeyword);
//		request.setAttribute("schMatchFlag", schMatchFlag);
		return "sys/org";
	}
	
	/**
	 * 新增
	 */
	@RequestMapping(value = "/add",method = RequestMethod.GET)
	@Token(save=true)
	public String add(HttpServletRequest request) {
		String pName = request.getParameter("pName")==null?"":request.getParameter("pName").trim();
		request.setAttribute("pName", pName);
		String pId = request.getParameter("pId")==null?"":request.getParameter("pId").trim();
		request.setAttribute("pId", pId);
		return "sys/org_add";
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value = "/update",method = RequestMethod.GET)
	@Token(save=true)
	public String update(HttpServletRequest request) {
		String id = request.getParameter("id")==null?"":request.getParameter("id").trim(); 
		request.setAttribute("id", id);
		return "sys/org_update";
	}

	/**
	 * 查看
	 */
	@RequestMapping(value = "/get",method = RequestMethod.GET)
	public String get(HttpServletRequest request) throws Exception {
		String id = request.getParameter("id")==null?"":request.getParameter("id").trim(); 
		request.setAttribute("id", id);
		TSysOrganization org = sysOrganizationService.get(Long.parseLong(id));
		request.setAttribute("org",org);
		return "sys/org_get";
	}
	
	
	/**
	 * 分页查询列表
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public String queryListByPager(@RequestParam String PAGE_SIZE,
			@RequestParam String CURRENT_PAGE,
			@RequestParam String current,
			HttpSession session,
			HttpServletRequest request) {
		try {
			String sessionUserNo = (String)session.getAttribute(WebConstants.SESSION_USER_NO);
			if(WebUtils.isNull(sessionUserNo)){
				return WebUtils.getData(false,loginTimeout,null);
			}
			//查询条件
//			String keyword = request.getParameter("keyword")==null?"":request.getParameter("keyword").trim();
//			keyword = WebUtils.filterCondition(keyword);
//			String matchFlag = request.getParameter("matchFlag")==null?"":request.getParameter("matchFlag").trim();
			if(StringUtils.isNotBlank(current)){
				CURRENT_PAGE = current;
			}
			//查询条件
			Criteria criteria = new Criteria();
			criteria.clear();
			//分页
			if(StringUtils.isNotBlank(PAGE_SIZE)){
				pageSize = Integer.valueOf(PAGE_SIZE);
			}
			if(StringUtils.isNotBlank(CURRENT_PAGE)){
				currentPage = Integer.valueOf(CURRENT_PAGE);
			}
			Pager pager = new Pager(pageSize,currentPage);
			ComboPager comboPager = sysOrganizationService.queryListByPager(criteria,pager);
			return WebUtils.getData(true, "", comboPager);
		} catch (Exception e) {
			logger.error("OrgController.java-queryListByPager-Exception: ", e);
			return WebUtils.getData(false, sysError, null);
		}
	}

	
	/**
	 * 保存新增
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@Token(remove=true)
	public ResulVO save(HttpSession session,HttpServletRequest request) {
		try {
			String createBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(createBy==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			//全称
			String orgName = request.getParameter("ORG_NAME")==null?"":request.getParameter("ORG_NAME").trim();
			//简称
			String abbreviation = request.getParameter("ABBREVIATION")==null?"":request.getParameter("ABBREVIATION").trim();
			//地址
			String address = request.getParameter("ADDRESS")==null?"":request.getParameter("ADDRESS").trim();
			//上级组织
			String superId = request.getParameter("pId")==null?"":request.getParameter("pId").trim();
			//备注
			String descCode = request.getParameter("DESC_CODE")==null?"":request.getParameter("DESC_CODE").trim();
//			String orderBy = request.getParameter("orderBy")==null?"":request.getParameter("orderBy").trim();
//			String isHasChild = request.getParameter("isHasChild")==null?"":request.getParameter("isHasChild").trim();
			if (StringUtils.isBlank(orgName)){
				return ResulVOUtils.error(OrgEnum.ORG_NAME.getCode(),OrgEnum.ORG_NAME.getMsg(),session);
			}
			if (StringUtils.isBlank(abbreviation)){
				return ResulVOUtils.error(OrgEnum.ORG_NAME_JC.getCode(),OrgEnum.ORG_NAME_JC.getMsg(),session);
			}
			Long orgLevel = null;
			if (!superId.equals("0")){
				//查询上级
				TSysOrganization sup = sysOrganizationService.get(Long.parseLong(superId));
				if (sup == null){
					return ResulVOUtils.error(OrgEnum.ORG_SUP_NULL.getCode(),OrgEnum.ORG_SUP_NULL.getMsg(),session);
				}else{
					orgLevel = sup.getOrgLevel()+1;
				}
			}else {
				orgLevel = 1L;
			}

			//封装对象
			TSysOrganization obj = new TSysOrganization();
			Long orgId = sysOrganizationService.seqAlways();
			obj.setOrgId(orgId);
			obj.setOrgName(orgName);
			obj.setAbbreviation(abbreviation);
			obj.setOrgLevel(orgLevel);
			obj.setAddress(address);
			obj.setSuperId(Long.parseLong(superId));
			obj.setCreateBy(createBy);
			obj.setDescCode(descCode);
		    sysOrganizationService.save(obj);
			return ResulVOUtils.success();
		} catch (Exception e) {
			logger.error("OrgController.java-save-Exception: ", e);
			return ResulVOUtils.error(OrgEnum.ORG_SAVE_ERROR.getCode(),OrgEnum.ORG_SAVE_ERROR.getMsg(),session);
		}
	}
	
	/**
	 * 根据主键查询详细信息
	 */
	@RequestMapping(value = "/getInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResulVO getInfo(HttpServletRequest request) {
		try {
			String id = request.getParameter("id")==null?"":request.getParameter("id").trim();
			if(StringUtils.isBlank(id)){
				return ResulVOUtils.error(OrgEnum.ORG_NULL.getCode(),OrgEnum.ORG_NULL.getMsg());
			}
			//System.out.println("id========"+id);
			TSysOrganization obj = sysOrganizationService.get(Long.parseLong(id));
			return ResulVOUtils.success(obj);
		} catch (Exception e) {
			logger.error("OrgController.java-getInfo-Exception: ", e);
			return ResulVOUtils.error(OrgEnum.ORG_NULL.getCode(),OrgEnum.ORG_NULL.getMsg());
		}
	}
	
	/**
	 * 保存修改
	 */
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	@ResponseBody
	@Token(remove=true)
	public ResulVO modify(HttpSession session,HttpServletRequest request) {
		try {
			String updateBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(updateBy==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			//全称
			String orgId = request.getParameter("orgId")==null?"":request.getParameter("orgId").trim();
			//全称
			String orgName = request.getParameter("ORG_NAME")==null?"":request.getParameter("ORG_NAME").trim();
			//简称
			String abbreviation = request.getParameter("ABBREVIATION")==null?"":request.getParameter("ABBREVIATION").trim();
			//地址
			String address = request.getParameter("ADDRESS")==null?"":request.getParameter("ADDRESS").trim();
			//备注
			String descCode = request.getParameter("DESC_CODE")==null?"":request.getParameter("DESC_CODE").trim();
			if (StringUtils.isBlank(orgName)){
				return ResulVOUtils.error(OrgEnum.ORG_NAME.getCode(),OrgEnum.ORG_NAME.getMsg(),session);
			}
			if (StringUtils.isBlank(abbreviation)){
				return ResulVOUtils.error(OrgEnum.ORG_NAME_JC.getCode(),OrgEnum.ORG_NAME_JC.getMsg(),session);
			}

			//封装对象
			TSysOrganization obj = new TSysOrganization();
			obj.setOrgId(Long.parseLong(orgId));
			obj.setOrgName(orgName);
			obj.setAbbreviation(abbreviation);
			obj.setAddress(address);
			obj.setUpdateBy(updateBy);
			obj.setUpdateDate(new Date());
			obj.setDescCode(descCode);
		    sysOrganizationService.update(obj);
			return ResulVOUtils.success();
		} catch (Exception e) {
			logger.error("OrgController.java-modify-Exception: ", e);
			return ResulVOUtils.error(OrgEnum.ORG_UP.getCode(),OrgEnum.ORG_UP.getMsg(),session);
		}
	}

	/**
	 * 单条删除和批量删除记录
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResulVO delete(HttpSession session,HttpServletRequest request) {
		try {
			String updateBy = (String)session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if(updateBy==null){
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(),SysUserEnums.LONGIN_OUT.getMsg());
			}
			String[] idsArray = request.getParameter("id").split(",");
			if (idsArray.length==0) {
				return ResulVOUtils.error(OrgEnum.ORG_DEL_BICH.getCode(),OrgEnum.ORG_DEL_BICH.getMsg());
			}
			for (int m=0;m<idsArray.length;m++){
				Long orgId = Long.parseLong(idsArray[m]);
				TSysOrganization org = sysOrganizationService.get(orgId);
				if (org == null){
					return ResulVOUtils.error(OrgEnum.ORG_NULL.getCode(),OrgEnum.ORG_NULL.getMsg());
				}
				if (org.getIsHasChild().longValue() == 1L){
					return ResulVOUtils.error(OrgEnum.ORG_HAS_CHILD.getCode(),OrgEnum.ORG_HAS_CHILD.getMsg());
				}
			}
		    sysOrganizationService.remove(idsArray,updateBy);
			return ResulVOUtils.success();
		} catch (Exception e) {
			logger.error("OrgController.java-delete-Exception: ", e);
			return ResulVOUtils.error(OrgEnum.ORG_DEL_ERROR.getCode(),OrgEnum.ORG_DEL_ERROR.getMsg());
		}
	}

	/**
	 * 获取组织机构树节点
	 */
	@RequestMapping(value = "/getAllOrgNodes", method = RequestMethod.POST)
	public void getAllOrgNodes(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			TSysUser sessionUser = (TSysUser)session.getAttribute(WebConstantsEnum.SESSION_USER.getMsg());
			if(null == sessionUser){
				logger.error("获取组织机构树节点异常：登录超时");
				return;
			}

			out = response.getWriter();
			String superId = request.getParameter("orgId")==null?"":request.getParameter("orgId").trim();
			//查询条件
			Criteria cri = new Criteria();
			cri.clear();
			if(StringUtils.isNotBlank(superId)){
				cri.put("superId", superId);
			}else{
				cri.put("superId", 0);
			}

			List<TSysOrganization> list = sysOrganizationService.selectByCondition(cri);
			if(list.size() == 0 && StringUtils.isBlank(superId)){
				String node = "[{id:999999, name:'系统内暂未有组织，请接入组织机构'}]" ;
				out.print(node);
				return;
			}
			String res = "[";
			int i=1;
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				TSysOrganization org = (TSysOrganization) iterator.next();
				String isParent = "true";
				if(org.getIsHasChild()==0L){
					isParent = "false";
				}
				String open = "";
				if(i==1){
					open = ",open:true";
				}
				res += "{ id:'"+org.getOrgId()+"',	name:'"+org.getOrgName()+"' , areaCode:'"+org.getAreaCode()+"' ,savelevel:'"+org.getOrgLevel()+"',isParent:"+isParent+" "+open+"}";
				if(i<list.size()){
					res += ",";
				}
				i++;
			}
			res += "]";
			out.print(res);
		} catch (Exception e) {
			logger.error("OrgController.java-getAllOrgNodes-Exception: ", e);
		}
	}

	/**
	 * 验证组织名称关键词是否已存在
	 */
	@RequestMapping(value = "/checkKeyword", method = RequestMethod.GET)
	@ResponseBody
	public ResulVO checkKeyword(HttpSession session, HttpServletRequest request) {
		try {
			String createBy = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
			if (StringUtils.isEmpty(createBy)) {
				return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
			}
			//组织结构全称
			String orgName = request.getParameter("ORG_NAME")==null?"":request.getParameter("ORG_NAME").trim();
			//组织结构简称
			String abbreviation = request.getParameter("ABBREVIATION")==null?"":request.getParameter("ABBREVIATION").trim();
			//组织机构ID
			String orgId = request.getParameter("orgId")==null?"":request.getParameter("orgId").trim();
			//查询条件
			Criteria cri = new Criteria();
			cri.clear();
			if(StringUtils.isNotBlank(orgId)){
				cri.put("orgIdNot", orgId);
			}
			if(StringUtils.isNotBlank(orgName)){
				cri.put("orgName", orgName);
			}
			if(StringUtils.isNotBlank(abbreviation)){
				cri.put("abbreviation", abbreviation);
			}
			Integer num = sysOrganizationService.countByExample(cri);
			return ResulVOUtils.success(num);
		} catch (Exception e) {
			logger.error("ContentController.java-checkKeyword-Exception: ", e);
			return ResulVOUtils.error(OrgEnum.ORG_CHECK_NAME.getCode(), OrgEnum.ORG_CHECK_NAME.getMsg());
		}
	}
}
