package com.yong.controller.sys;


import com.yong.commons.SpringUtils.token.Token;
import com.yong.commons.enums.ResulEnum;
import com.yong.commons.enums.SysUserEnums;
import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.MD5;
import com.yong.commons.utils.WebUtils;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.sys.TSysUser;
import com.yong.service.sys.TSysOrganizationService;
import com.yong.service.sys.TSysRoleService;
import com.yong.service.sys.TSysUserService;
import com.yong.vo.ResulVO;
import com.yong.commons.utils.ResulVOUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

/**
 * 用户管理
 */
@Controller
@RequestMapping("/admin/user")
@Slf4j
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    private int pageSize = 10;
    private int currentPage = 1;

    @Value("${login_timeout:}")
    private String loginTimeout;
    @Value("${sys_error:}")
    private String sysError;
    @Autowired
    private TSysUserService tSysUserService;

    @Autowired
    private TSysOrganizationService organizationService;

    @Autowired
    private TSysRoleService sysRoleService;
    /*@GetMapping(value = "/hello")
    public ResulVO say() {
        TSysUser tSysUser = userService.findOen(10080L);

        if (tSysUser == null) {
            //抛出系统定义的异常
            throw new SysException(ResulEnum.NO_SYS_USER);
        }

        ResulVO resulVO = new ResulVO();
        //resulVO.setDate(tSysUser);

        List<TSysUser> lsit = userService.findAll();
        //for()set 进list精简方法 利用Java8特性 lambad
        //获取用户类型List<Long>
        List<String> userTypeList = lsit.stream().map(e -> e.getUserType()).collect(Collectors.toList());
        resulVO.setRs(userTypeList);
        return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), ResulEnum.NO_SYS_USER.getMsg());


    }*/


    /**
     * 用户列表
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String memberList() {
        return "member/member-list";
    }

    /**
     * 添加用户
     *
     * @return
     */
    @RequestMapping(value = "/member-add")
    @Token(save = true)
    public String member_add(HttpServletRequest request) {

        return "member/member-add";
    }

    /**
     * 个人信息
     *
     * @return
     */
    @RequestMapping(value = "/getPersonInfo")
    public String getPersonInfo(HttpServletRequest request,HttpSession session) {
        String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
        if(StringUtils.isBlank(id)){
            id = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
        }
        request.setAttribute("id", id);
        return "member/member-personInfo";
    }


    /**
     * 编辑用户
     *
     * @return
     */
    @RequestMapping(value = "/member-update")
    @Token(save=true)
    public String member_update(HttpServletRequest request) {
        String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
        request.setAttribute("id", id);
        return "member/member-update";
    }

    /**
     * 修改密码
     *
     * @return
     */
    @RequestMapping(value = "/update_pwd")
    public String updatePwd(HttpServletRequest request) {
        return "sys/update_pwd";
    }

    /**
     * 用户列表查询
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO member_list_data(
            @RequestParam String PAGE_SIZE,
            @RequestParam String CURRENT_PAGE,
            @RequestParam String current,
            HttpSession session,
            HttpServletRequest request) throws Exception {
        try {
            String createBy = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (StringUtils.isEmpty(createBy)) {
                return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
            }
            String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName").trim();

            if (!StringUtils.isEmpty(current)) {
                CURRENT_PAGE = current;
            }
            Criteria criteria = new Criteria();
            criteria.clear();
            userName=java.net.URLDecoder.decode(userName,"UTF-8");

            if (!StringUtils.isEmpty(userName) && !userName.equals("undefined")) {
                criteria.put("userNameLike", userName);
            }
            criteria.put("createBy",Long.parseLong(createBy));
            //分页
            if (!StringUtils.isEmpty(PAGE_SIZE)) {
                pageSize = Integer.valueOf(PAGE_SIZE);
            }
            if (!StringUtils.isEmpty(CURRENT_PAGE)) {
                currentPage = Integer.valueOf(CURRENT_PAGE);
            }
            Pager pager = new Pager(pageSize, currentPage);
            ComboPager comboPager = tSysUserService.queryUserByPager(criteria, pager);
            return ResulVOUtils.success(comboPager);
        } catch (Exception e) {
            log.error("member_list_data ERROR:", e);
            return ResulVOUtils.error(ResulEnum.USER_LIST_FAIKL.getCode(), ResulEnum.USER_LIST_FAIKL.getMsg());
        }

    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    @Token(remove = true)
    public ResulVO save(HttpSession session, HttpServletRequest request) throws Exception {
        try {
            String createBy = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (StringUtils.isEmpty(createBy)) {
                return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
            }
            //用户名
            String userName = request.getParameter("USER_NAME") == null ? "" : request.getParameter("USER_NAME").trim();
            if (StringUtils.isBlank(userName)){
                return ResulVOUtils.error(SysUserEnums.USER_NAME_NULL.getCode(), SysUserEnums.USER_NAME_NULL.getMsg());
            }
            //密码
            String password = request.getParameter("PASSWORD") == null ? "" : request.getParameter("PASSWORD").trim();
            if (StringUtils.isBlank(password)){
                return ResulVOUtils.error(SysUserEnums.PWD_NULL.getCode(), SysUserEnums.PWD_NULL.getMsg());
            }
            //联系电话
            String phone = request.getParameter("PHONE") == null ? "" : request.getParameter("PHONE").trim();
            String[] pkRoleArray = request.getParameterValues("ROLE_ID");
            if (pkRoleArray.length<0){
                return ResulVOUtils.error(SysUserEnums.USER_ROLE_NOLL.getCode(), SysUserEnums.USER_ROLE_NOLL.getMsg());
            }
            for (int i=0;i<pkRoleArray.length;i++){
                if (null == sysRoleService.selectByPrimaryKey(Long.parseLong(pkRoleArray[i]))){
                    return ResulVOUtils.error(SysUserEnums.USER_ROLE_NOT.getCode(), SysUserEnums.USER_ROLE_NOT.getMsg());
                }
            }
            TSysUser sysUser = new TSysUser();
            sysUser.setUserName(userName);
            sysUser.setPassword(password);
            sysUser.setPhone(phone);
            sysUser.setCreateBy(createBy);

            //保存用户和用户角色
            tSysUserService.saveUser(sysUser, pkRoleArray);
            return ResulVOUtils.success();
        } catch (Exception e) {
            log.error("user-save:", e);
            String token = UUID.randomUUID().toString();
            session.setAttribute("token", token);
            return ResulVOUtils.error(ResulEnum.SAVE_USER_FAIL.getCode(), ResulEnum.SAVE_USER_FAIL.getMsg());
        }
    }

    /**
     * 单条删除和批量删除记录
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO delete(HttpSession session, HttpServletRequest request) {
        try {
            String updateBy = (String) session.getAttribute(WebConstantsEnum.
                    SESSION_USER_ON.getMsg());
            if (updateBy == null) {
                return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
            }

            String[] pkRoleArray = request.getParameter("PK_USER").split(",");
            if (pkRoleArray.length == 0) {
                return ResulVOUtils.error(SysUserEnums.CHECK_DEL_RECODE.getCode(), SysUserEnums.CHECK_DEL_RECODE.getMsg());
            }

            tSysUserService.deleteUserAndUserRoleByPkSysUser(pkRoleArray, updateBy);
            return ResulVOUtils.success();
        } catch (Exception e) {
            log.error("member_list_data ERROR:", e);
            return ResulVOUtils.error(ResulEnum.ROLE_DEL_FAIL.getCode(), ResulEnum.ROLE_DEL_FAIL.getMsg());
        }
    }

    /**
     * 修改页面，获取用户信息
     */
    @RequestMapping(value = "/getUserInfo", method = RequestMethod.POST, produces = "text/html;charset=utf-8")
    @ResponseBody
    public String getUserInfo(HttpServletRequest request) {
        try {
            String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
            if (id == null && "".equals(id)) {
                return WebUtils.getData(false, sysError, null);
            }
            //System.out.println("userName========"+userName);
            TSysUser sysUser = tSysUserService.getUserInfoByPk(Long.parseLong(id));
            return WebUtils.getData(true, "", sysUser);
        } catch (Exception e) {
            logger.error("ForcerController.java-getInfo-Exception: ", e);
            return WebUtils.getData(false, sysError, null);
        }
    }

    /**
     * 停用/启用
     */
    @RequestMapping(value = "/updateOpen", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO updateOpen(HttpSession session, HttpServletRequest request) {
        try {
            String sessionUserNo = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (WebUtils.isNull(sessionUserNo)) {
                return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), ResulEnum.NO_SYS_USER.getMsg());
            }
            String pkSysUser = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
            TSysUser sysUser = new TSysUser();
            sysUser.setPkSysUser(Long.parseLong(pkSysUser));
            sysUser.setUpdateBy(sessionUserNo);
            tSysUserService.updateOpen(sysUser);
            return ResulVOUtils.success();
        } catch (Exception e) {
            logger.error("modify-method-Exception: ", e);
            String token = UUID.randomUUID().toString();
            session.setAttribute("token", token);
            return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), "修改用户信息异常!");
        }
    }

     /**
     * 修改用户信息
     */
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    @ResponseBody
    @Token(remove=true)
    public ResulVO modify(HttpSession session, HttpServletRequest request) {
        try {
            String sessionUserNo = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (WebUtils.isNull(sessionUserNo)) {
                return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), ResulEnum.NO_SYS_USER.getMsg());
            }
            String pkSysUser = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
            String[] pkRoleArray = request.getParameterValues("ROLE_ID");
            if (pkRoleArray.length == 0) {
                String token = UUID.randomUUID().toString();
                session.setAttribute("token", token);
                return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), "请为用户分配角色!");
            }
            TSysUser sysUser = new TSysUser();
            sysUser.setPkSysUser(Long.parseLong(pkSysUser));

            sysUser.setUpdateBy(sessionUserNo);

            //修改用户和用户角色
            tSysUserService.modify(sysUser, pkRoleArray);
            return ResulVOUtils.success();
        } catch (Exception e) {
            logger.error("modify-method-Exception: ", e);
            String token = UUID.randomUUID().toString();
            session.setAttribute("token", token);
            return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), "修改用户信息异常!");
        }
    }


    /**
     * 确认旧密码
     */
    @RequestMapping(value = "/checkOldPwd", method = RequestMethod.GET)
    @ResponseBody
    public ResulVO checkOldPwd(HttpSession session, HttpServletRequest request) {
        try {
            TSysUser sysUser = (TSysUser) session.getAttribute(WebConstantsEnum.SESSION_USER.getMsg());
            if (sysUser == null) {
                return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), ResulEnum.NO_SYS_USER.getMsg());
            }

            String oldPwd = request.getParameter("OLD_PWD") == null ? "" : request.getParameter("OLD_PWD").trim();
            String encPwd = MD5.encrypt(oldPwd, sysUser.getUserName());
            Integer num = tSysUserService.checkOldPwd(sysUser.getPkSysUser(), encPwd);
            return ResulVOUtils.success(num);
        } catch (Exception e) {
            logger.error("checkOldPwd-method-Exception: ", e);
            return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), "");
        }
    }

    /**
     * 确认用户名
     */
    @RequestMapping(value = "/checkUserName", method = RequestMethod.GET)
    @ResponseBody
    public ResulVO checkUserName(HttpSession session, HttpServletRequest request) {
        try {
            TSysUser sysUser = (TSysUser) session.getAttribute(WebConstantsEnum.SESSION_USER.getMsg());
            if (sysUser == null) {
                return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), ResulEnum.NO_SYS_USER.getMsg());
            }
            String userName = request.getParameter("USER_NAME") == null ? "" : request.getParameter("USER_NAME").trim();
            Criteria cri = new Criteria();
            cri.clear();;
            cri.put("userName",userName);
            Integer num = tSysUserService.countByExample(cri);
            return ResulVOUtils.success(num);
        } catch (Exception e) {
            logger.error("checkOldPwd-method-Exception: ", e);
            return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), "");
        }
    }

    /**
     * 修改密码
     */
    @RequestMapping(value = "/modify_pwd", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO modifyPwd(HttpSession session, HttpServletRequest request) {
        try {
            TSysUser sessionUser = (TSysUser) session.getAttribute(WebConstantsEnum.SESSION_USER.getMsg());
            if (sessionUser == null) {
                return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), ResulEnum.NO_SYS_USER.getMsg());
            }
            Long pkSysUser = sessionUser.getPkSysUser();

            String pwd = request.getParameter("NEW_PWD") == null ? "" : request.getParameter("NEW_PWD").trim();

            TSysUser sysUser = new TSysUser();
            sysUser.setPkSysUser(pkSysUser);
            //pwd = MD5.encrypt(pwd, sessionUser.getUserName());//加密
            pwd = MD5.encrypt(pwd, pkSysUser.toString());//加密
            sysUser.setPassword(pwd);
            sysUser.setUpdateBy(String.valueOf(pkSysUser));
            //修改密码
            tSysUserService.modifyPwd(sysUser);
            return ResulVOUtils.success();

        } catch (Exception e) {
            logger.error("modifyPwd-method-Exception: ", e);
            String token = UUID.randomUUID().toString();
            session.setAttribute("token", token);
            return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), "修改密码异常!");
        }
    }


    /**
     * 根据角色ID查询用户
     * @return
     */
    @RequestMapping(value = "/findUser", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO findUser(
            HttpSession session,
            HttpServletRequest request) throws Exception {
        try {
            String createBy = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (StringUtils.isEmpty(createBy)) {
                return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
            }
            //角色ID
            String roleId = request.getParameter("ROLE_ID") == null ? "" : request.getParameter("ROLE_ID").trim();
            Criteria criteria = new Criteria();
            criteria.clear();
            if(StringUtils.isNotBlank(roleId)){
                criteria.put("userRoleId", roleId);
            }
            List userList = tSysUserService.selectByExample(criteria);
            return ResulVOUtils.success(userList);
        } catch (Exception e) {
            log.error("findUser ERROR:", e);
            return ResulVOUtils.error(ResulEnum.FIND_USER_LIST_FAIL.getCode(), ResulEnum.FIND_USER_LIST_FAIL.getMsg());
        }

    }
}
