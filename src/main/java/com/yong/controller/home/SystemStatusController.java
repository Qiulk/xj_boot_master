package com.yong.controller.home;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.yong.commons.utils.PublicMsg;
import com.yong.commons.utils.WebUtils;
import com.yong.model.home.Criteria;
import com.yong.model.home.TSystemStatus;
import com.yong.service.home.TSystemStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public class SystemStatusController {
	private static final Logger logger = LoggerFactory.getLogger(SystemStatusController.class);
	@Autowired
	private TSystemStatusService systemStatusService;

	@Value("${sys_error:}")
	private String sysError;



	/**
	 * 基础配置主页
	 */
	@RequestMapping(value = "/gotoStatusIndex",method = RequestMethod.GET)
	public String gotoStatusIndex() {
		return "sys/status_update";
	}



	/**
	 * 获取字典数据，返回list
	 */
	@RequestMapping(value = "/status/statusList", method = RequestMethod.POST)
	@ResponseBody
	public String selectByContion(HttpSession session, HttpServletRequest request) {
		try {
			String statusType = request.getParameter("statusType")==null?"":request.getParameter("statusType").trim();
			Criteria cri = new Criteria();
			cri.clear();
			cri.put(statusType,statusType);
			List<TSystemStatus> list = systemStatusService.selectByContion(cri);

			return WebUtils.getData(true, "",list.get(0));
		} catch (Exception e) {
			logger.error("getSystemStatusList-method-Exception: ", e);
			return WebUtils.getData(false, "获取字典数据异常！",null);
		}
	}

	/**
	 * 获取字典数据，返回list
	 */
	@RequestMapping(value = "/getSystemStatusList", method = RequestMethod.POST,produces = "text/html;charset=utf-8")
	@ResponseBody
	public String getSystemStatusList(HttpSession session, HttpServletRequest request) {
		try {
			String statusType = request.getParameter("STATUS_TYPE")==null?"":request.getParameter("STATUS_TYPE").trim();
			List<TSystemStatus> list = systemStatusService.queryByStatusType(statusType);
			return WebUtils.getData(true, "",list);
		} catch (Exception e) {
			logger.error("getSystemStatusList-method-Exception: ", e);
			return WebUtils.getData(false, "获取字典数据异常！",null);
		}
	}
	
	/**
	 * 获取字典数据，返回map
	 */
	@RequestMapping(value = "/getSystemStatus", method = RequestMethod.POST,produces = "text/html;charset=utf-8")
	@ResponseBody
	public String getSystemStatus(HttpSession session, HttpServletRequest request) {
		try {
			String statusType = request.getParameter("STATUS_TYPE")==null?"":request.getParameter("STATUS_TYPE").trim();
			Map statusMap = systemStatusService.statusCodeAndStatusNameMap(statusType);
			return WebUtils.getData(true, "",statusMap);
		} catch (Exception e) {
			logger.error("getSystemStatus-method-Exception: ", e);
			return WebUtils.getData(false, "获取字典数据异常！",null);
		}
	}


	@RequestMapping(value="/ueditor")
	@ResponseBody
	public String ueditor(HttpServletRequest request) {
		return PublicMsg.UEDITOR_CONFIG;
	}


	/**
	 * 根据主键查询详细信息
	 */
	@RequestMapping(value = "/status/getInfo", method = RequestMethod.POST,produces = "text/html;charset=utf-8")
	@ResponseBody
	public String getInfo(HttpServletRequest request) {
		try {
			String id = request.getParameter("id")==null?"":request.getParameter("id").trim();
			if(id == null && "".equals(id)){
				return WebUtils.getData(false, sysError, null);
			}
			//System.out.println("id========"+id);
			TSystemStatus obj = systemStatusService.get(id);
			return WebUtils.getData(true, "", obj);
		} catch (Exception e) {
			logger.error("ForcerController.java-getInfo-Exception: ", e);
			return WebUtils.getData(false,sysError,null);
		}
	}
}
