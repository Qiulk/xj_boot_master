package com.yong.controller.home;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yong.model.ueditor.Ueditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.yong.commons.utils.FileUpload;


@Controller
public class FileUpLoadController {
	private static final Logger logger = LoggerFactory.getLogger(FileUpLoadController.class);
    //访问路径
	@Value("${local_url:}")
	private String localUrl;
	//临时路径
    @Value("${tmp_url:}")
   	private String tmpUrl;
    //真实路径
    @Value("${real_path:}")
   	private String realPath;



	@RequestMapping(value="/imgUpload")
	@ResponseBody
	public Ueditor imgUpload(MultipartFile upfile) {
		Ueditor ueditor = new Ueditor();
		return ueditor;
	}

	/**
	 * 文件上传Action
	 *
	 * @param req
	 * @return UEDITOR 需要的json格式数据
	 */
	@RequestMapping(value="loan/ueditor/fileupload/upload",method = RequestMethod.POST)
	@ResponseBody
	public Ueditor upload(HttpServletRequest req,HttpServletResponse response){
		Ueditor ueditor = new Ueditor();

		//物理路径
		String sourcePath = req.getSession().getServletContext().getRealPath("/");
		String tmpPath = tmpUrl; //临时存储路径

		//Map<String,Object> result = new HashMap<String, Object>();

		MultipartHttpServletRequest mReq  =  null;
		MultipartFile file = null;
		String fileName = "";
		String fileUrl = "";
		// 原始文件名   UEDITOR创建页面元素时的alt和title属性
		String originalFileName = "";

		//定义允许上传的文件扩展名
		HashMap<String, String> extMap = new HashMap<String, String>();
		extMap.put("image", "gif,jpg,jpeg,png,bmp");
		//extMap.put("flash", "swf,flv");
		extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
		extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");
		String dirName = "image";
		//最大文件大小
		long maxSize = 512000;//500k
		try {
			mReq = (MultipartHttpServletRequest)req;
			// 从config.json中取得上传文件的ID
			file = mReq.getFile("upfile");
			//格式imgFile
			String fileExt = "";
			if (file.getOriginalFilename().lastIndexOf(".") >= 0){
				fileExt = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1).toLowerCase();
			}
			logger.info("fileExt==="+fileExt);
			//System.out.println("fileExt==="+fileExt);
			if(!Arrays.<String>asList(extMap.get(dirName).split(",")).contains(fileExt)){
				//throw new IOException("上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。");
				//result.put("state", "Picture format error,Only allow " + extMap.get(dirName) + "");
				ueditor.setState("传文件扩展名是不允许的扩展名。\\n只允许 " + extMap.get(dirName) + "格式。");
				return ueditor;
			}
			//大小
			if(file.getSize() > maxSize){
				//throw new IOException("上传文件太大,不能超过500k");
				//result.put("state", "Picture too big,not more than 500k");
				ueditor.setState("上传文件太大,不能超过500k");
				return ueditor;
			}
			//创建文件夹-临时
			String dirFileName = dirName + "/";
			File saveDirFile = new File(tmpPath+dirFileName);
			if (!saveDirFile.exists()) {
				saveDirFile.mkdirs();
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String ymd = sdf.format(new Date());
			sdf = new SimpleDateFormat("hh");
			String hh = sdf.format(new Date());
			sdf = new SimpleDateFormat("mm");
			String mm = sdf.format(new Date());
			dirFileName += ymd + "/"+ ymd+hh + "/"+ ymd+hh+mm+ "/";
			File dirFile = new File(tmpPath+dirFileName);
			if (!dirFile.exists()) {
				dirFile.mkdirs();
			}

			//创建文件夹-正式
			String realFilePath = realPath + dirName + "/";
			File realFile = new File(realFilePath);
			if (!realFile.exists()) {
				realFile.mkdirs();
			}
			String realFile2Path = realFilePath + ymd + "/"+ ymd+hh + "/"+ ymd+hh+mm+ "/";
			File realFile2 = new File(realFile2Path);
			if (!realFile2.exists()) {
				realFile2.mkdirs();
			}
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			fileName = df.format(new Date()) + "_" + new Random().nextInt(1000);

			originalFileName = FileUpload.fileUp(file, tmpPath+dirFileName, fileName);
			fileUrl = localUrl + dirFileName +originalFileName;
			logger.info("文件上传成功，地址："+fileUrl);
			ueditor.setState("SUCCESS");
			ueditor.setUrl(fileUrl);
			ueditor.setTitle(originalFileName);
			ueditor.setOriginal(originalFileName);
		}
		catch (Exception e) {
			logger.error("FileUpLoadController-"+fileName+"-upload-Exception: ", e.getMessage());
			//e.printStackTrace();
			//System.out.println("文件上传失败"+e.getMessage());
//			result.put("state", "文件上传失败!");
//			result.put("url","");
//			result.put("title", "");
//			result.put("original", "");
			//System.out.println("文件 "+fileName+" 上传失败!");
			ueditor.setState("文件上传失败");
			ueditor.setUrl("");
			ueditor.setTitle("");
			ueditor.setOriginal("");
		}

//		String resu = JSON.toJSONString(result);
//		logger.info("resu==="+resu);
		//System.out.println("resu==="+resu);
		return ueditor;
	}

}

