package com.yong.controller.home;

import com.yong.commons.enums.SysUserEnums;
import com.yong.commons.utils.Base64ToImg;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.commons.utils.WebUtils;
import com.yong.model.sys.TSysUser;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 文件上传
 */
@Controller
@RequestMapping("/file")
@Slf4j
public class MyfileCOntroller {

    //访问路径
    @Value("${local_url:}")
    private String localUrl;
    //临时路径
    @Value("${tmp_url:}")
    private String tmpUrl;
    //真实路径
    @Value("${real_path:}")
    private String realPath;
    //访问路径
    @Value("${real_url:}")
    private String realUrl;


    /**
     * 文件上传
     * @param file
     * @param request
     * @return
     */
    @RequestMapping("upload")
    @ResponseBody
    public ResulVO getfile(@RequestParam("file") MultipartFile file,HttpServletRequest request){
//        System.out.println("file name = "+file.getOriginalFilename());

        //获取项目路径
        String contentPate = request.getContextPath();


        // 获取文件名
        String fileName = file.getOriginalFilename();
        // 获取后缀
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        // 文件上产的路径
        //String filePath = "d:/upload/";

        fileName = UUID.randomUUID() + fileName;
        // fileName处理
        String filePathName = tmpUrl+fileName;
        // 文件对象
        File dest = new File(filePathName);
        // 创建路径
        if(!dest.getParentFile().exists()){
            dest.getParentFile().mkdir();
        }

        try {
            file.transferTo(dest);
            String temHtpath = localUrl +fileName;
            return ResulVOUtils.success(temHtpath); //"上传成功";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResulVOUtils.error(SysUserEnums.FILE_UPLOD_FAIL.getCode(),SysUserEnums.FILE_UPLOD_FAIL.getMsg());
    }

    /**
     * 文件批量上传
     * @param request
     * @return
     */
    @RequestMapping(value = "/batchupload", method = RequestMethod.POST)
    @ResponseBody
    public void handleFileUpload(HttpServletRequest request) {
        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
        MultipartFile file = null;
        BufferedOutputStream stream = null;

        //获取项目路径
        String contentPate = request.getContextPath();

        for (int i = 0; i < files.size(); ++i) {
            file = files.get(i);

            // 获取文件名
            String fileName = file.getOriginalFilename();
            // 获取后缀
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            //构建文件名
            fileName = UUID.randomUUID()+ "_" + fileName;
            // 路径处理
            String filePathName = tmpUrl+fileName;
            // 文件对象
            File dest = new File(filePathName);
            // 创建路径
            if(!dest.getParentFile().exists()){
                dest.getParentFile().mkdir();
            }
            try {
                file.transferTo(dest);
                String temHtpath = localUrl +fileName;
                //return ResulVOUtils.success(temHtpath); //"上传成功";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 文件下载
     * @param response
     * @throws FileNotFoundException
     */
    @RequestMapping("download")
    public void download(HttpServletResponse response) throws FileNotFoundException {
        File file =new File("F:\\旧系统迁移文件\\20180409-桌面迁移文件\\素材\\图片素材\\timg (4).jpg");
        FileInputStream fileInputStream=new FileInputStream(file);
        // 设置被下载而不是被打开 F:\\旧系统迁移文件\\20180409-桌面迁移文件\\素材\\图片素材\\timg (4).jpg
        response.setContentType("application/gorce-download");
        // 设置被第三方工具打开,设置下载的文件名
        response.addHeader("Content-disposition","attachment;fileName=timg (4).jpg");
        try {
            OutputStream outputStream = response.getOutputStream();
            byte[] bytes = new byte[1024];
            int len = 0;
            while ((len = fileInputStream.read(bytes))!=-1){
                outputStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件批量上传
     * @param request
     * @return
     */
    @RequestMapping(value = "/scanFileUploadPout", method = RequestMethod.POST)
    @ResponseBody
    public void scanFileUploadPout(HttpServletRequest request) {
        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("RemoteFile");
        MultipartFile file = null;
        BufferedOutputStream stream = null;

        //获取项目路径
        String contentPate = request.getContextPath();

        for (int i = 0; i < files.size(); ++i) {
            file = files.get(i);

            // 获取文件名
            String fileName = file.getOriginalFilename();
            // 获取后缀
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            //构建文件名
            fileName = UUID.randomUUID()+ "_" + fileName;




            // 路径处理
            String filePathName = tmpUrl+fileName;
            // 文件对象
            File dest = new File(filePathName);
            // 创建路径
            if(!dest.getParentFile().exists()){
                dest.getParentFile().mkdir();
            }
            try {
                file.transferTo(dest);
                String temHtpath = localUrl +fileName;
                log.info("=========================================:"+temHtpath);
                //return ResulVOUtils.success(temHtpath); //"上传成功";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 文件批量上传
     * @param request
     * @return
     */
    @RequestMapping(value = "/scanupload", method = RequestMethod.POST)
    @ResponseBody
    public void scanFileUpload(HttpServletRequest request, HttpSession session) {
        String ImageName = request.getParameterValues("ImageName")==null?"":request.getParameter("ImageName").trim();
        String RemoteFile = request.getParameter("RemoteFile")==null?"":request.getParameter("RemoteFile").trim();
        String id = request.getParameter("id")==null?"":request.getParameter("id").trim();
        String type = request.getParameter("type")==null?"":request.getParameter("type").trim();
        try {
            TSysUser loginUser = (TSysUser)session.getAttribute("CURRENT_USER");
            if(null == loginUser){
                return;
            }

            //判断参数是否为空
            if(WebUtils.isBlank(RemoteFile)){
                log.info("=====================RemoteFile参数为空");
                return;
            }
            if(WebUtils.isBlank(ImageName)){
                log.info("=====================ImageName参数为空");
                return;
            }
            String[] ImageNameArry = ImageName.split(",");
            String[] RemoteFileArry = RemoteFile.split(",");
            //判断名称与文件流base64是否一致
            if(ImageNameArry.length != RemoteFileArry.length){
                return;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String ymd = sdf.format(new Date());
            sdf = new SimpleDateFormat("hh");
            String hh = sdf.format(new Date());
            sdf = new SimpleDateFormat("mm");
            String mm = sdf.format(new Date());
            String dirFileName = ymd + "/"+ ymd+hh + "/"+ ymd+hh+mm+ "/";
            String imgPath =  realPath + dirFileName;
            File dirFile = new File(imgPath);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            String url = "";//访问路径
            String path = "";//真实路径
            for (int i=0;i<RemoteFileArry.length;i++){
                url = "";
                path = "";
                String str = RemoteFileArry[i];
                Base64ToImg.GenerateImage(str,imgPath,ImageNameArry[i]);
                path = imgPath+ImageNameArry[i];
                url = path.replace(realPath,realUrl);
                log.info("真实路径：{}",path);
                log.info("访问路径：{}",url);
                //存入数据库

                if(url.isEmpty() || path.isEmpty()){
                    throw new Exception("路径为空");
                }

            }

        }catch (Exception e){
            log.error("MyfileController.class-scanFileUpload-Exception:",e);
        }
    }
}

