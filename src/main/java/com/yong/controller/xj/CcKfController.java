package com.yong.controller.xj;

import com.yong.model.xj.TLoanCckf;
import com.yong.service.xj.TLoanCckfService;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * description: CcKfController <br>
 * date: 2019/12/17 23:57 <br>
 * author: LKQiu <br>
 * version: 1.0 <br>
 */
@RequestMapping(value = "/admin/cckf")
@Controller
@Slf4j
public class CcKfController {

    @Autowired
    private TLoanCckfService loanCckfService;

    @RequestMapping(method = RequestMethod.GET)
    public String memberList() {
        return "xj/cckf_index";
    }


    @GetMapping(value = "/getCckf")
    @ResponseBody
    public ResulVO getCckf() {
        return loanCckfService.findLoanCckf("index");
    }

    @RequestMapping(value = "jumpUpdate")
    public String jumpUpdate(HttpServletRequest request) {

        ResulVO resulVO = loanCckfService.findLoanCckf("index");
        TLoanCckf loanCckf = (TLoanCckf) resulVO.getRs();

        request.setAttribute("cckfMsg", loanCckf);
        return "xj/cckf_update";
    }

    @PostMapping(value = "update")
    @ResponseBody
    public ResulVO update(TLoanCckf cckf) {
        cckf.setUpdateTime(new Date());
        return loanCckfService.updateLoanCckf(cckf);
    }
}
