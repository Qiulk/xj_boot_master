package com.yong.controller.xj;

import com.yong.service.xj.SMSApiService;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.SMSApiSimpleDTO;
import com.yong.vo.xj.SMSApiVarCodeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/12/16 14:25
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/12/16	   邱良堃		    		新建
 * </pre>
 */
@Controller
@RequestMapping(value = "loan/sms")
public class SMSApi {

    @Autowired
    private SMSApiService smsApiService;

    @RequestMapping(value = "sendSMSVarCode")
    @ResponseBody
    public ResulVO sendSMSVarCode(SMSApiVarCodeDTO dto, HttpSession session) {
//        Long userId = (Long) session.getAttribute("user_Id");
        dto.setUserId(412423L);
        return smsApiService.sendSMSVarCode(dto);
    }


    @RequestMapping(value = "sendSMSSimple")
    @ResponseBody
    public ResulVO sendSMSSimple(SMSApiSimpleDTO dto, HttpSession session) {
        Long userId = (Long) session.getAttribute("user_Id");
        dto.setUserId(userId);
        return smsApiService.sendSMSSimple(dto);
    }
}
