package com.yong.controller.xj;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.service.xj.LoanInfoService;
import com.yong.service.xj.TLoanCckfService;
import com.yong.vo.ResulVO;
import com.yong.vo.xj.ApplyInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/22 23:04
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/22	   邱良堃		    		新建
 * </pre>
 */
@Controller
@RequestMapping("/loan/loanInfo")
@Slf4j
public class LoanInfoController {

    @Autowired
    private LoanInfoService loanInfoService;

    @Autowired
    private TLoanCckfService tLoanCckfService;

    @GetMapping(value = "/queryById")
    @ResponseBody
    public ResulVO queryById(@RequestParam(name = "pkId", required = true) Long pkId) {
        return loanInfoService.queryById(pkId);
    }

    @PostMapping(value = "/add")
    @ResponseBody
    public ResulVO add(@RequestBody ApplyInfoDTO applyInfoDTO, HttpSession session) {
        return loanInfoService.add(applyInfoDTO, session);
    }

    @PutMapping(value = "/edit")
    @ResponseBody
    public ResulVO edit(@RequestBody ApplyInfoDTO applyInfoDTO, HttpSession session) {
        return loanInfoService.edit(applyInfoDTO, session);
    }

    @GetMapping(value = "/ccKeFu")
    @ResponseBody
    public ResulVO cckefu(@RequestParam(name = "webid", required = true) String webid) {
        if (webid != null && !webid.equals("")) {
            return ResulVOUtils.error(202, "已有ID");
        }
        return tLoanCckfService.findLoanCckf();
    }
}
