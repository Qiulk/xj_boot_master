
package com.yong.controller.xj;

import com.yong.commons.utils.AuthCode.SendAuthCode;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.model.xj.TUser;
import com.yong.service.xj.SMSApiService;
import com.yong.service.xj.TUserService;
import com.yong.vo.ResulVO;
import com.yong.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/24 0:12
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/24	   邱良堃		    		新建
 * </pre>
 */

@Controller
@RequestMapping("/loan")
@Slf4j
public class LoanUserController {

    @Autowired
    private TUserService userService;

    @Autowired
    private SendAuthCode sendAuthCode;

    @Autowired
    private SMSApiService smsApiService;

    @GetMapping(value = "/userLogin")
    @ResponseBody
    public ResulVO userLogin(HttpSession httpSession, @RequestParam(name = "loginPhone", required = true) String loginPhone, @RequestParam(name = "password", required = true) String password) {

        return userService.userLogin(httpSession, loginPhone, password);

    }

    @GetMapping(value = "/userSave")
    @ResponseBody
    public ResulVO userRegister(@RequestParam(name = "loginPhone", required = true) String loginPhone, @RequestParam(name = "password", required = true) String password,
                                @RequestParam(name = "varCode", required = true) String varCode, HttpSession session) {
        TUser user = new TUser();

        user.setLoginPhone(loginPhone);
        user.setPassword(password);

        ResulVO resulVO = smsApiService.authVarCode(loginPhone, varCode);

        if (resulVO != null) {
            return resulVO;
        }

        return userService.save(user, session);
    }

    @PostMapping(value = "/exit")
    @ResponseBody
    public ResulVO exit(HttpSession session) {
        session.removeAttribute("user_Id");
        return ResulVOUtils.success();
    }


    @GetMapping(value = "/getUser")
    @ResponseBody
    public ResulVO get(HttpSession session) {
        Long userId = (Long) session.getAttribute("user_Id");
        return userService.findById(userId);
    }

    @GetMapping(value = "/getAuthCode")
    public ResulVO getAuthCode() {
        return sendAuthCode.sendAuthCode();
    }

//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public List<User> getUserList() {
//        // 处理"/users/"的GET请求，用来获取用户列表
//        // 还可以通过@RequestParam从页面中传递参数来进行查询条件或者翻页信息的传递
//        List<User> r = new ArrayList<User>(users.values());
//        return r;
//    }

//    @RequestMapping(value = "/", method = RequestMethod.POST)
//    public String postUser(@ModelAttribute User user) {
//        // 处理"/users/"的POST请求，用来创建User
//        // 除了@ModelAttribute绑定参数之外，还可以通过@RequestParam从页面中传递参数
//        users.put(user.getId(), user);
//        return "success";
//    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
//    public User getUser(@PathVariable Long id) {
//        // 处理"/users/{id}"的GET请求，用来获取url中id值的User信息
//        // url中的id可通过@PathVariable绑定到函数的参数中
//        return users.get(id);
//    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public String putUser(@PathVariable Long id, @ModelAttribute User user) {
//        // 处理"/users/{id}"的PUT请求，用来更新User信息
//        User u = users.get(id);
//        u.setName(user.getName());
//        u.setAge(user.getAge());
//        users.put(id, u);
//        return "success";
//    }

//     @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    public String deleteUser(@PathVariable Long id) {
//        // 处理"/users/{id}"的DELETE请求，用来删除User
//        users.remove(id);
//        return "success";
//    }

}

