package com.yong.controller.xj;

import com.yong.commons.SpringUtils.token.Token;
import com.yong.commons.enums.ResulEnum;
import com.yong.commons.enums.SysUserEnums;
import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.commons.utils.WebConstants;
import com.yong.commons.utils.WebUtils;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.xj.TApplyInfo;
import com.yong.service.home.TSystemStatusService;
import com.yong.service.sys.TSysOrganizationService;
import com.yong.service.sys.TSysRoleService;
import com.yong.service.xj.LoanInfoService;
import com.yong.service.xj.TApplyInfoService;
import com.yong.service.xj.TStatusService;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * 用户管理
 */
@Controller
@RequestMapping("/admin/jk")
@Slf4j
public class ApplyInfoController {

    private static final Logger logger = LoggerFactory.getLogger(ApplyInfoController.class);

    private int pageSize = 10;

    private int currentPage = 1;

    @Value("${login_timeout:}")
    private String loginTimeout;

    @Value("${sys_error:}")
    private String sysError;

    @Value("${loan_upload_fw_url}")
    private String Img_Url;

    @Autowired
    private TApplyInfoService applyInfoService;

    @Autowired
    private LoanInfoService loanInfoService;

    @Autowired
    private TStatusService tStatusService;

    @Autowired
    private TSystemStatusService systemStatusService;

    /**
     * 用户列表
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String memberList() {
        return "xj/apply-list";
    }

    @RequestMapping(value = "/takeCashList")
    public String takeCashList() {
        return "xj/repayMent-list";
    }

    /**
     * 添加用户
     *
     * @return
     */
    @RequestMapping(value = "/member-add")
    @Token(save = true)
    public String member_add(HttpServletRequest request) {
        return "member/member-add";
    }

    /**
     * 个人信息
     *
     * @return
     */
    @RequestMapping(value = "/getPersonInfo")
    public String getPersonInfo(HttpServletRequest request, HttpSession session) {
        String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
        if (StringUtils.isBlank(id)) {
            id = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
        }
        request.setAttribute("id", id);
        return "member/member-personInfo";
    }

    /**
     * 编辑用户
     *
     * @return
     */
    @RequestMapping(value = "/update")
    @Token(save = true)
    public String member_update(HttpServletRequest request) {
        String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
        request.setAttribute("id", id);
        return "xj/apply-update";
    }

    /**
     * 编辑用户
     *
     * @return
     */
    @RequestMapping(value = "/takeCashUpdate")
    @Token(save = true)
    public String takeCashUpdate(HttpServletRequest request) {
        String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
        request.setAttribute("id", id);
        return "xj/repayMent-update";
    }

    /**
     * 编辑用户
     *
     * @return
     */
    @RequestMapping(value = "/viewLoan")
    @Token(save = true)
    public String viewLoan(HttpServletRequest request) {
        try {
            String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
            if (id == null && "".equals(id)) {
                return WebUtils.getData(false, sysError, null);
            }
            ResulVO resulVO = loanInfoService.queryById(Long.parseLong(id));
            request.setAttribute("loanInfo", resulVO.getRs());
            request.setAttribute("Img_Url", Img_Url);
        }
        catch (Exception e) {
            logger.error("ApplyInfoController.java-getInfo-Exception: ", e);
            return WebUtils.getData(false, sysError, null);
        }
        return "xj/apply-loanInfo";
    }

    /**
     * 修改密码
     *
     * @return
     */
    @RequestMapping(value = "/update_pwd")
    public String updatePwd(HttpServletRequest request) {
        return "sys/update_pwd";
    }

    /**
     * 用户列表查询
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO member_list_data(
            @RequestParam String PAGE_SIZE,
            @RequestParam String CURRENT_PAGE,
            @RequestParam String current,
            HttpSession session,
            HttpServletRequest request) throws Exception {
        try {
            String createBy = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (StringUtils.isEmpty(createBy)) {
                return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
            }
            String realName = request.getParameter("realName") == null ? "" : request.getParameter("realName").trim();
            if (!StringUtils.isEmpty(current)) {
                CURRENT_PAGE = current;
            }
            Criteria criteria = new Criteria();
            criteria.clear();
            realName = java.net.URLDecoder.decode(realName, "UTF-8");
            if (!StringUtils.isEmpty(realName) && !realName.equals("undefined")) {
                criteria.put("realName", realName);
            }
            criteria.put("createBy", Long.parseLong(createBy));
            //分页
            if (!StringUtils.isEmpty(PAGE_SIZE)) {
                pageSize = Integer.valueOf(PAGE_SIZE);
            }
            if (!StringUtils.isEmpty(CURRENT_PAGE)) {
                currentPage = Integer.valueOf(CURRENT_PAGE);
            }
            Pager pager = new Pager(pageSize, currentPage);
            ComboPager comboPager = applyInfoService.queryUserByPager(criteria, pager);
            return ResulVOUtils.success(comboPager);
        }
        catch (Exception e) {
            log.error("member_list_data ERROR:", e);
            return ResulVOUtils.error(ResulEnum.USER_LIST_FAIKL.getCode(), ResulEnum.USER_LIST_FAIKL.getMsg());
        }
    }

    /**
     * 用户列表查询
     *
     * @return
     */
    @RequestMapping(value = "/takeList", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO takeList(
            @RequestParam String PAGE_SIZE,
            @RequestParam String CURRENT_PAGE,
            @RequestParam String current,
            HttpSession session,
            HttpServletRequest request) throws Exception {
        try {
            String createBy = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (StringUtils.isEmpty(createBy)) {
                return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
            }
            String realName = request.getParameter("realName") == null ? "" : request.getParameter("realName").trim();
            if (!StringUtils.isEmpty(current)) {
                CURRENT_PAGE = current;
            }
            Criteria criteria = new Criteria();
            criteria.clear();
            realName = java.net.URLDecoder.decode(realName, "UTF-8");
            if (!StringUtils.isEmpty(realName) && !realName.equals("undefined")) {
                criteria.put("realName", realName);
            }
            criteria.put("createBy", Long.parseLong(createBy));
            //分页
            if (!StringUtils.isEmpty(PAGE_SIZE)) {
                pageSize = Integer.valueOf(PAGE_SIZE);
            }
            if (!StringUtils.isEmpty(CURRENT_PAGE)) {
                currentPage = Integer.valueOf(CURRENT_PAGE);
            }
            Pager pager = new Pager(pageSize, currentPage);
            ComboPager comboPager = applyInfoService.takeCachqueryUserByPager(criteria, pager);
            return ResulVOUtils.success(comboPager);
        }
        catch (Exception e) {
            log.error("member_list_data ERROR:", e);
            return ResulVOUtils.error(ResulEnum.USER_LIST_FAIKL.getCode(), ResulEnum.USER_LIST_FAIKL.getMsg());
        }
    }

    /**
     * 修改页面，获取用户信息
     */
    @RequestMapping(value = "/getInfo", method = RequestMethod.POST, produces = "text/html;charset=utf-8")
    @ResponseBody
    public String getUserInfo(HttpServletRequest request) {
        try {
            String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
            if (id == null && "".equals(id)) {
                return WebUtils.getData(false, sysError, null);
            }
            TApplyInfo applyInfo = applyInfoService.getUserInfoByPk(Long.parseLong(id));
            Map statusMap = systemStatusService.statusCodeAndStatusNameMap(WebConstants.APPLY_STATUS);
            String statusName = (String) statusMap.get(String.valueOf(applyInfo.getStatus()));
            Map resutl = new HashMap();
            resutl.put("tx_card", applyInfo.getTx_card());
            resutl.put("pkId", applyInfo.getPkId());
            resutl.put("nowNode", statusName);
            resutl.put("remark", applyInfo.getRemark());
            //            ApplyInfoDTO
            return WebUtils.getData(true, "", resutl);
        }
        catch (Exception e) {
            logger.error("ApplyInfoController.java-getInfo-Exception: ", e);
            return WebUtils.getData(false, sysError, null);
        }
    }

    /**
     * 修改用户信息
     */
    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    @ResponseBody
    @Token(remove = true)
    public ResulVO modify(HttpSession session, HttpServletRequest request) {
        try {
            String sessionUserNo = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (WebUtils.isNull(sessionUserNo)) {
                return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), ResulEnum.NO_SYS_USER.getMsg());
            }
            String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
            String tx_card = request.getParameter("tx_card") == null ? "" : request.getParameter("tx_card").trim();
            String remark = request.getParameter("remark") == null ? "" : request.getParameter("remark").trim();
            String status = request.getParameter("status") == null ? "" : request.getParameter("status").trim();
            TApplyInfo obj = new TApplyInfo();
            obj.setPkId(Long.parseLong(id));
            obj.setTx_card(tx_card);
            obj.setStatus(status);
            obj.setRemark(remark);
            obj.setUpdateBy(sessionUserNo);
            obj.setUpdateDate(new Date());
            applyInfoService.modify(obj);
            return ResulVOUtils.success();
        }
        catch (Exception e) {
            logger.error("modify-method-Exception: ", e);
            String token = UUID.randomUUID().toString();
            session.setAttribute("token", token);
            return ResulVOUtils.error(ResulEnum.NO_SYS_USER.getCode(), "修改用户信息异常!");
        }
    }

    /**
     * 获取状态列表
     */
    @RequestMapping(value = "/getStatusList", method = RequestMethod.POST, produces = "text/html;charset=utf-8")
    @ResponseBody
    public String getStatusList(HttpSession session, HttpServletRequest request, Integer queryType) {
        ResulVO re = tStatusService.queryStatusList(queryType);
        Map resutl = new HashMap();
        resutl.put("statusList", re.getRs());
        return WebUtils.getData(true, "", resutl);
    }
}
