package com.yong.controller.xj;

import com.yong.commons.enums.ResulEnum;
import com.yong.commons.enums.SysUserEnums;
import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <pre>
 *  还款列表
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/7 15:21
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/07	   邱良堃		    		新建
 * </pre>
 */
@Controller
@RequestMapping("/admin/hk")
@Slf4j
public class RepayMentController {

    private static final Logger logger = LoggerFactory.getLogger(RepayMentController.class);

    private int pageSize = 10;
    private int currentPage = 1;

    /**
     * 用户列表
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String memberList() {
        return "xj/repayMent-list";
    }


    /**
     * 还款列表
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO member_list_data(
            @RequestParam String PAGE_SIZE,
            @RequestParam String CURRENT_PAGE,
            @RequestParam String current,
            HttpSession session,
            HttpServletRequest request) throws Exception {
        try {
            String createBy = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
            if (StringUtils.isEmpty(createBy)) {
                return ResulVOUtils.error(SysUserEnums.LONGIN_OUT.getCode(), SysUserEnums.LONGIN_OUT.getMsg());
            }
            String realName = request.getParameter("realName") == null ? "" : request.getParameter("realName").trim();

            if (!StringUtils.isEmpty(current)) {
                CURRENT_PAGE = current;
            }
            Criteria criteria = new Criteria();
            criteria.clear();
            realName = java.net.URLDecoder.decode(realName, "UTF-8");

            if (!StringUtils.isEmpty(realName) && !realName.equals("undefined")) {
                criteria.put("realName", realName);
            }
            criteria.put("createBy", Long.parseLong(createBy));
            //分页
            if (!StringUtils.isEmpty(PAGE_SIZE)) {
                pageSize = Integer.valueOf(PAGE_SIZE);
            }
            if (!StringUtils.isEmpty(CURRENT_PAGE)) {
                currentPage = Integer.valueOf(CURRENT_PAGE);
            }
            Pager pager = new Pager(pageSize, currentPage);
//            ComboPager comboPager = applyInfoService.queryUserByPager(criteria, pager);
            return ResulVOUtils.success();
        } catch (Exception e) {
            log.error("member_list_data ERROR:", e);
            return ResulVOUtils.error(ResulEnum.USER_LIST_FAIKL.getCode(), ResulEnum.USER_LIST_FAIKL.getMsg());
        }

    }

}
