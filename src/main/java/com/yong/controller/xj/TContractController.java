package com.yong.controller.xj;

import com.yong.commons.enums.ResulEnum;
import com.yong.commons.enums.SysUserEnums;
import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.xj.TContract;
import com.yong.service.xj.TContractService;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/11 15:21
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/11	   邱良堃		    		新建
 * </pre>
 */
@Controller
@RequestMapping("/admin/ht")
@Slf4j
public class TContractController {

    private static final Logger logger = LoggerFactory.getLogger(TContractController.class);

    private int pageSize = 10;
    private int currentPage = 1;

    @Autowired
    private TContractService tContractService;

    /**
     * 用户列表
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String memberList() {
        return "xj/contract-list";
    }

    /**
     * 用户列表
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO member_list_data(
            @RequestParam String PAGE_SIZE,
            @RequestParam String CURRENT_PAGE,
            @RequestParam String current,
            HttpSession session,
            HttpServletRequest request) throws Exception {
        try {
            Pager pager = new Pager(pageSize, currentPage);
            pager.setTotalRows(1);
            ComboPager comboPager = new ComboPager();
            comboPager.setPager(pager);
            comboPager.setRs(tContractService.findParant());
            return ResulVOUtils.success(comboPager);
        } catch (Exception e) {
            log.error("member_list_data ERROR:", e);
            return ResulVOUtils.error(ResulEnum.USER_LIST_FAIKL.getCode(), ResulEnum.USER_LIST_FAIKL.getMsg());
        }
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public String getInfo(HttpServletRequest request) {
        String id = request.getParameter("id") == null ? "" : request.getParameter("id").trim();
        request.setAttribute("id", id);
        TContract tContract = tContractService.findById(Long.parseLong(id));
        request.setAttribute("name", tContract.getContent());
        return "xj/contract-info";
    }

    @RequestMapping(value = "/getInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO get(Long paraentId) {
        return ResulVOUtils.success(tContractService.findByParant(paraentId));
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO update(TContract tContract) {
        System.out.println(tContract);
        return tContractService.update(tContract);
    }


}
