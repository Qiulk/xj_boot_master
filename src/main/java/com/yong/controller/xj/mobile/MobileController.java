package com.yong.controller.xj.mobile;

import com.yong.commons.enums.ApiResultStatus;
import com.yong.commons.enums.ResulEnum;
import com.yong.commons.enums.WebConstantsEnum;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.commons.utils.WebUtils;
import com.yong.controller.xj.ApplyInfoController;
import com.yong.model.home.ComboPager;
import com.yong.model.home.Criteria;
import com.yong.model.home.Pager;
import com.yong.model.xj.TApplyInfo;
import com.yong.model.xj.TLoanCckf;
import com.yong.service.xj.LoanInfoService;
import com.yong.service.xj.TApplyInfoService;
import com.yong.service.xj.TLoanCckfService;
import com.yong.service.xj.TStatusService;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * description: MobileController <br>
 * date: 2019/12/19 22:34 <br>
 * author: LKQiu <br>
 * version: 1.0 <br>
 */
@Controller
@RequestMapping("/mobile/admin")
@Slf4j
public class MobileController {

    private static final Logger logger = LoggerFactory.getLogger(MobileController.class);

    private int pageSize = 10;

    private int currentPage = 1;

    @Autowired
    private TStatusService tStatusService;

    @Autowired
    private TLoanCckfService loanCckfService;

    @Autowired
    private TApplyInfoService applyInfoService;

    @Autowired
    private LoanInfoService loanInfoService;

    @RequestMapping(value = "/getCckfMsg", method = RequestMethod.GET)
    @ResponseBody
    public ResulVO getCckfMsg() {
        //        return loanCckfService.findLoanCckf("");
        return ResulVOUtils.error(ApiResultStatus.FAIL_ERRO_500, "获取失败");
    }

    @RequestMapping(value = "/updateCCkf", method = RequestMethod.POST)
    @ResponseBody
    public ResulVO updateCCkf(TLoanCckf cckf) {
        cckf.setUpdateTime(new Date());
        return loanCckfService.updateLoanCckf(cckf);
    }

    @RequestMapping(value = "/getApplyList", method = RequestMethod.GET)
    @ResponseBody
    public ResulVO getApplyList(@RequestParam String PAGE_SIZE,
            @RequestParam String CURRENT_PAGE,
            @RequestParam String applyType,
            HttpServletRequest request) {
        try {
            Criteria criteria = new Criteria();
            criteria.clear();
            //分页
            if (!StringUtils.isEmpty(PAGE_SIZE)) {
                pageSize = Integer.valueOf(PAGE_SIZE);
            }
            if (!StringUtils.isEmpty(CURRENT_PAGE)) {
                currentPage = Integer.valueOf(CURRENT_PAGE);
            }
            ComboPager comboPager = new ComboPager();
            Pager pager = new Pager(pageSize, currentPage);
            if (applyType != null) {
                if ("10".equals(applyType)) {
                    comboPager = applyInfoService.queryUserByPager(criteria, pager);
                }
                else {
                    comboPager = applyInfoService.takeCachqueryUserByPager(criteria, pager);
                }
            }
            return ResulVOUtils.success(comboPager);
        }
        catch (Exception e) {
            log.error("member_list_data ERROR:", e);
            return ResulVOUtils.error(ResulEnum.USER_LIST_FAIKL.getCode(), ResulEnum.USER_LIST_FAIKL.getMsg());
        }
    }

    @RequestMapping(value = "/getStatusList", method = RequestMethod.GET)
    @ResponseBody
    public ResulVO getStatusList(@RequestParam(value = "queryType", required = false) Integer queryType) {
        queryType = 1;
        return tStatusService.queryStatusList(queryType);
    }

    @RequestMapping(value = "/getApplyInfo", method = RequestMethod.GET)
    @ResponseBody
    public ResulVO getApplyInfo(String id) {
        ResulVO resulVO = loanInfoService.queryById(Long.parseLong(id));
        return resulVO;
    }

    @RequestMapping(value = "/updateApply")
    @ResponseBody
    public ResulVO updateApply(TApplyInfo applyInfo, HttpSession session) {
        String sessionUserNo = (String) session.getAttribute(WebConstantsEnum.SESSION_USER_ON.getMsg());
        if (applyInfo != null) {
            applyInfo.setUpdateBy(sessionUserNo);
            applyInfo.setUpdateDate(new Date());
            try {
                applyInfoService.modify(applyInfo);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ResulVOUtils.success();
    }
}
