package com.yong.controller.xj;

import com.yong.service.xj.LoanFlowService;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/28 10:49
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/28	   邱良堃		    		新建
 * </pre>
 */
@Controller
@Slf4j
@RequestMapping("/loan/loanFlow")
public class LoanFlowController {

    @Autowired
    private LoanFlowService loanFlowService;

    @GetMapping(value = "/list")
    @ResponseBody
    public ResulVO queryPageList(@RequestParam(name = "start", defaultValue = "0") Integer start
            , @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpSession session) {
        return loanFlowService.queryPageList(start, pageSize, session);
    }


    @GetMapping(value = "/get")
    @ResponseBody
    public ResulVO queryById(HttpSession session) {
//        return loanFlowService.queryPageList(start, pageSize, session);
        return null;
    }


}
