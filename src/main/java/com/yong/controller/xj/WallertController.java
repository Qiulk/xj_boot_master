package com.yong.controller.xj;

import com.yong.service.xj.TAccountMsgService;
import com.yong.service.xj.TContractService;
import com.yong.service.xj.WalletService;
import com.yong.service.xj.impl.WalletServiceImpl;
import com.yong.vo.ResulVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/11/6 21:34
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/11/06	   邱良堃		    		新建
 * </pre>
 */
@Controller
@RequestMapping("/loan/wallet")
@Slf4j
public class WallertController {

    @Autowired
    private TAccountMsgService accountMsgService;

    @Autowired
    private WalletService walletService;

    @Autowired
    private TContractService contractService;

    @GetMapping(value = "/queryById")
    @ResponseBody
    public ResulVO queryById(HttpSession session) {
        return accountMsgService.queryAccountByUid(session);
    }

    @PutMapping(value = "/takeCash")
    @ResponseBody
    public ResulVO takeCash(HttpSession session) {

        return walletService.takeCash(session);
    }

    @GetMapping(value = "/getContract")
    @ResponseBody
    public ResulVO getContract(Long conId) {
        return contractService.findByParant(conId);
    }

}
