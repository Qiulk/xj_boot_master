package com.yong.controller.xj;

import com.yong.commons.utils.DateFormatUtil;
import com.yong.commons.utils.ResulVOUtils;
import com.yong.vo.ResulVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * <pre>
 *
 * </pre>
 * <pre>
 * @author 邱良堃
 * <b>mail</mail> qiuliangkun@syncsoft.com.cn
 * <b>data</data> 2019/10/31 23:48
 * @versison 1.0.0
 * 修改记录
 *  版本号		修订日期		修改人		bug编号		修改内容
 *  1.0.0		2019/10/31	   邱良堃		    		新建
 * </pre>
 */
@Controller
public class FileUPloadC {

    //上传路径
    @Value("${loan_upload_url:}")
    private String tmpUrl;

    @Value("loan_upload_xd_url")
    private String xd_Url;

    @PostMapping(value = "/loan/fileUpload")
    @ResponseBody
    public ResulVO fileUpload(@RequestParam(value = "upfile") MultipartFile file,
                              @RequestParam(value = "reverse") String reverse,
                              @RequestParam(value = "pkId") Long pkId, Model model, HttpServletRequest request) {
        if (file.isEmpty()) {
            System.out.println("文件为空空");
        }
                                                        String fileName = file.getOriginalFilename();  // 文件名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        String filePath = tmpUrl; // 上传后的路径
        String wjj = DateFormatUtil.getDateString(new Date());
        wjj = wjj.replace("-", "");
        filePath += wjj + "/";
        fileName = pkId + reverse + suffixName; // 新文件名
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String filename = wjj + "/" + fileName;
        return ResulVOUtils.success(filename);
    }

}
