/*
 Navicat Premium Data Transfer

 Source Server         : LK
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : xj_boot

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 16/12/2019 19:28:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_loan_sms
-- ----------------------------
DROP TABLE IF EXISTS `t_loan_sms`;
CREATE TABLE `t_loan_sms`  (
  `id` bigint(15) NOT NULL COMMENT '主键',
  `phoneNum` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `userId` bigint(15) NULL DEFAULT NULL COMMENT '用户ID',
  `content` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `smsType` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'varCode验证。simple普通',
  `status` int(5) NULL DEFAULT NULL COMMENT '状态t_sms_status',
  `backMsg` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '调用接口返回信息',
  `backCode` int(11) NULL DEFAULT NULL COMMENT '调用接口返回值',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_loan_sms
-- ----------------------------
INSERT INTO `t_loan_sms` VALUES (18508829658, '18965145657', 412423, '123456', 'varCode', 10, '请求成功', 200, '2019-12-16 18:00:56', '2019-12-16 18:00:56');

SET FOREIGN_KEY_CHECKS = 1;
