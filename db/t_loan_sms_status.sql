/*
 Navicat Premium Data Transfer

 Source Server         : LK
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : xj_boot

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 16/12/2019 19:29:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_loan_sms_status
-- ----------------------------
DROP TABLE IF EXISTS `t_loan_sms_status`;
CREATE TABLE `t_loan_sms_status`  (
  `id` int(5) NOT NULL COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_loan_sms_status
-- ----------------------------
INSERT INTO `t_loan_sms_status` VALUES (10, '已发送');
INSERT INTO `t_loan_sms_status` VALUES (11, '已验证');
INSERT INTO `t_loan_sms_status` VALUES (12, '已过期');
INSERT INTO `t_loan_sms_status` VALUES (13, '发送失败');

SET FOREIGN_KEY_CHECKS = 1;
